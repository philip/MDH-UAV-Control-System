/**************************************************************************
* NAME: system_clock.h                                                    *
* AUTHOR: Lennart Eriksson                                                *
* PURPOSE: Enable and handle system clock functionality                   *
* INFORMATION:                                                            *
* This file initilizes the system clock and handles delays, get time      *
* in both micro- and milliseconds.                                        *
*                                                                         *
* GLOBAL VARIABLES:                                                       *
* Variable        Type          Description                               *
* --------        ----          -----------                               *
**************************************************************************/


#ifndef DRIVERS_SYSTEM_CLOCK_H_
#define DRIVERS_SYSTEM_CLOCK_H_

#include "stm32f4xx.h"

/***********************************************************************
* BRIEF: Starts the system clock at 100MHz                             *
* INFORMATION: In the current version it works with ADC and DMA        *
***********************************************************************/
void system_clock_config(void);

/***********************************************************************
* BRIEF: Get the time since the system started in milliseconds
* INFORMATION: return the time in milliseconds
***********************************************************************/
uint32_t clock_get_ms();

/***********************************************************************
* BRIEF: Get the time since the system started in microseconds
* INFORMATION: return the time in microseconds
***********************************************************************/
uint32_t clock_get_us();

/***********************************************************************
* BRIEF: stall the system for number of milliseconds
* INFORMATION:
***********************************************************************/
void clock_delay_ms(uint32_t ms);

/***********************************************************************
* BRIEF: Stall the system for a number of microseconds
* INFORMATION:
***********************************************************************/
void clock_delay_us(uint32_t us);


#endif /* DRIVERS_SYSTEM_CLOCK_H_ */
