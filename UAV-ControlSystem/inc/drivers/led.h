/**************************************************************************
* NAME: 	   led.h                                                      *
* 	                                                                      *
* AUTHOR: 	   Jonas Holmberg 											  *
*																	      *
* PURPOSE: 	   Contains some led functionality.							  *
* 										                                  *
* INFORMATION: Contains functions to configure pins to leds. It also      *
* 			   contains some led error codes that can be used in the 	  *
* 			   system, although at the time hardcoded to work only with   *
* 			   the revo on board leds.   								  *
* 			                                                              *
* GLOBAL VARIABLES:                                                       *
* Variable        Type          Description                               *
* --------        ----          -----------                               *
***************************************************************************/

#ifndef DRIVERS_LED_H_
#define DRIVERS_LED_H_

//Update rate for the led
#define LED_UPDATE_TIMER 100000

//Different led warning approaches
typedef enum
{
	LEDWARNING_LED0_BLINK = 0,
	LEDWARNING_LED1_BLINK,
	LEDWARNING_LED0_ON,
	LEDWARNING_LED1_ON,
	LEDWARNING_LED0_BLINK_ONCE,
	LEDWARNING_LED1_BLINK_ONCE,
	LEDWARNING_OFF
}ledWarnings_t;


/**************************************************************************
* BRIEF:	   Enables pins so that they can be use for a Led
*
* INFORMATION: Given a pin and port enables that configuration to use led
**************************************************************************/
void ledEnable(uint16_t led_pin, GPIO_TypeDef* led_port);


/**************************************************************************
* BRIEF: 	   Enables the two leds on board leds on the revolution board
*
* INFORMATION:
**************************************************************************/
void ledReavoEnable(void);


/**************************************************************************
* BRIEF:	   Toggles a led
*
* INFORMATION: Given a pin and port, it attempts to toggle a led that
* 			   should be linked with the given combination.
**************************************************************************/
void ledToggle(uint16_t led_pin, GPIO_TypeDef* led_port);


/**************************************************************************
* BRIEF:	   Turns on a led.
*
* INFORMATION: Given a pin and port, the function tries to turn on a led.
**************************************************************************/
void ledOn(uint16_t led_pin, GPIO_TypeDef* led_port);


/**************************************************************************
* BRIEF: 	   Turns off a led.
*
* INFORMATION: Given a pin and port, the function tries to turn off a led.
**************************************************************************/
void ledOff(uint16_t led_pin, GPIO_TypeDef* led_port);


/**************************************************************************
* BRIEF: 	   Turns on a led that is inverted
*
* INFORMATION: Given a pin and port, the function tries to turn on a led.
**************************************************************************/
void ledOnInverted(uint16_t led_pin, GPIO_TypeDef* led_port);


/**************************************************************************
* BRIEF:	   Turns off a led that is inverted
*
* INFORMATION: Given a pin and port, the function tries to turn off a led.
**************************************************************************/
void ledOffInverted(uint16_t led_pin, GPIO_TypeDef* led_port);


/**************************************************************************
* BRIEF:	   Updates the warning leds in the system
*
* INFORMATION: Checks if any led warning should be active and if its the
* 			   case perform some led activities on a specific led
**************************************************************************/
void ledIndicatorsUpdate(void);


/**************************************************************************
* BRIEF: 	   Change the warning type of a led
*
* INFORMATION: Change the warning type of led given a warningId that is
*              obtained from ledWarnings_t.
**************************************************************************/
void ledSetWarningType(uint16_t warningId);


#endif /* DRIVERS_LED_H_ */
