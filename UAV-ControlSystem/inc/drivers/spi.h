
/***************************************************************************
 * NAME: spi.h                                                             *
 * AUTHOR: Lennart Eriksson                                                *
 * PURPOSE: Set up SPI communication and enable sending and receiving data *
 * INFORMATION:                                                            *
 *	The SPI is implemented using the spi_profile struct that contains all  *
 *	necessary data for transmission and reception of data.                 *
 *	                                                                       *
 * GLOBAL VARIABLES:                                                       *
 * Variable		Type		Description                                    *
 * --------		----		-----------                                    *
 * 																	       *
 **************************************************************************/

#ifndef DRIVERS_SPI_H_
#define DRIVERS_SPI_H_


#include "stm32f4xx.h"

// The struct to be initialized and used when sending or receiving data
typedef struct spi_profile
{
	SPI_HandleTypeDef *profile;	// The SPI handle
	GPIO_TypeDef *spi_nss_port; // The Slave Select Port
	uint16_t spi_nss;			//The Slave Select Pin
}spi_profile;

/****************************************************************************
 * BRIEF: initializes a slave on the selected SPI instance                  *
 * INFORMATION: Initialize a slave on the SPI. The SPI bus will only be     *
 *   initialized once only per bus and if several slaves are used on the    *
 *   same bus this function will return the struct with the pre initialized *
 *   SPI bus and the correct pin and port.                                  *
 ***************************************************************************/
bool spi_init(SPI_TypeDef *spi_instance, // The spi instance to be used i.e. SPI1 or SPI3 on the REVO
			  spi_profile *out_profile,  // The output profile to be used when sending and receiving data
			  uint32_t nss_pin,          // The Slave Select Pin
			  GPIO_TypeDef *nss_port);   // The port of the Slave Select Pin

/***********************************************************************
 * BRIEF: transmit data to the selected slave over SPI                  *
 * INFORMATION:                                                         *
 * data[0] = register                                                   *
 * data[1..n] = command                                                 *
 ***********************************************************************/
bool spi_transmit(spi_profile *profile,   // The profile to send data over
				  uint8_t *data,          // The data to be sent data[0] = register, data[1..n] = the command
				  uint32_t length,        // The length of the data, including the register byte
				  uint32_t timeout_ms);   // The timeout before giving up the try given in milliseconds


/***********************************************************************
 * BRIEF: request data from selected slave on specified register       *
 * INFORMATION: By passing in a register we can request specified      *
 *  data over the SPI bus                                              *
 ***********************************************************************/
bool spi_receive_reg_value(spi_profile *profile,  // The profile to send data over
						   uint8_t reg,			  // The register to get data from
						   uint8_t *data,         // The output data buffer
						   uint32_t length,       // The length of the buffer
						   uint32_t timeout_ms);  // The timeout before giving up the try given in milliseconds


/***********************************************************************
 * BRIEF: Receive data from the selected slave over SPI without        *
 *  requesting from what register i.e. just listen if a slave has      *
 *  something to say                                                   *
 * INFORMATION: By passing in a register we can request specified      *
 *  data over the SPI bus                                              *
 ***********************************************************************/
bool spi_receive(spi_profile *profile,  // The profile to send data over
			     uint8_t *data,         // The output data buffer
			     uint32_t length,       // The length of the buffer
			     uint32_t timeout_ms);  // The timeout before giving up the try given in milliseconds

#endif /* DRIVERS_SPI_H_ */
