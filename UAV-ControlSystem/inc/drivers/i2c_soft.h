/**************************************************************************
* NAME: 	   i2c_soft.h												  *
*																		  *
* AUTHOR: 	   Jonas Holmberg											  *
*                                                        				  *
* PURPOSE: 	   Used to communicate via I2C in a SW simulated manner.	  *
*																		  *
* INFORMATION: A software implementation of the I2C. It toggles the pins  *
* 			   that are used on and of to generate I2C messages.		  *
* 			                                                              *
* GLOBAL VARIABLES:                                                       *
* Variable        Type          Description                               *
* --------        ----          -----------                               *
***************************************************************************/

#ifndef DRIVERS_I2C_SOFT_H_
#define DRIVERS_I2C_SOFT_H_

#include "stm32f4xx.h"

/* Struct used to create a soft i2c handler */
typedef struct
{
	GPIO_TypeDef * i2c_Port;
	uint16_t i2c_scl_pin;
	uint16_t i2c_sda_pin;
}I2C_SOFT_handle_t;

/***********************************************************************
 * BRIEF:       Initializes the SW I2C.
 * INFORMATION: Initializes the SW I2C, needs to be done before any
 * 			    thing else.
 ***********************************************************************/
void i2c_soft_Init(I2C_TypeDef *i2c, I2C_SOFT_handle_t *out_profile);

/***********************************************************************
 * BRIEF:       Writes a message.
 * INFORMATION: Tries to write to an address. reg is the message that is
 *   			written to the addr. data is the size of the data that
 *   			is written.
 ***********************************************************************/
bool i2c_soft_Write(I2C_SOFT_handle_t *handle, uint8_t addr, uint8_t reg, uint8_t data);

/***********************************************************************
 * BRIEF:       Reads a message.
 * INFORMATION: Tries to read a message from addr. reg is the message
 * 				that says a read is desired. len is the length of the
 * 				message that should be read and buf is the buffer that
 * 				will store the read data.
 ***********************************************************************/
bool i2c_soft_Read(I2C_SOFT_handle_t *handle, uint8_t addr, uint8_t reg, uint8_t len, uint8_t *buf);

#endif /* DRIVERS_I2C_SOFT_H_ */
