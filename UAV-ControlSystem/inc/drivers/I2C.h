/***************************************************************************
* NAME: I2C.h                                                              *
* AUTHOR: Lennart Eriksson                                                 *
* PURPOSE: Enabole the I2C Communication channel on the Revo board         *
* INFORMATION:                                                             *
* This file initilizes the I2C communication that can be used by barometer *
* communication etc.                                                       *
*                                                                          *
* GLOBAL VARIABLES:                                                        *
* Variable        Type          Description                                *
* --------        ----          -----------                                *
***************************************************************************/

#ifndef DRIVERS_I2C_H_
#define DRIVERS_I2C_H_

#include "stm32f4xx.h"

/******************************************************************************
* BRIEF: Configure the I2C bus to be used                                     *
* INFORMATION: This function only implements I2C1 or I2C2 which are available *
* on the REVO board															  *
******************************************************************************/
bool i2c_configure(I2C_TypeDef* i2c,
                   I2C_HandleTypeDef* out_profile,
				   uint32_t my_address);

/******************************************************************************
* BRIEF: Get data over the I2C bus                                            *
* INFORMATION:                                                                *
* Since this system uses a 7 bit addressing mode we send the slave address    *
* in the first bytes 7 MSbs and then the LSb is set to 1 indicating that      *
* it is a receive command, after that the slave responds with a 1 bit ack and *
* the data is sent one byte at a time with an acknowledge in between          *
* every byte, as per the standard. Returns true if successful                 *
******************************************************************************/
bool i2c_receive(I2C_HandleTypeDef* profile,
				 uint8_t slave_address,
				 uint8_t* buffer,
				 uint32_t length);

/******************************************************************************
* BRIEF: Send data over the I2C bus                                           *
* INFORMATION:                                                                *
* Since this system uses a 7 bit addressing mode we send the slave address    *
* in the first bytes 7 MSbs and then the LSb is set to 0 indicating that      *
* it is a send command, after that the slave responds with a 1 bit ack and    *
* the data is sent one byte at a time with an acknowledge in between          *
* every byte, as per the standard. Returns true if successful                 *
******************************************************************************/
bool i2c_send(I2C_HandleTypeDef* profile,
			  uint8_t slave_address,
			  uint8_t* data,
			  uint32_t length);



/******************************************************************************
* BRIEF: Configure the I2C bus to be used                                     *
* INFORMATION: This function only implements I2C1 or I2C2 DMA which are       *
* available on the REVO board												  *
******************************************************************************/
bool i2c_configure_DMA(I2C_TypeDef *i2c,
		   I2C_HandleTypeDef *out_profile,
		   DMA_HandleTypeDef *out_rxDMA_profile,
		   DMA_HandleTypeDef *out_txDMA_profile,
		   uint32_t my_address);

#endif /* DRIVERS_I2C_H_ */




// ---------------------- I2C Working with the compas on the REVO board ------------------------------

/*
int main(void)
{

	// Initialize the Hardware Abstraction Layer
	HAL_Init();

	init_system();

	I2C_HandleTypeDef i2c_profile;


	//---- COMPAS WORKING ----
	i2c_configure(I2C1, &i2c_profile, 0x56);



    uint32_t address = 0b00011110;
    uint8_t start_request_1[2] = { 0b00000000, 0b01110000 };
    uint8_t start_request_2[2] = { 0b00000001, 0b10100000 };
    uint8_t start_request_3[2] = { 0b00000010, 0b00000000 };


    uint8_t request_data[1] = { 0b00000110 };
    uint8_t reset_pointer_data[1] = { 0b00000011 };
    uint8_t response_data[6] = { 0x0 };

	// This sequence starts the compass by first initializing it with the first 2 send
	// The third is there to say that the system should be continous communication
	i2c_send(&i2c_profile, address, &start_request_1, 2);
	i2c_send(&i2c_profile, address, &start_request_2, 2);
	i2c_send(&i2c_profile, address, &start_request_3, 2);

	// Delay for at least 6 ms for system startup to finish
	HAL_Delay(10);

	while (1)
	{
		i2c_send(&i2c_profile, address, &request_data, 1);
		i2c_receive(&i2c_profile, address, &response_data, 6);
		i2c_send(&i2c_profile, address, &reset_pointer_data, 1);

		//		  HAL_Delay(100);
		if(response_data[0] != 0)
			response_data[0] = 0;
	}
}
*/
