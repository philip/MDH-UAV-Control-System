/*
 * beeper.h
 *
 *  Created on: 14 nov. 2016
 *      Author: holmis
 */

#ifndef DRIVERS_BEEPER_H_
#define DRIVERS_BEEPER_H_

#include "stm32f4xx_revo.h"

void initBeeper(uint16_t led_pin, GPIO_TypeDef* led_port);

void busyWaitBeep(uint16_t beepTimeMs);

#endif /* DRIVERS_BEEPER_H_ */


