/*
 * arduino_com.h
 *
 *  Created on: 26 okt. 2016
 *      Author: Philip
 */

#ifndef DRIVERS_ARDUINO_COM_H_
#define DRIVERS_ARDUINO_COM_H_

#include "drivers/usart.h"

#define ARDUINO_BAUD 115200
#define ARDUINO_DMA_SIZE 20


/***********************************************************************
* BRIEF: RX packet structure from arduino com                          *
* INFORMATION: Contains the whole compass message 					   *
***********************************************************************/
typedef struct compass_data_t {
	uint8_t header  __attribute__((packed));
	int16_t x		__attribute__((packed));
	int16_t y		__attribute__((packed));
	int16_t z		__attribute__((packed));
	uint8_t crc		__attribute__((packed));
} compass_data_t;

/***********************************************************************
* BRIEF: RX packet structure from arduino com                          *
* INFORMATION: Contains the whole gps data message 					   *
***********************************************************************/
typedef struct gps_data_t {
	uint8_t header	    __attribute__((packed));
	float latitude	    __attribute__((packed));
	float longitude	    __attribute__((packed));
	uint8_t num_of_sats __attribute__((packed));
	uint8_t crc		    __attribute__((packed));
} gps_data_t;

/***********************************************************************
* BRIEF: RX packet structure from arduino com                          *
* INFORMATION: Contains the whole ping sensor data message			   *
***********************************************************************/
typedef struct ping_data_t {
	uint8_t header			__attribute__((packed));
	uint16_t distance_mm	__attribute__((packed));
	uint8_t crc				__attribute__((packed));
}ping_data_t;

/* An instance of the GPS data read from Arduino Com */
gps_data_t gps_data;

/* An instance of the compass data read from Arduino Com */
compass_data_t compass_data;

/* An instance of the ping data read from Arduino Com */
ping_data_t ping_data;

/***********************************************************************
* BRIEF: Initializes the UART for Arduino com                          *
* INFORMATION: A DMA Buffer starts storing the bytes received from RX  *
***********************************************************************/
void arduinoCom_init(USART_TypeDef* usart_inst);

/***********************************************************************
* BRIEF: Checks if new RX packet is available                          *
* INFORMATION: Is called by the scheduler                              *
***********************************************************************/
bool arduino_frame_available();

/***********************************************************************
* BRIEF: Updates "gps_data" and "compass_data"         				   *
* INFORMATION: Is called by the scheduler                              *
***********************************************************************/
void arduino_read();

/***********************************************************************
* BRIEF: Update the output sensor values and sends them to the Arduino *
* INFORMATION:             				                               *
***********************************************************************/
void arduino_send_sensor_values();

/***********************************************************************
* BRIEF: Check so that the heartbeat messages are comming with a 	   *
*  		 steady stream												   *
* INFORMATION: Check the last time a heart beat message was received   *
* 			   and checks against a pre defined time before declaring  *
* 			   the communication as dead							   *
***********************************************************************/
bool arduino_com_alive();

/***********************************************************************
* BRIEF: Set a color on a specific led on the neo ledstrip attached    *
*        to the arduino                                                *
* INFORMATION: Send a command with the led index and RGB color to the  *
* 			   Arduino                                                 *
***********************************************************************/
void arduino_set_led_color(uint8_t index, uint8_t r, uint8_t g, uint8_t b);

/***********************************************************************
* BRIEF: Tell the arduino that the FC system is OK and everything is   *
*        working                                                       *
* INFORMATION: Set the datavalue to 0xBA1DFACE for the led data        *
***********************************************************************/
void arduino_im_ok();

#endif /* DRIVERS_ARDUINO_COM_H_ */



//----------------------- Example code for the parser --------------------------

//arduinoCom_init(USART1);
//while (1) {
//	if(arduino_frame_available())
//		arduino_read();
//	HAL_Delay(15);
//	float lng = gps_data.longitude;
//	float lat = gps_data.latitude;
//}
