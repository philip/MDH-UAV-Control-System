 /**********************************************************************
 * NAME: motormix.h                                                    *
 * AUTHOR: Philip Johansson                                            *
 * PURPOSE: Combine the control system outputs to motor values         *
 * INFORMATION:                                                        *
 * Each control loop has its output which is a combination of error    *
 * in the input unit times some tuned constants. These outputs are     *
 * read by the mixer, combined and scaled into a valid output for      *
 * each motor.                                                         *
 *                                                                     *
 *                                                                     *
 * GLOBAL VARIABLES:                                                   *
 * Variable		Type		Description                                *
 * --------		----		-----------                                *
 * 																	   *
 **********************************************************************/


#ifndef DRIVERS_MOTORMIX_H_
#define DRIVERS_MOTORMIX_H_

#include "stm32f4xx_revo.h"

/* Amount of motors */
#define MOTOR_COUNT 10

// TODO: These are only temporary before merge with PID part
int16_t PID_Out[3];



// TODO: Implement in EEPROM
/* Important values to be used by the mixer */
typedef struct  {
	uint16_t minThrottle;			// Pulse when motors are stopped
	uint16_t maxThrottle; 			// Pulse when motors are maxed out
	uint16_t minCommand;			// Pulse when motors are running idle (Armed)
	uint16_t maxCommand;			// Max throttle allowed. Mixer can go higher than this though.
	uint16_t minCheck; 				// In Non Airmode: If throttle is below minCheck we set motors to minCommand
	uint16_t padding;
	bool pid_at_min_throttle; 		// When enabled PIDs are used at minimum throttle
	bool motorstop;					// If enabled motors will stop spinning at no throttle when Armed
	bool yaw_reverse_direction; 	// Default should be 1. Can be either -1 or 1
} mixerConfig_s;

/* Global mixerConfig to bee available to EEPROM */
extern mixerConfig_s mixerConfig;

/*Is set in motor mix and used in pidUAVcore and mix */
extern bool motorLimitReached;

/***********************************************************************
* BRIEF: The motormixer                                                *
* INFORMATION: Sums the output from all control loops and adapts the   *
* result to a suitable motor signal                                    *
***********************************************************************/
void mix();


#endif /* DRIVERS_MOTORMIX_H_ */
