/**************************************************************************
* NAME:  Johan G�rtner                                                    *
* PURPOSE:   Initialize driver for the PWM signal to certain outputs      *
* INFORMATION:                                                            *
* GLOBAL VARIABLES:                                                       *
* Variable        Type          Description                               *
* --------        ----          -----------                               *
* **************************************************************************/

#ifndef DRIVERS_PWM_H_
#define DRIVERS_PWM_H_

/**************************************************************************
* BRIEF:   pwmInit initializes a pwm signal to a certain pin output       *
* INFORMATION:
* Example - pwmInit(GPIOB, GPIO_PIN_0, TIM3, TIM_CHANNEL_3, 2000, 100);
* GPIO = selects GPIO output on processor
* pin = selects pin output on processor
* tim = Timer configuration
* Channel = Selects a channel for a certain timer, each timer as 4 channels
* period = Period of PWM signal
* pulse = the "duty cycle" of the pwm signal                                   *
**************************************************************************/
void pwmInit(GPIO_TypeDef * GPIO, uint16_t pin, TIM_TypeDef * tim, uint32_t Channel, uint16_t period, uint16_t pulse, uint32_t prescaler);

/**************************************************************************
* BRIEF:  setPwmPulse changes a certain pulse for an initialized pwm signal                                                              *
* INFORMATION: It's only possible to change the pwm output for a certain timer
* on a certain channel, not a pin output
* Example - setPwmPulse(TIM_CHANNEL_4, TIM3, 500);                                                         *
**************************************************************************/
void setPwmPulse(uint32_t Channel, TIM_TypeDef * tim, uint16_t newPulse);

/**************************************************************************
* BRIEF:  startPwm Activates a pwm signal for a certain timer on a certain channel
* INFORMATION:
* Example - startPwm(TIM_CHANNEL_4, TIM3);
**************************************************************************/
void startPwm(uint32_t Channel, TIM_TypeDef * tim);

/**************************************************************************
* BRIEF:  stopPwm Deactivates a pwm signal for a certain timer on a certain channel
* INFORMATION:
* Example - stopPwm(TIM_CHANNEL_4, TIM3);
**************************************************************************/
void stopPwm(uint32_t Channel, TIM_TypeDef * tim);

#endif /* DRIVERS_PWM_H_ */
