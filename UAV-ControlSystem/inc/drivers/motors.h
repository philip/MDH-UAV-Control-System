/*
 * motors.h
 *
 *  Created on: 15 sep. 2016
 *      Author: jgr12001
 */

#ifndef DRIVERS_MOTORS_H_
#define DRIVERS_MOTORS_H_

#define MAX_PULSE 1950

#include "stm32f4xx_revo.h"
/* Struct of motor output protocol */
typedef enum {
	PWM,
	Oneshot125
}motorOutput;

extern bool perfromMotorCalibration;

/**************************************************************************
* BRIEF: Initializing a motor (maps a pwm signal to a certain pin)        										*
* INFORMATION: The motor becomes active, Each motor configuration is changed in the "stm32f4xx_revo.h" file     *
* Example - pwmEnableMotor(MOTOR_1)                                                         					*
**************************************************************************/
void pwmEnableMotor(uint8_t motor, motorOutput motorOutput);

/**************************************************************************
* BRIEF: Enables all motors (maps a pwm signal to all pins which will be connected to a motor)                                                                 *
* INFORMATION: All motors become active
**************************************************************************/
void pwmEnableAllMotors(motorOutput motorOutput);

/**************************************************************************
* BRIEF: Deactivates a motor (Disables the pwm signal for the motor pin )
* INFORMATION:
* Example - pwmDeactivateMotor(MOTOR_1)
**************************************************************************/
void pwmDeactivateMotor(uint8_t motor);

/**************************************************************************
* BRIEF: If a motor is Deactivated (no pwm signal), this function will activate the motor to it's last state (the pwm signal will go back to is's last state)  *
* INFORMATION:
* Example - pwmActivateMotor(MOTOR_1)
**************************************************************************/
void pwmActivateMotor(uint8_t motor);

/**************************************************************************
* BRIEF: Deactivates all motors (Deactivate a pwm signal to all motor pins)                  *
* INFORMATION:																				 *
* Example - pwmDeactivateAllMotors()                                                         *
**************************************************************************/
void pwmDeactivateAllMotors(void);

/**************************************************************************
* BRIEF: Activates all motors (Activates a pwm signal to all motor pins to it's last state)*                                                             *
* INFORMATION:																			   *
* Example - pwmActivateAllMotors()                                                         *
**************************************************************************/
void pwmActivateAllMotors(void);

/**************************************************************************
* BRIEF: Change the speed of a certain motor if it's active                                             *
* INFORMATION: The speed is changes by switching the the pulse of a pwm signal							*
* Example - pwmAdjustSpeedOfMotor(MOTOR_2, 200)                                                         *
**************************************************************************/
void pwmAdjustSpeedOfMotor(uint8_t motor, uint16_t pulse);

/**************************************************************************
* BRIEF: Change the speed of a certain motor if it's active                                             									 *
* INFORMATION: The speed is change by switching the the Duty Cycle of a pwm signal (The Duty Cycle may only take values between 0 - 100 %)   *
* Example - pwmAdjustSpeedOfMotor(MOTOR_2, 50);                                                        										 *
**************************************************************************/
void pwmAdjustSpeedOfMotorDutyCycle(uint8_t motor, uint16_t DutyCycle);

/**************************************************************************
* BRIEF: 	   Calibrates the motors if it should                                             									 *
* INFORMATION: Will perform a sequence that will calibrate the motors. The
* 		       motors will only be calibrated if a boolean value that is �
* 		       checked inside have been set to true                                                        										 *
**************************************************************************/
void calibrateMotors();


#endif /* DRIVERS_MOTORS_H_ */
