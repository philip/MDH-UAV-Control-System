/**************************************************************************
* NAME: 	   barometer.h												  *
*																		  *
* AUTHOR: 	   Jonas Holmberg											  *
*                                                        				  *
* PURPOSE: 	   Used to provide an estimated altitude, in regards to the   *
*              lift of height that would represent zero meters in height. *
*																		  *
* INFORMATION: Using I2C to communicate with the barometer a pressure and *
*			   temperature value can be obtained. These values can then be*
*			   used to estimate an altitude. Note that this is not an     *
*			   altitude value relative to the ground underneath it. It is *
*			   relative to the position where the system was started from.*
*			   The start position of the system will indicate the zero    *
*			   height. It is that position and only that one which will   *
*			   be the compared height.									  *
* 			                                                              *
* GLOBAL VARIABLES:                                                       *
* Variable        Type          Description                               *
* --------        ----          -----------                               *
***************************************************************************/

#ifndef DRIVERS_BAROMETER_H_
#define DRIVERS_BAROMETER_H_

typedef enum {
	CALCSTATE_D2_CALCULATION = 0,	//Tell the sensor that we want to read D2
	CALCSTATE_D2_READ,				//Read D2 from the sensor
	CALCSTATE_D1_CALCULATION,		//Tell the sensor that we want to read D1
	CALCSTATE_D1_READ,				//Read D1 from the sensor
	CALCSTATE_CALCULATE_PTA			//preassure, temp, altidute calc
}calculationState;

/***********************************************************************
 * BRIEF:       Initializes the barometer.							   *
 * INFORMATION: Initializes the barometer and it needs to be called	   *
 *              before anything else when using the barometer.         *
 ***********************************************************************/
bool barometer_init();

/***********************************************************************
 * BRIEF:       Resets the barometer.								   *
 * INFORMATION: Resets the barometer needs to be called after the init.*
 * 				It will send a reset message over the I2C to the       *
 * 				barometer telling it that is should perform a reset.   *
 * 				This needs to be done or it wont be possible to read   *
 * 				data from the barometer.  							   *
 ***********************************************************************/
bool barometer_reset();

/***********************************************************************
 * BRIEF:	    Calculates the values of the preassure, temperature and*
 * 			    altitude.											   *
 * INFORMATION: This function needs to be called five times for the    *
 * 				data to be updated. This is because of some limitations*
 * 				and to ensure the schedulability of the system it needs*
 * 				to be divided. Firstly there is an inherit delay inside*
 * 				the barometer sensor. To get data from the barometer a *
 * 				message needs to be sent that tells the barometer to   *
 * 				prepare the data. This takes, depending on the amount  *
 * 				of sampling that is done up to 10 ms for the highest   *
 * 				amount of sampling. This also needs to be done two     *
 * 				times before that data can be calculated. Also since   *
 * 				the implementation uses a software I2C at the moment   *
 * 				because of some problems with the DMA implementation   *
 * 				the speed is not very high. Therefore sending several  *
 * 				messages and reading at the same time may take to long *
 * 				time and could cause the system to be unschedulable.   *
 * 				Because of this the function is divided into different *
 * 				cases:												   *
 * 					1: Prepare data.								   *
 * 					2: Read data.									   *
 * 					3: Prepare data.								   *
 * 					4: Read data.									   *
 * 					5: Calculate temperature, pressure and altitude.   *
 ***********************************************************************/
void barometer_CaclulateValues();

/***********************************************************************
 * BRIEF:	    Retrieves the previously calculated pressure.		   *
 * INFORMATION: Returns the last calculated pressure value. No         *
 *              calculation is performed here so calling this will give*
 *              the same value until a new calculation has been        *
 *              performed.											   *
 ***********************************************************************/
double barometer_GetCurrentPressure();

/***********************************************************************
 * BRIEF:	    Retrieves the previously calculated temperature.	   *
 * INFORMATION: Returns the last calculated temperature value. No      *
 *              calculation is performed here so calling this will give*
 *              the same value until a new calculation has been		   *
 *              performed.											   *
 ***********************************************************************/
double barometer_GetCurrentTemperature();

/***********************************************************************
 * BRIEF:	    Retrieves the previously calculated altitude.		   *
 * INFORMATION: Returns the last calculated altitude value. No		   *
 *              calculation is performed here so calling this will give*
 *              the same value until a new calculation has been		   *
 *              performed.										       *
 ***********************************************************************/
float barometer_GetCurrentAltitude();

/***********************************************************************
 * BRIEF:	    Gets the altitude based on the last number of values.   *
 * INFORMATION: Averages the value on the last few reading to get a more*
 * 			    accurate reading.									    *
 ***********************************************************************/
float barometer_GetCurrentAveragedtAltitude();

#endif /* DRIVERS_BAROMETER_H_ */
