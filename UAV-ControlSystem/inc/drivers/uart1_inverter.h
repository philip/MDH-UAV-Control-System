/*
 * uart1_inverter.h
 *
 *  Created on: 21 sep. 2016
 *      Author: Philip
 */
 /**********************************************************************
 * NAME: uart1_inverter.h                                              *
 * AUTHOR: Philip Johansson                                            *
 * PURPOSE: Flip USART inversion on USART0                             *
 * INFORMATION:                                                        *
 * This functionality exists only on USART0.                           *
 * Initialize with the desired value                                   *
 *	                                                                   *
 * GLOBAL VARIABLES:                                                   *
 * Variable		Type		Description                                *
 * --------		----		-----------                                *
 * 																	   *
 **********************************************************************/

#ifndef DRIVERS_UART1_INVERTER_H_
#define DRIVERS_UART1_INVERTER_H_

#pragma once

extern bool uart1_rx_inverter;

/***********************************************************************
* BRIEF: Init Inverter                                                 *
* INFORMATION: Must be ran before inversion can be activated.          *
* The "on" arg sets if the inverter should be initiated on or off      *
***********************************************************************/
void uart1_rx_inverter_init(bool on);

/***********************************************************************
* BRIEF: Set Inverter                                                  *
* INFORMATION: Does not need bo be ran unless the value is changed.    *
***********************************************************************/
void uart1_rx_inverter_set(bool on);


#endif /* DRIVERS_UART1_INVERTER_H_ */
