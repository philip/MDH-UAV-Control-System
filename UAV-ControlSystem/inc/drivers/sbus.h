 /**********************************************************************
 * NAME: sbus.h                                                        *
 * AUTHOR: Philip Johansson                                            *
 * PURPOSE: Read SBUS messages from RX                                 *
 * INFORMATION:                                                        *
 * The SBUS protocol writes most significant bit first. So 1 in binary *
 * which is 0000001 will be sent as 10000000.                          *
 * Parity: 		even                                                   *
 * Stops: 		2                                                      *
 * Start byte:	0x0F (follows 0x00)                                    *
 * Stop byte:	0x00                                                   *
 * Packet size:	25 bytes                                               *
 *	                                                                   *
 * GLOBAL VARIABLES:                                                   *
 * Variable		Type		Description                                *
 * --------		----		-----------                                *
 * 																	   *
 **********************************************************************/
#include <stdint.h>


#ifndef DRIVERS_SBUS_H_
#define DRIVERS_SBUS_H_

#define SBUS_BAUDRATE 100000
#define SBUS_FRAME_SIZE 25
#define SBUS_HEADER 0x0F
#define SBUS_FOOTER 0x00
#define USART1_SBUS_DMA_SIZE SBUS_FRAME_SIZE + 1 // sbus package is 176 bits (22 bytes)

#define SBUS_MAX_CHANNEL 18 // Including two digital
#define STICK_CHANNEL_COUNT 4
#define AUX_CHANNEL_COUNT 4
#define MAX_AUX_CHANNEL_COUNT (SBUS_MAX_CHANNEL - STICK_CHANNEL_COUNT)



#define PWM_RANGE_MIN 1000
#define PWM_RANGE_MAX 2000
#define PWM_RANGE_MIDDLE (PWM_RANGE_MIN + ((PWM_RANGE_MAX - PWM_RANGE_MIN) / 2)) // Should be 1500 default

#define PWM_PULSE_MIN 750	// minimum PWM pulse considered valid input
#define PWM_PULSE_MAX 2250	// maximum PWM pulse considered valid input

extern float rc_rate;

/***********************************************************************
* BRIEF: Failsafe scenarios of the RX                                  *
* INFORMATION: TODO: Implement use of these                            *
***********************************************************************/
typedef enum {
	RX_FAILSAFE_MODE_AUTO = 0,
	RX_FAILSAFE_MODE_HOLD,
	RX_FAILSAFE_MODE_SET,
	RX_FAILSAFE_MODE_INVALID,
} rxFailsafeChannelMode_e;


/***********************************************************************
* BRIEF: RX packet structure                                           *
* INFORMATION: Contains a whole SBUS message besides the footer        *
***********************************************************************/
typedef struct sbusFrame_s {
	/* Whole package is 25 bytes */

	/* SBUS Header is 0x0F (Byte 0) */
	uint8_t syncByte;

	/* 176 bits of data for channels (11 bits per channel * 16 channels) = 22 bytes (1-22) */

	/* Channel 1..4 are "sticks" */
	unsigned int chan1 	: 11;	// Elevator
	unsigned int chan2 	: 11;	// Aileron
	unsigned int chan3 	: 11;	// Throttle
	unsigned int chan4 	: 11;	// Rudder

	/* Channel 5.. are AUX channels */
	unsigned int chan5 	: 11;
	unsigned int chan6 	: 11;
	unsigned int chan7 	: 11;
	unsigned int chan8 	: 11;	// ARM switch

	/* Depending on define 4 or more channels are used */
	unsigned int chan9 	: 11;
	unsigned int chan10 : 11;
	unsigned int chan11 : 11;
	unsigned int chan12 : 11;
	unsigned int chan13 : 11;
	unsigned int chan14 : 11;
	unsigned int chan15 : 11;
	unsigned int chan16 : 11;

	/* Byte 23 contains all flags
	* Bit 7: digital channel 17 (0x80)
	* Bit 6: digital channel 18 (0x40)
	* Bit 5: frame lost (0x20)
	* Bit 4: failsafe activated (0x10)
	* Bit 0-3: n/a
	*/
	unsigned int flag_DChannel_17 : 1;
	unsigned int flag_DChannel_18 : 1;
	unsigned int flag_FrameLost : 1;
	unsigned int flag_Failsafe : 1;
	unsigned int flag_NA : 4;

	 /* Byte 24 - The EndByte is 0x00 for FrSky
	  * Not included in this struct
	  */

} __attribute__ ((__packed__)) sbusFrame_s;

/* these are scaled RC Inputs centered around 0 */
typedef struct rc_input_t {
	int16_t Roll;
	int16_t Pitch;
	int16_t Yaw;
	int16_t Throttle;
} rc_input_t;



/* This instance is read by the whole system and should contain actual RX data */
extern sbusFrame_s sbusChannelData;

/* these are scaled RC Inputs centered around 0 */
extern rc_input_t rc_input;

/***********************************************************************
* BRIEF: Initializes the UART for sbus                                 *
* INFORMATION: A DMA Buffer starts storing the bytes received from RX  *
* Sbus Inverter is activated                                           *
***********************************************************************/
void sbus_init();

/***********************************************************************
* BRIEF: Checks if new RX packet is available                          *
* INFORMATION: Is called by the scheduler                              *
***********************************************************************/
bool sbus_frame_available();

/***********************************************************************
* BRIEF: Updates "sbusChannelData"                                     *
* INFORMATION: Is called by the scheduler                              *
***********************************************************************/
void sbus_read();

/***********************************************************************
* BRIEF: 	   Give the value of a channel in "sbusChannelData"        *
* INFORMATION: Given a channel id the value of that channel will be	   *
* 			   returned.                             			       *
***********************************************************************/
int getChannelValue(sbusFrame_s frame, int id);

#endif /* DRIVERS_SBUS_H_ */
