/*
 * ADC.h
 *
 *  Created on: 13 sep. 2016
 *      Author: Philip
 */
 /**********************************************************************
 * NAME: adc.h                                                         *
 * AUTHOR: Philip Johansson                                            *
 * PURPOSE: Set up and read from ADC                                   *
 * INFORMATION:                                                        *
 *	How to use this driver is explained in page 107 of HAL driver      *
 *	Enable the ADC clock                                               *
 *	Enable the GPIO clock for the pin wanted                           *
 *	Configure the GPIO pin as analog input                             *
 *	Configure the ADC speed (prescaler/sampling time)                  *
 *	Enable continuous measurement mode                                 *
 *	                                                                   *
 *	Read more at: www.visualgdb.com/tutorials/arm/stm32/adc/           *
 *	                                                                   *
 * GLOBAL VARIABLES:                                                   *
 * Variable		Type		Description                                *
 * --------		----		-----------                                *
 * 																	   *
 **********************************************************************/

#ifndef DRIVERS_ADC_H_
#define DRIVERS_ADC_H_

#include "stm32f4xx.h"


/* Voltage and current scaling values */
struct
{
	uint32_t vcc_scale;
	uint32_t i_scale_left;
	uint32_t i_scale_right;

} adcScaleStruct_t;


/***********************************************************************
* BRIEF: Configuration of ADC                                          *
* INFORMATION: Also initializes                                        *
***********************************************************************/
void adc_configure();

/***********************************************************************
* BRIEF: Add the wanted channels to a list                             *
* INFORMATION: Not initialized here but later                          *
***********************************************************************/
void adc_pin_add(uint32_t adc_channel);

/***********************************************************************
* BRIEF: Read ADC channel                                              *
* INFORMATION: Returns a mean value from "ADB_BUFFER_LENGTH" samples.  *
***********************************************************************/
uint32_t adc_read(uint32_t adc_channel);

/***********************************************************************
* BRIEF: When ADC is configured this function starts the DMA sampling  *
* INFORMATION: Third arg is size. When its full it starts over.        *
***********************************************************************/
void adc_start();

#endif /* DRIVERS_ADC_H_ */
