#ifndef DRIVERS_COMPASS_H
#define DRIVERS_COMPASS_H



bool initialize_compass();

void calibrate_compass();

void calculate_heading();

extern int MagnetMax[3];
extern int MagnetMin[3];

#endif //DRIVERS_COMPASS_H
