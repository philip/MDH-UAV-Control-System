 /**********************************************************************
 * NAME:        failsafe_toggles.h                                     *
 * AUTHOR:      Jonas Holmberg                                         *
 * PURPOSE:     Give the ability to create boolean flags and failsafe  *
 * 				variables.                                	    	   *
 * INFORMATION: Create any boolean variable that should be able to be  *
 * 				used throughout the system. These can be boolean vales *
 * 				for standard checks, failsafes etc. These can also be  *
 * 				linked to a RC channel and value range wherein the     *
 * 				value will be set to one.							   *
 *	                                                                   *
 * GLOBAL VARIABLES:                                                   *
 * Variable		  Type		     Description                           *
 * --------		  ----		     -----------                           *
 * systemFlags	  boolFlags_t    Represents a bitfield of all the      *
 * 					 			 boolean values in the system.	       *
 * flagConfigArr  flags_Configuration_t Configuration values for any   *
 * 									    flag that should be changeable *
 * 									    from the RC controller.		   *
 **********************************************************************/

#ifndef DRIVERS_FAILSAFE_TOGGLES_H_
#define DRIVERS_FAILSAFE_TOGGLES_H_

#include "stdint.h"

//Macro functions
#define getFlagMaskValue(x)							(1 << x)

#define failSafe_vals_offset 						0		//offset for the fail safe values in the bitfield
#define boolean_vals_offset							3		//offset for the booleans values in the bitfield. Equals the amount of failsafe ids

/*If a new value is added to the bitfield these IDs must be reviewed and checkd so that they still are correct*/
//failsafe values
#define systemFlags_Failsafe_rcChannelInRange_id	0	+ failSafe_vals_offset
#define systemFlags_Failsafe_noRcReceived_id		1	+ failSafe_vals_offset
#define systemFlags_Failsafe_toManyMissedFrames_id	2	+ failSafe_vals_offset

//other flags
#define systemFlags_armed_id						0	+ boolean_vals_offset
#define systemFlags_flightmode_acceleromter_id	 	1	+ boolean_vals_offset
#define systemFlags_flightmode_barometer_id	 		2	+ boolean_vals_offset
#define systemFlags_flightmode_compass_id	 		3	+ boolean_vals_offset
#define systemFlags_flightmode_gps_id		 		4	+ boolean_vals_offset
#define systemFlags_mixerfullscale_id				5	+ boolean_vals_offset
#define systemFlags_mixerlowscale_id				6	+ boolean_vals_offset
#define systemFlags_flightMode_3_id					7	+ boolean_vals_offset
#define systemFlags_barometerIsCalibrated_id		8	+ boolean_vals_offset
#define systemFlags_AcceleromterIsCalibrated_id		9	+ boolean_vals_offset
/* Stick booleans */
#define systemFlags_throttleMax_id					10	+ boolean_vals_offset
#define systemFlags_stickLeft_id					11	+ boolean_vals_offset
#define systemFlags_stickRight_id					12	+ boolean_vals_offset
#define systemFlags_stickUp_id						13	+ boolean_vals_offset
#define systemFlags_stickDown_id					14	+ boolean_vals_offset
#define systemFlags_stickCenterH_id					15	+ boolean_vals_offset
#define systemFlags_stickCenterV_id					16	+ boolean_vals_offset
#define systemFlags_throttleLeft_id					17	+ boolean_vals_offset


/*Mask values for each of the flag values*/
//failsafe values
#define systemFlags_Failsafe_rcChannelInRange_mask		getFlagMaskValue(systemFlags_Failsafe_rcChannelInRange_id)
#define systemFlags_Failsafe_noRcReceived_mask			getFlagMaskValue(systemFlags_Failsafe_noRcReceived_id)
#define systemFlags_Failsafe_toManyMissedFrames_mask 	getFlagMaskValue(systemFlags_Failsafe_toManyMissedFrames_id)

//other flags
#define systemFlags_armed_mask							getFlagMaskValue(systemFlags_armed_id)
#define systemFlags_flightmode_acceleromter_mask		getFlagMaskValue(systemFlags_flightmode_acceleromter_id)
#define systemFlags_flightmode_barometer_mask	 		getFlagMaskValue(systemFlags_flightmode_barometer_id)
#define systemFlags_flightmode_compass_mask	 			getFlagMaskValue(systemFlags_flightmode_compass_id)
#define systemFlags_flightmode_gps_mask		 			getFlagMaskValue(systemFlags_flightmode_gps_id)
#define systemFlags_mixerfullscale_mask					getFlagMaskValue(systemFlags_mixerfullscale_id)
#define systemFlags_mixerlowscale_mask					getFlagMaskValue(systemFlags_mixerlowscale_id)
#define systemFlags_flightMode_3_mask					getFlagMaskValue(systemFlags_flightMode_3_id)
#define systemFlags_barometerIsCalibrated_mask			getFlagMaskValue(systemFlags_barometerIsCalibrated_id)
#define systemFlags_AcceleromterIsCalibrated_mask		getFlagMaskValue(systemFlags_AcceleromterIsCalibrated_id)
/* Stick booleans */
#define systemFlags_throttleMax_mask					getFlagMaskValue(systemFlags_throttleMax_id)
#define systemFlags_stickLeft_mask						getFlagMaskValue(systemFlags_stickLeft_id)
#define systemFlags_stickRight_mask						getFlagMaskValue(systemFlags_stickRight_id
#define systemFlags_stickUp_mask						getFlagMaskValue(systemFlags_stickUp_id)
#define systemFlags_stickDown_mask						getFlagMaskValue(systemFlags_stickDown_id)
#define systemFlags_stickCenterH_mask					getFlagMaskValue(systemFlags_stickCenterH_id)
#define systemFlags_stickCenterV_mask					getFlagMaskValue(systemFlags_stickCenterV_id)
#define systemFlags_throttleLeft_mask					getFlagMaskValue(systemFlags_throttleLeft_id)



//typedef for the boolean value
typedef unsigned int booleanValue_t;

/* Struct containing all the failsafe on boolean values in the system */
typedef union bitSetRegion
{
	struct
	{
		//fail-safe booleans
		booleanValue_t rcChannelInRange 		: 1;
		booleanValue_t noRcReceived				: 1;

		//Flag boleans that are not fail-safe
		booleanValue_t armed 					: 1;
		booleanValue_t acceleromter 			: 1;
		booleanValue_t barometer 				: 1;
		booleanValue_t compass 					: 1;
		booleanValue_t gps	 					: 1;
		booleanValue_t mixerfullscale			: 1;
		booleanValue_t mixerlowscale			: 1;
		booleanValue_t flightMode_3				: 1;
		booleanValue_t barometerIsCalibrated	: 1;
		booleanValue_t AcceleromterIsCalibrated : 1;
		/* Stick booleans */
		booleanValue_t throttleMax				: 1;
		booleanValue_t stickLeft				: 1;
		booleanValue_t stickRight				: 1;
		booleanValue_t stickUp					: 1;
		booleanValue_t stickDown				: 1;
		booleanValue_t stickCenterH				: 1;
		booleanValue_t stickCenterV				: 1;
		booleanValue_t throttleLeft				: 1;

	}bitField;
	uint64_t intRepresentation;
}boolFlags_t;

/* Stuct to define values that can be changed and manipulated by RC commands  */
typedef struct
{
	uint16_t minRange;			//minimum range value in a RC channel (uint16 to save space, increase if higher ranges are needed)
	uint16_t maxRange;			//maximum range value in a RC channel (uint16 to save space, increase if higher ranges are needed)
	uint8_t channelNumber;		//The RC channel number for the value
	uint8_t flagId;				//ID for the flag to be changed in the system
}flags_Configuration_t;

typedef enum
{
	FLAG_CONFIGURATION_ARM = 0,
	FLAG_CONFIGURATION_FLIGHTMODE_ACCELEROMETER,
	FLAG_CONFIGURATION_FLIGHTMODE_BAROMETER,
	FLAG_CONFIGURATION_FLIGHTMODE_COMPASS,
	FLAG_CONFIGURATION_FLIGHTMODE_GPS,
	FLAG_CONFIGURATION_MIXERFULLSCALE,
	FLAG_CONFIGURATION_MIXERLOWSCALE,
	FLAG_CONFIGURATION_FLIGHTMODE_3,
	/* Stick booleans */
	FLAG_CONFIGURATION_THROTTLEMAX,
	FLAG_CONFIGURATION_STICKLEFT,
	FLAG_CONFIGURATION_STICKRIGHT,
	FLAG_CONFIGURATION_STICKUP,
	FLAG_CONFIGURATION_STICKDOWN,
	FLAG_CONFIGURATION_STICKCENTERH,
	FLAG_CONFIGURATION_STICKCENTERV,
	FLAG_CONFIGURATION_THROTTLELEFT,


	//Counter
	FLAG_CONFIGURATION_COUNT
}flags_configuration_IDs_t;

/* Bitfield containing values for all the flags and failsafe booleans sin the system */
extern boolFlags_t systemFlags;

/* Array of flag configurations. These are values that can be set by RC. */
extern flags_Configuration_t flagConfigArr[FLAG_CONFIGURATION_COUNT];

/***********************************************************************
* BRIEF:	   Could be used to set start values for some values
* INFORMATION: Possible to set the values for any of the failsafes
***********************************************************************/
void initFlags();

/***********************************************************************
* BRIEF:       Process RC channels that should operate on some flags
* INFORMATION: Reads the value of the RC channels that and checks if
* 			   any of the channels should handle some flag value in the
* 			   system. If it should it will update its state.
***********************************************************************/
void flags_ProcessRcChannel(int rcChannel_ID, int value);

/***********************************************************************
* BRIEF:       Process RC channels that should operate on some flags
* INFORMATION: Reads the value of the RC channels that and checks if
* 			   any of the channels should handle some flag value in the
* 			   system. If it should it will update its state.
***********************************************************************/
void flags_ProcessRcChannel_Improved(uint8_t minChannel, uint8_t maxChannel);

/***********************************************************************
* BRIEF:       Set flag to true(1)
* INFORMATION: Given an id set that value to true
***********************************************************************/
void flags_Set_ID(int id);

/***********************************************************************
* BRIEF:       Set flag to false(0)
* INFORMATION: Given an id set that value to false
***********************************************************************/
void flags_Clear_ID(int id);

/***********************************************************************
* BRIEF:       Toggle a flag
* INFORMATION: Given an id changes the current value of a flag to its
* 			   opposite.
***********************************************************************/
void flags_Toggle_ID(int id);

/***********************************************************************
* BRIEF:	   Checks if a flag is set
* INFORMATION: Given an id value, check if that is set to true in the
* 			   bitfield
***********************************************************************/
bool flags_IsSet_ID(int id);

/***********************************************************************
* BRIEF:       Set flag to true(1)
* INFORMATION: Given a mask set that value to true
***********************************************************************/
void flags_Set_MASK(int mask);

/***********************************************************************
* BRIEF:       Set flag to false(0)
* INFORMATION: Given a mask set that value to false
***********************************************************************/
void flags_Clear_MASK(int mask);

/***********************************************************************
* BRIEF:       Toggle a flag
* INFORMATION: Given a mask changes the current value of a flag to its
* 			   opposite.
***********************************************************************/
void flags_Toggle_MASK(int mask);

/***********************************************************************
* BRIEF:	   Checks if a flag is set
* INFORMATION: Given a mask value, check if that is set to true in the
* 			   bitfield
***********************************************************************/
bool flags_IsSet_MASK(int mask);




#endif /* DRIVERS_FAILSAFE_TOGGLES_H_ */
