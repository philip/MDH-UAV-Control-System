/**************************************************************************
* NAME: filter.h                                                          *
* AUTHOR: Johan G�rtner                                                   *
*
* PURPOSE: This file contains filter functions                            *
* INFORMATION:                                                            *
* GLOBAL VARIABLES:                                                       *
* Variable        Type          Description                               *
* --------        ----          -----------                               *
*
* **************************************************************************/



#ifndef FLIGHT_FILTER_H_
#define FLIGHT_FILTER_H_


#define M_PIf  3.14159265358979323846f	/*Float number of pi*/

#include<stdio.h>
#include<stdint.h>

/*Struct of inputs to filter*/
typedef struct pt1Filter_s {
	float state;
	float RC;
	float dT;
} pt1Filter_t;

/**************************************************************************
* BRIEF: First order lowpass filter                                       *
* INFORMATION:                                                            *
**************************************************************************/
float pt1FilterApply4(pt1Filter_t *filter, float input, uint8_t f_cut, float dT);

#endif /* FLIGHT_FILTER_H_ */
