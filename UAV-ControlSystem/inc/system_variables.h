 /**********************************************************************
 * NAME: adc.h                                                         *
 * AUTHOR: Philip Johansson                                            *
 * PURPOSE: Set up and read from ADC                                   *
 * INFORMATION:                                                        *
 *	Here we gather common variables for the system as a whole          *
 *	                                                                   *
 * GLOBAL VARIABLES:                                                   *
 * Variable		Type		Description                                *
 * --------		----		-----------                                *
 * 																	   *
 **********************************************************************/

#ifndef SYSTEM_VARIABLES_H_
#define SYSTEM_VARIABLES_H_

#define EEPROM_SYS_VERSION 114

#define ADC_STATE
#include "stm32f4xx.h"


extern uint8_t pid_pitch_pk;







#endif /* SYSTEM_VARIABLES_H_ */
