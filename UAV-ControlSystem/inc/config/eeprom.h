 /**********************************************************************
 * NAME: eeprom.h                                                      *
 * AUTHOR: Philip Johansson                                            *
 * PURPOSE: Virtual EEPROM driver                                      *
 * INFORMATION:                                                        *
 * This file mainly has two functions of which one brings the settings *
 * from EEPROM and the other stores them                               *
 *	                                                                   *
 * All values we want in EEPROM have to be added to the EEPROM_ID_t    *
 * struct as well as defined in eeprom_Arr with a pointer to the data  *
 *                                                                     *
 * GLOBAL VARIABLES:                                                   *
 * Variable		Type		Description                                *
 * --------		----		-----------                                *
 * 																	   *
 **********************************************************************/

#ifndef CONFIG_EEPROM_H_
#define CONFIG_EEPROM_H_

#include "stm32f4xx.h"
#include "stm32f4xx_revo.h"

/* Macros used to check if a define has no value assigned  */
#define DO_EXPAND(VAL)  VAL ## 1
#define EXPAND(VAL)     DO_EXPAND(VAL)

/* Defines the base addresses for each sector of the flash memory  */
#define FLASH_SECTOR_0_START_ADDRESS 			0x08000000			//Secotr 0
#define FLASH_SECTOR_1_START_ADDRESS			0x08004000			//Secotr 1
#define FLASH_SECTOR_2_START_ADDRESS			0x08008000			//Secotr 2
#define FLASH_SECTOR_3_START_ADDRESS			0x0800C000			//Secotr 3
#define FLASH_SECTOR_4_START_ADDRESS			0x08010000			//Secotr 4
#define FLASH_SECTOR_5_START_ADDRESS			0x08020000			//Secotr 5
#define FLASH_SECTOR_6_START_ADDRESS			0x08040000			//Secotr 6
#define FLASH_SECTOR_7_START_ADDRESS			0x08060000			//Secotr 7
#define FLASH_SECTOR_8_START_ADDRESS			0x08080000			//Secotr 8
#define FLASH_SECTOR_9_START_ADDRESS			0x080A0000			//Secotr 9
#define FLASH_SECTOR_10_START_ADDRESS			0x080C0000			//Secotr 10
#define FLASH_SECTOR_11_START_ADDRESS			0x080E0000			//Secotr 11

/* Define size gor each sector in bytes */
#define FLASH_SIZE_16_KB						0x3FFF				//16 Kb
#define FLASH_SIZE_64_KB						0xFFFF				//64 Kb
#define FLASH_SIZE_128_KB						1FFFF				//128 kb

/* ToDo: Could add a check for this in the system. If the ammount of data that
 * should be stored in the selected sector is larger than the size
 * it could produce an error */
#define FLASH_SECTOR_0_SIZE						FLASH_SIZE_16_KB
#define FLASH_SECTOR_1_SIZE						FLASH_SIZE_16_KB
#define FLASH_SECTOR_2_SIZE						FLASH_SIZE_16_KB
#define FLASH_SECTOR_3_SIZE						FLASH_SIZE_16_KB
#define FLASH_SECTOR_4_SIZE						FLASH_SIZE_64_KB
#define FLASH_SECTOR_5_SIZE						FLASH_SIZE_128_KB
#define FLASH_SECTOR_6_SIZE						FLASH_SIZE_128_KB
#define FLASH_SECTOR_7_SIZE						FLASH_SIZE_128_KB
#define FLASH_SECTOR_8_SIZE						FLASH_SIZE_128_KB
#define FLASH_SECTOR_9_SIZE						FLASH_SIZE_128_KB
#define FLASH_SECTOR_10_SIZE					FLASH_SIZE_128_KB
#define FLASH_SECTOR_11_SIZE					FLASH_SIZE_128_KB


/* Defines where emulated EEPROM starts from - OBS! Also defined in LinkerScript.id
 *
 * NOTE - Use sector 7 if it should work on both the revo and nucleo boards
 * 		  If only to be used on the revo board use sector 11.
 *
 * 		  Important - If sectoer 7 is used, make sure the ROM only uses 0-6 in the
 * 		  			  LinkerScript.id file*/
#define EEPROM_BASE_ADDR						FLASH_SECTOR_7_START_ADDRESS
#if !defined(EEPROM_BASE_ADDR) || (EXPAND(EEPROM_BASE_ADDR) == 1)
#error "No valid sector address have been defined for the EEPROM"
#endif

/* The sector to delete based on what is used as EEPROM. When the EEPROM should
 * be rewritten it needs to delete the current values in the EEPROM. This is
 * Based on what section is defined to be used for the EEPROM. */
#if EEPROM_BASE_ADDR == FLASH_SECTOR_0_START_ADDRESS
#define EEPROM_SECTOR_ERASE 		  FLASH_SECTOR_0
#define EEPROM_SECTOR_SIZE			  FLASH_SECTOR_0_SIZE
#elif EEPROM_BASE_ADDR == FLASH_SECTOR_1_START_ADDRESS
#define EEPROM_SECTOR_ERASE 		  FLASH_SECTOR_1
#define EEPROM_SECTOR_SIZE			  FLASH_SECTOR_1_SIZE
#elif EEPROM_BASE_ADDR == FLASH_SECTOR_2_START_ADDRESS
#define EEPROM_SECTOR_ERASE 		  FLASH_SECTOR_2
#define EEPROM_SECTOR_SIZE			  FLASH_SECTOR_2_SIZE
#elif EEPROM_BASE_ADDR == FLASH_SECTOR_3_START_ADDRESS
#define EEPROM_SECTOR_ERASE 		  FLASH_SECTOR_3
#define EEPROM_SECTOR_SIZE			  FLASH_SECTOR_3_SIZE
#elif EEPROM_BASE_ADDR == FLASH_SECTOR_4_START_ADDRESS
#define EEPROM_SECTOR_ERASE 		  FLASH_SECTOR_4
#define EEPROM_SECTOR_SIZE			  FLASH_SECTOR_4_SIZE
#elif EEPROM_BASE_ADDR == FLASH_SECTOR_5_START_ADDRESS
#define EEPROM_SECTOR_ERASE 		  FLASH_SECTOR_5
#define EEPROM_SECTOR_SIZE			  FLASH_SECTOR_5_SIZE
#elif EEPROM_BASE_ADDR == FLASH_SECTOR_6_START_ADDRESS
#define EEPROM_SECTOR_ERASE 		  FLASH_SECTOR_6
#define EEPROM_SECTOR_SIZE			  FLASH_SECTOR_6_SIZE
#elif EEPROM_BASE_ADDR == FLASH_SECTOR_7_START_ADDRESS
#define EEPROM_SECTOR_ERASE 		  FLASH_SECTOR_7
#define EEPROM_SECTOR_SIZE			  FLASH_SECTOR_7_SIZE
#elif EEPROM_BASE_ADDR == FLASH_SECTOR_8_START_ADDRESS
#define EEPROM_SECTOR_ERASE 		  FLASH_SECTOR_8
#define EEPROM_SECTOR_SIZE			  FLASH_SECTOR_8_SIZE
#elif EEPROM_BASE_ADDR == FLASH_SECTOR_9_START_ADDRESS
#define EEPROM_SECTOR_ERASE 		  FLASH_SECTOR_9
#define EEPROM_SECTOR_SIZE			  FLASH_SECTOR_9_SIZE
#elif EEPROM_BASE_ADDR == FLASH_SECTOR_10_START_ADDRESS
#define EEPROM_SECTOR_ERASE 		  FLASH_SECTOR_10
#define EEPROM_SECTOR_SIZE			  FLASH_SECTOR_10_SIZE
#elif EEPROM_BASE_ADDR == FLASH_SECTOR_11_START_ADDRESS
#define EEPROM_SECTOR_ERASE 		  FLASH_SECTOR_11
#define EEPROM_SECTOR_SIZE			  FLASH_SECTOR_11_SIZE
#else
#error "No valid sector address have been decided for the EEPROM"
#endif


/* Defines for what type of value something is, system, profile etc */
#define EEPROM_VALUE_TYPE_SYSTEM		1
#define EEPROM_VALUE_TYPE_PROFILE		2
#define EEPROM_VALUE_TYPE_HEADER		3
#define EEPROM_VALUE_TYPE_FOOTER		4

/* The size in bytes of the profile buffers. The error handler will be called if this is too small */
#define EEPROM_PROFILE_SIZE 200

/* The profiles one can choose from */
typedef enum {
	PROFILE_1 = 1,
	PROFILE_2,
	PROFILE_3
} ACTIVE_PROFILE;

/* List of all header EEPROM values */
typedef enum {
	EEPROM_VERSION = 0,

	/* Counts the amount of system settings */
	EEPROM_HEADER_COUNT
} EEPROM_HEADER_ID_t;

/* List of all system EEPROM values */
typedef enum {
	EEPROM_ACTIVE_PROFILE = 0,
	EEPROM_ADC_SCALES,
	EEPROM_UART1_RX_INV,

	/* Motor calibrate bool */
	EEPROM_MOTORCALIBRATE,

	/* Period values for tasks */
	EEPROM_PERIOD_SYSTEM,
	EEPROM_PERIOD_GYROPID,
	EEPROM_PERIOD_ACCELEROMETER,
	EEPROM_PERIOD_ATTITUDE,
	EEPROM_PERIOD_RX,
	EEPROM_PERIOD_RX_CLI,
	EEPROM_PERIOD_SERIAL,
	EEPROM_PERIOD_BATTERY,
	EEPROM_PERIOD_ARDUINO,
#ifdef BARO
	EEPROM_PERIOD_BARO,
#endif
#ifdef COMPASS
	EEPROM_PERIOD_COMPASS,
#endif
#ifdef GPS
	EEPROM_PERIOD_GPS,
#endif
#ifdef SONAR
	EEPROM_PERIOD_SONAR,
#endif
#if defined(BARO) || defined(SONAR)
	EEPROM_PERIOD_ALTITUDE,
#endif
#ifdef BEEPER
	EEPROM_PERIOD_BEEPER,
#endif

	/* Motormix values */
	EEPROM_MOTORMIX_CONFIG,

	/* Flags eeprom values */
	EEPROM_FLAG_ARM,
	EEPROM_FLAG_FLIGHTMODE_ACCELEROMETER,
	EEPROM_FLAG_FLIGHTMODE_BAROMETER,
	EEPROM_FLAG_FLIGHTMODE_COMPASS,
	EEPROM_FLAG_FLIGHTMODE_GPS,
	EEPROM_FLAG_MIXERFULLSCALE,
	EEPROM_FLAG_MIXERLOWSCALE,
	EEPROM_FLAG_FLIGHTMODE_3,

	/* accel calibration values */
	EEPROM_FLAG_ACCELTUNE_OFFSET_X,
	EEPROM_FLAG_ACCELTUNE_OFFSET_Y,
	EEPROM_FLAG_ACCELTUNE_OFFSET_Z,

	/* accel calibration fine tune values */
	EEPROM_FLAG_ACCELTUNE_FINE_ROLL,
	EEPROM_FLAG_ACCELTUNE_FINE_PITCH,

	/* Compass calibration */
	EEPROM_FLAG_COMPASSTUNE_MAGNET_MAX,
	EEPROM_FLAG_COMPASSTUNE_MAGNET_MIN,

	/* Counts the amount of system settings */
	EEPROM_SYS_COUNT
} EEPROM_SYS_ID_t;

/* List of all profile EEPROM values */
typedef enum {
	EEPROM_PID_GYRO,
	EEPROM_PID_ACCELEROMETER,
	EEPROM_PID_COMPASS,
	EEPROM_PID_BAROMETER,

	/* Counts the amount of settings in profile */
	EEPROM_PROFILE_COUNT
} EEPROM_PROFILE_ID_t;

/* List of all footer EEPROM values */
typedef enum {
	EEPROM_CRC = 0,

	/* Counts the amount of system settings */
	EEPROM_FOOTER_COUNT
} EEPROM_FOOTER_ID_t;

/***********************************************************************
* BRIEF: Writes EEPROM data to FLASH. Requires the next active profile *
* to be selected (current profile can be used as input)                *
* INFORMATION: passes all data directly from where they are defined    *
***********************************************************************/
void writeEEPROM(ACTIVE_PROFILE new_active_profile);

/***********************************************************************
* BRIEF: Writes EEPROM data to FLASH without the need of setting next  *
* active profile                                                       *
* INFORMATION: Keeps the current profile active                        *
***********************************************************************/
void saveEEPROM();

/***********************************************************************
* BRIEF: Reads EEPROM data from FLASH                                  *
* INFORMATION: passes all data directly to where they are defined      *
***********************************************************************/
bool readEEPROM();

/***********************************************************************
* BRIEF: Choose a profile between 1 .. 3                               *
* INFORMATION: The current changes will be saved                       *
***********************************************************************/
void setActiveProfile(ACTIVE_PROFILE profile);

/***********************************************************************
* BRIEF: Writes current profile values to all EEPROM profiles          *
* INFORMATION: used when EEPROM is corrupt or there is a version       *
* mismatch                                                             *
***********************************************************************/
void resetEEPROM(void);

/***********************************************************************
* BRIEF: Erases the eeprom and reboots						           *
* INFORMATION: Erases the eeprom sector and reboots the system so that *
* the default values set in the code will replace the values.		   *
***********************************************************************/
void defaultEEPROM(void);

/***********************************************************************
* BRIEF: Gets the address of a value from an id                        *
* INFORMATION: Gets the address of a dataPtr based on a given ID.
* 			   The val "dataType" will specify if it is a system of a  *
* 			   profile variable.									   *
***********************************************************************/
void * getDataAddresFromID(uint16_t id, uint8_t dataType);

/***********************************************************************
* BRIEF: Gets the current profile value	                               *
* INFORMATION: Will return a simple int value of the active profile    *
***********************************************************************/
int getActiveProfile();

#endif /* CONFIG_EEPROM_H_ */
