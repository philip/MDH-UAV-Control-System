 /**********************************************************************
 * NAME:        cli.h                                                  *
 * AUTHOR:      Jonas Holmberg                                         *
 * PURPOSE:     Provide the ability to change some values by use of    *
 * 				command line interface.                                *
 * INFORMATION: By some predefined commands values can be changed in   *
 * 				the system by writing in a serial communication        *
 * 				terminal that runs on usart. It also has the ability   *
 * 				to save the value changes to EEPROM and reset all	   *
 * 				values. The system can also be reseted and rebooted.   *
 *	                                                                   *
 * GLOBAL VARIABLES:                                                   *
 * Variable		Type		   Description                             *
 * --------		----		   -----------                             *
 * cliUsart		Usart_profile  The handler to the usart used by the cli*																   *
 **********************************************************************/

#ifndef CONFIG_CLI_H_
#define CONFIG_CLI_H_

#include "drivers/usart.h"

/* The handler to the usart that is used by the CLI */
extern usart_profile cliUsart;

/***********************************************************************
* BRIEF: 	   Call will start the cli loop                            *
* INFORMATION: Should be invoked if you want to start the cli loop,	   *
* 			   will try to read instructions until told to stop.       *
* 			   Will not be part of the regular scheduler loop.		   *
***********************************************************************/
void cliRun();

/***********************************************************************
* BRIEF: 	   Initiates the CLI                                  	   *
* INFORMATION: The function initiates the CLI. To do this it will need *
* 			   a usart that it should receive its commands from.	   *
***********************************************************************/
void cliInit(USART_TypeDef* usart);

/***********************************************************************
* BRIEF: 	   Function that checks if the CLI asigned usart has a new *
* 			   message that can be read. 						   	   *
* INFORMATION: If there is a new message in the designated usart the   *
* 		   	   function will return true, otherwise false.			   *
***********************************************************************/
bool cliHasMessage();

/***********************************************************************
* BRIEF:	   Reads a cahracter from the usart and checks if it       *
* 		       is the start character for the CLI.					   *
* INFORMATION: Will read a character from the usart and compare if it  *
*              is the character that needs to be read to start the CLI *
***********************************************************************/
bool cliShouldRun();


#endif /* CONFIG_CLI_H_ */

/* CHECKLIST - How to add new command in CLI                          */
/* A: First add the value that should be changed to the EEPROM
 *
 * 	  1: Create a EEPROM enum id for the value in eeprom.h. Make sure
 * 	     to place within the correct enum container system or profile.
 *
 * 	  2: In eeprom.c add two values to the correct array for the value
 * 	     profile or system. The two values should be the pointer to the
 * 	     value to store in eeprom and the size of said value.
 *
 * 	  3: Now the value should be storable in the EEPROM.
 *
 * B: Create the actual command in the CLI.
 *
 * 	  1: First create an enum id for the CLI command in the command_Ids_t
 * 	     contained in the cli.c.
 *
 * 	  2: When the id is created it represents an array value in the
 * 	     commandTable, an array of cliCommandConfig_t.
 *
 * 	  3: Initialize the values of the cliCommandConfig_t for the newly
 * 	     created command id. Provide in this order:
 * 	     - The name of the command when typing it in CLI.
 * 	     - The created Command id.
 * 	     - The EEPROM id to the value the command shoud operate on.
 * 	     - The type of EEPROM value syetem, profile, etc...
 * 	     - The offset of the value at the given eeprom address, usable if
 * 	       the value stored in the EEPROM is for example a struct and
 * 	       the value to be accessed is for example 4 bytes from the start
 * 	       of the struct. In that case this value would be set to 4.
 * 	     - The type of the value the command should operate on. Uint8,
 * 	       uint16, uint 32 etc...
 * 	     - The min and maximum value that can be given using the command.
 *
 * C: When both these things have been done the command should have been
 * 	  added to the CLI and it should operate on the correct value in the
 * 	  system that is also stored in the EEPROM.
 *
 * D: NOTE, be careful when setting up the CLI command, any small mistake
 *    in the command can cause strange errors that can be hard to track
 *    down at a later stage. Therefore make sure that the command works
 *    as intended and double check all the values for the command and see
 *    that they are what they should be.
 *  */

