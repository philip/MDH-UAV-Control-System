/*
 * math_helpers.h
 *
 *  Created on: 21 sep. 2016
 *      Author: holmis
 */

#ifndef UTILITIES_MATH_HELPERS_H_
#define UTILITIES_MATH_HELPERS_H_


#define M_PIf       3.14159265358979323846f

#define RAD    (M_PIf / 180.0f)

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define ABS(x) ((x) > 0 ? (x) : -(x))


#endif /* UTILITIES_MATH_HELPERS_H_ */
