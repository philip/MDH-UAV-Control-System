/**************************************************************************
* NAME: 	   tasks.h													  *
*																	      *
* AUTHOR:  	   Jonas Holmberg											  *
*                                                            			  *
* PURPOSE:     Defining the the scheduler to be used in the system to     *
*              organize the runtime for the tasks in the system based on  *
*              priority.     											  *
* 		   																  *
* INFORMATION: Adds the initial task values for each task that can be in  *
* 			   the system, in the c file. In the h file functions that    *
* 			   when called will* perform the action of its corresponding  *
* 			   task.							  					      *
*                                                            			  *
* GLOBAL VARIABLES:                                                       *
* Variable        Type          Description                               *
* --------        ----          -----------                               *
***************************************************************************/

#ifndef SCHEDULER_TASKS_H_
#define SCHEDULER_TASKS_H_

#include <stdint.h>

#define GetUpdateRateHz(x)		(1000000 / x)		//x is the desired hz value given in micro seconds
#define GetUpdateRateMs(x)		(x*1000)			//X is the desired ms value given as micro seconds
#define GetUpdateRateUs(x)		(x)					//X is the desired us value given as micro seconds (Its the same, used for clarification)
#define GetUpdateRateSeconds(x)	(x * 1000000)		//X is the desired second value given as micro seconds

//All the task functions in the system, one for each task
void systemTaskSystem(void);
void systemTaskGyroPid(void);
void systemTaskAccelerometer(void);
void systemTaskAttitude(void);
void systemTaskRx(void);
bool systemTaskRxCheck(uint32_t currentDeltaTime);
void systemTaskRxCli(void);
bool systemTaskRxCliCheck(uint32_t currentDeltaTime);
void systemTaskSerial(void);
void systemTaskBattery(void);
void systemTaskArduino(void);
void systemTaskBaro(void);
void systemTaskCompass(void);
void systemTaskGps(void);
void systemTaskSonar(void);
void systemTaskAltitude(void);
void systemTaskBeeper(void);

//Tasks used only for testing purposes
void systemTaskDebug_1(void);
void systemTaskDebug_2(void);
void systemTaskDebug_3(void);

#endif /* SCHEDULER_TASKS_H_ */
