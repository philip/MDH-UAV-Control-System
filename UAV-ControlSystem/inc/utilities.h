/*
 * utilities.h
 *
 *  Created on: 16 sep. 2016
 *      Author: Philip
 */
 /**********************************************************************
 * NAME: utilities.c                                                   *
 * AUTHOR: Philip Johansson                                            *
 * PURPOSE: Set up and read from ADC                                   *
 * INFORMATION:                                                        *
 *	Here we gather usable functions by the whole system                *
 *	                                                                   *
 * GLOBAL VARIABLES:                                                   *
 * Variable		Type		Description                                *
 * --------		----		-----------                                *
 * 																	   *
 **********************************************************************/

#ifndef UTILITIES_H_
#define UTILITIES_H_

#include <stdint.h>
#include "stm32f4xx_it.h"

#define maxStringSize_CLI  		100		//Max sting size used for the messages in the CLI

#define ABS_FLOAT(x)	(((x) < 0)? -(x): (x))

typedef char typeString[maxStringSize_CLI];
typedef struct typeStringArr { char val[maxStringSize_CLI]; } typeStringArr;

/***********************************************************************
* BRIEF:       Calculates the length of a string(char[])
* INFORMATION: Calculates the number of characters in a char arr
***********************************************************************/
uint16_t calculateStringLength (const char * src, int maxSize);

/***********************************************************************
* BRIEF:       Gives the length of a typestring						   *
* INFORMATION: Calculates the number of characters in a typestring.    *
* 			   Essentially it calculates the number of strings in an   *
* 			   string array.                						   *
***********************************************************************/
uint32_t calculateTypeStringLength(typeString arr[], int maxSize);

/***********************************************************************
* BRIEF:	   Checks if a string consists of numbers
* INFORMATION: Given a string of numbers it will check if it is a number
* 			   by comparing to the ascii table
***********************************************************************/
bool isStringNumbers(char * msg);

/***********************************************************************
* BRIEF:	   Parses a string of numbers to int
* INFORMATION: Parses a string of numbers to a 64-bit integer.
***********************************************************************/
uint64_t parseToInt64(char * msg);

/***********************************************************************
* BRIEF:       Tries to split a string on whitespace
* INFORMATION: Splits a string on whitespace and places it in dst
***********************************************************************/
bool splitStringWhitespace(char * src, typeString dst[], uint16_t stringSize, uint16_t arraySize);

/***********************************************************************
* BRIEF:       Finds previous whitespace in a string
* INFORMATION: Given a string and start index it will try to find
*              one whitespace counting downwards.
***********************************************************************/
int FindPreviousWhiteSpaceIndex(char * msg, int startIndex);

/***********************************************************************
* BRIEF: Sums elements of array until index of second arg              *
* INFORMATION: Returns the sum                                         *
***********************************************************************/
uint32_t accumulate(uint32_t list[], int length);

/***********************************************************************
* BRIEF: A function that can be called on exceptions                   *
* INFORMATION: If an exception happens its easier to find it in an     *
* infinite loop                                                        *
***********************************************************************/
void Error_Handler(void);

/**************************************************************************
* BRIEF: Constrain float values within a defined limit                    *
* INFORMATION: Used in PID loop to limit values                           *
**************************************************************************/
float constrainf(float amt, int low, int high);


uint8_t reverse(uint8_t byte);

int16_t constrain(int16_t value, int16_t min, int16_t max);

#endif /* UTILITIES_H_ */
