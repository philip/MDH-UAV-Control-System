/*
 * stm32f4xx_revo.h
 *
 *  Created on: 8 sep. 2016
 *      Author: len12007
 */

#ifndef UTILITIES_STM32F4XX_REVO_STM32F4XX_REVO_H_
#define UTILITIES_STM32F4XX_REVO_STM32F4XX_REVO_H_

#ifdef __cplusplus
 extern "C" {
#endif

 /* Includes ------------------------------------------- */
#include "stm32f4xx_hal.h"

 //Code here

//Leds? (taken from betaflight REVO h file)
#define Led0_PIN					GPIO_PIN_5		//blue
#define Led0_GPIO_PORT				GPIOB
#define Led1						GPIO_PIN_4		//yellow
#define Led1_GPIO_PORT				GPIOB


 //Servo out, To be used for motors?
#define SERVO_OUT_1_PIN				GPIO_PIN_0
#define SERVO_OUT_1_GPIO_PORT		GPIOB
#define SERVO_OUT_2_PIN				GPIO_PIN_1
#define SERVO_OUT_2_GPIO_PORT		GPIOB
#define SERVO_OUT_3_PIN				GPIO_PIN_3
#define SERVO_OUT_3_GPIO_PORT		GPIOA
#define SERVO_OUT_4_PIN				GPIO_PIN_2
#define SERVO_OUT_4_GPIO_PORT		GPIOA
#define SERVO_OUT_5_PIN				GPIO_PIN_1
#define SERVO_OUT_5_GPIO_PORT		GPIOA
#define SERVO_OUT_6_PIN				GPIO_PIN_0
#define SERVO_OUT_6_GPIO_PORT		GPIOA


/* Flex I/O 1-10 */
//#define FLEX_IO_1_PIN				GND
//#define FLEX_IO_1_PORT			GND
//#define FLEX_IO_2_PIN				VCC_UNREG
//#define FLEX_IO_2_PORT			VCC_UNREG
#define FLEX_IO_3_PIN				GPIO_PIN_12
#define FLEX_IO_3_PORT				GPIOB
#define FLEX_IO_4_PIN				GPIO_PIN_13
#define FLEX_IO_4_PORT				GPIOB
#define FLEX_IO_5_PIN				GPIO_PIN_14
#define FLEX_IO_5_PORT				GPIOB
#define FLEX_IO_6_PIN				GPIO_PIN_15
#define FLEX_IO_6_PORT				GPIOB
#define FLEX_IO_7_PIN				GPIO_PIN_6
#define FLEX_IO_7_PORT				GPIOC
#define FLEX_IO_8_PIN				GPIO_PIN_7
#define FLEX_IO_8_PORT				GPIOC
#define FLEX_IO_9_PIN				GPIO_PIN_8
#define FLEX_IO_9_PORT				GPIOC
#define FLEX_IO_10_PIN				GPIO_PIN_9
#define FLEX_IO_10_PORT				GPIOC


/* Communication */
#define USE_USART1
#define USART1_RX_PIN				GPIO_PIN_10
#define USART1_RX_PORT				GPIOA
#define USART1_TX_PIN				GPIO_PIN_9
#define USART1_TX_PORT				GPIOA

#define USE_USART3
#define USART3_RX_PIN           	GPIO_PIN_11
#define USART3_RX_PORT           	GPIOB
#define USART3_TX_PIN            	GPIO_PIN_10
#define USART3_TX_PORT           	GPIOB

#define USE_USART6
#define USART6_RX_PIN            	GPIO_PIN_7
#define USART6_RX_PORT            	GPIOC
#define USART6_TX_PIN            	GPIO_PIN_6
#define USART6_TX_PORT            	GPIOC


/* I2C */
#define I2C1_SDA_PIN				GPIO_PIN_9
#define I2C1_SCL_PIN				GPIO_PIN_8
#define I2C1_PORT					GPIOB

#define I2C2_SCL_PIN				GPIO_PIN_10
#define I2C2_SDA_PIN				GPIO_PIN_11
#define I2C2_PORT					GPIOB




#define SPI1_MISO					GPIO_PIN_6
#define SPI1_MISO_PORT				GPIOA
#define SPI1_MOSI					GPIO_PIN_7
#define SPI1_MOSI_PORT				GPIOA
#define SPI1_SCK					GPIO_PIN_5
#define SPI1_SCK_PORT				GPIOA

#define SPI3_MISO					GPIO_PIN_11
#define SPI3_MISO_PORT				GPIOC
#define SPI3_MOSI					GPIO_PIN_12
#define SPI3_MOSI_PORT				GPIOC
#define SPI3_SCK					GPIO_PIN_10
#define SPI3_SCK_PORT				GPIOC


#define SPI3_NSS					GPIO_PIN_15
#define SPI3_NSS_PORT				GPIOA



 /* Gyro */
#define GYRO
#define MPU6000_CS_PIN				GPIO_PIN_4
#define MPU6000_CS_PORT				GPIOA

#define MPU6000_SPI_INSTANCE		SPI1			//Dont know if necessary for us to specify


 /* Led Warnings */
#define USE_LEDS
//#define USE_LED_WARNINGS
//#define USE_LED_WARNINGS_MISSED_PERIOD
//#define USE_LED_WARNINGS_SYSTEM_LOAD

 /* Scheduler */
//#define USE_DEBUG_TASKS		//Only to be used when testing scheduler, not when intending to run the whole system
#define USE_TASK_AGE_CYCLE_STATISTICS

/* EEPROM */
//#define USE_DEBUG_EEPROM

 /* Baro */
#define BARO
#define BARO_USE_I2C_SOFT
//#define BARO_USE_I2C_HARD //Dont work with DMA right now if fixed should be faster. Otherwise software is faster than hardware I2C

#define MPU6000_NSS_PIN					GPIO_PIN_4
#define MPU6000_NSS_PORT				GPIOA

 /* Compass */
#define COMPASS


 /* GPS */
#define GPS


 /* Sonar */
//#define SONAR


 /* Beeper */
#define BEEPER
#define BEEPER_PIN						GPIO_PIN_12
#define BEEPER_PORT						GPIOB


 /* Define all the moter of the system, servos + extra */
#define MOTOR_1						1
#define MOTOR_1_PIN					SERVO_OUT_1_PIN
#define MOTOR_1_PORT				SERVO_OUT_1_GPIO_PORT
#define MOTOR_1_TIM					TIM3
#define MOTOR_1_CHANNEL				TIM_CHANNEL_3

#define MOTOR_2						2
#define MOTOR_2_PIN					SERVO_OUT_2_PIN
#define MOTOR_2_PORT				SERVO_OUT_2_GPIO_PORT
#define MOTOR_2_TIM					TIM3
#define MOTOR_2_CHANNEL				TIM_CHANNEL_4

#define MOTOR_3						3
#define MOTOR_3_PIN					SERVO_OUT_3_PIN
#define MOTOR_3_PORT				SERVO_OUT_3_GPIO_PORT
#define MOTOR_3_TIM					TIM9
#define MOTOR_3_CHANNEL				TIM_CHANNEL_2

#define MOTOR_4						4
#define MOTOR_4_PIN					SERVO_OUT_4_PIN
#define MOTOR_4_PORT				SERVO_OUT_4_GPIO_PORT
#define MOTOR_4_TIM					TIM2
#define MOTOR_4_CHANNEL				TIM_CHANNEL_3

#define MOTOR_5						5
#define MOTOR_5_PIN					SERVO_OUT_5_PIN
#define MOTOR_5_PORT				SERVO_OUT_5_GPIO_PORT
#define MOTOR_5_TIM					TIM5
#define MOTOR_5_CHANNEL				TIM_CHANNEL_2

#define MOTOR_6						6
#define MOTOR_6_PIN					SERVO_OUT_6_PIN
#define MOTOR_6_PORT				SERVO_OUT_6_GPIO_PORT
#define MOTOR_6_TIM					TIM5
#define MOTOR_6_CHANNEL				TIM_CHANNEL_1

#define MOTOR_7						7
#define MOTOR_7_PIN					GPIO_PIN_14
#define MOTOR_7_PORT				GPIOB
#define MOTOR_7_TIM					TIM12
#define MOTOR_7_CHANNEL				TIM_CHANNEL_1

#define MOTOR_8						8
#define MOTOR_8_PIN					GPIO_PIN_15
#define MOTOR_8_PORT				GPIOB
#define MOTOR_8_TIM					TIM12
#define MOTOR_8_CHANNEL				TIM_CHANNEL_2

#define MOTOR_9						9
#define MOTOR_9_PIN					GPIO_PIN_8
#define MOTOR_9_PORT				GPIOC
#define MOTOR_9_TIM					TIM8
#define MOTOR_9_CHANNEL				TIM_CHANNEL_3

#define MOTOR_10					10
#define MOTOR_10_PIN				GPIO_PIN_9
#define MOTOR_10_PORT				GPIOC
#define MOTOR_10_TIM				TIM8
#define MOTOR_10_CHANNEL			TIM_CHANNEL_4


#ifdef __cplusplus
}
#endif

#endif /* UTILITIES_STM32F4XX_REVO_STM32F4XX_REVO_H_ */
