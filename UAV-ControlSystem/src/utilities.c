 /**********************************************************************
 * NAME: utilities.c                                                   *
 * AUTHOR: Philip Johansson                                            *
 * PURPOSE: Set up and read from ADC                                   *
 * INFORMATION:                                                        *
 *	Here we gather usable functions by the whole system                *
 *	                                                                   *
 * GLOBAL VARIABLES:                                                   *
 * Variable		Type		Description                                *
 * --------		----		-----------                                *
 * 																	   *
 **********************************************************************/

#include "utilities.h"

/***********************************************************************
* BRIEF:       Calculates the length of a string(char[])
* INFORMATION: Calculates the number of characters in a char arr
***********************************************************************/
uint16_t calculateStringLength (const char * src, int maxSize)
{
	uint16_t toReturn = 0;
	for(int i = 0; src[i] != 0 ; i++)
	{
		toReturn ++;
	}
	return toReturn;
}

/***********************************************************************
* BRIEF:       Gives the length of a typestring						   *
* INFORMATION: Calculates the number of characters in a typestring.    *
* 			   Essentially it calculates the number of strings in an   *
* 			   string array.                						   *
***********************************************************************/
uint32_t calculateTypeStringLength(typeString arr[], int maxSize)
{
	int i = 0;

	for(i = 0; i < maxSize && arr[i][0] != 0; i++);

	return i;
}

/***********************************************************************
* BRIEF:	   Checks if a string consists of numbers
* INFORMATION: Given a string of numbers it will check if it is a number
* 			   by comparing to the ascii table
***********************************************************************/
bool isStringNumbers(char * msg)
{
	char c;
	int i = 0;
	while(c = msg[i], c != 0)
	{
		if (c >= 48 && c <= 57)
			i++;
		else
			return false;
	}

	return true;
}

/***********************************************************************
* BRIEF:	   Parses a string of numbers to int
* INFORMATION: Parses a string of numbers to a 64-bit integer.
***********************************************************************/
uint64_t parseToInt64(char * msg)
{
	uint64_t toReturn = 0;
	uint64_t multiplier = 1;
	int i;

	//Count the number of values in the char
	for(i = 0; msg[i+1] != 0; i++);

	//iter each value from the back
	for (;i >= 0; i--)
	{
		//switch each number value
		switch((uint8_t)msg[i])
		{
		case 48:	//0
			toReturn = toReturn + 0 * multiplier;
			break;
		case 49:	//1
			toReturn = toReturn + 1 * multiplier;
			break;
		case 50:	//2
			toReturn = toReturn + 2 * multiplier;
			break;
		case 51:	//3
			toReturn = toReturn + 3 * multiplier;
			break;
		case 52:	//4
			toReturn = toReturn + 4 * multiplier;
			break;
		case 53:	//5
			toReturn = toReturn + 5 * multiplier;
			break;
		case 54:	//6
			toReturn = toReturn + 6 * multiplier;
			break;
		case 55:	//7
			toReturn = toReturn + 7 * multiplier;
			break;
		case 56:	//8
			toReturn = toReturn + 8 * multiplier;
			break;
		case 57:	//9
			toReturn = toReturn + 9 * multiplier;
			break;
		}
		multiplier = multiplier * 10;
	}

	return toReturn;
}

/***********************************************************************
* BRIEF:       Tries to split a string on whitespace
* INFORMATION: Splits a string on whitespace and places it in dst
***********************************************************************/
bool splitStringWhitespace(char * src, typeString dst[], uint16_t stringSize, uint16_t arraySize)
{
	char currentChar;
	uint32_t iterator = 0;				//iterates through the string
	uint32_t arrayIterator = 0;			//iterates through the array positioning
	uint32_t arrayStringIterator = 0;	//iterates through the string inside the array
	//Loop through the values in src, until we find \r(enter) or null
	while(currentChar = src[iterator], currentChar != '\r' && currentChar != 0 )
	{
		//check if whitespace
		if (currentChar == ' ')
		{
			//increase the array iterator
			arrayIterator ++;
			if (arrayIterator >= arraySize)	//check that its not to large
			{
				//usart_send("To many words in the command");
				return false;
			}

			arrayStringIterator = 0;
			goto increment;				//continue to the next iteration, with a goto the increment
		}

		dst[arrayIterator][arrayStringIterator] = src[iterator];	//set the char to the correct array position
		arrayStringIterator ++;
		if (arrayStringIterator >=stringSize)
		{
			return false;
		}

		//Goto statement to jump to the incrementation
increment:
		iterator ++;	//increment iterator
	}

	return true;
}

/***********************************************************************
* BRIEF:       Finds previous whitespace in a string
* INFORMATION: Given a string and start index it will try to find
*              one whitespace counting downwards.
***********************************************************************/
int FindPreviousWhiteSpaceIndex(char * msg, int startIndex)
{
	int toReturn = 0;
	for (int i = startIndex; i >= 0; i--)
	{
		if(msg[i] == ' ')
		{
			toReturn = i;
			break;
		}
	}
	return toReturn;
}

/***********************************************************************
* BRIEF: Sums elements of array until index of second arg              *
* INFORMATION: Returns the sum                                         *
***********************************************************************/
uint32_t accumulate(uint32_t * list, int length)
{
	int value = 0;
	for (int i = 0; i < length; i++)
	{
		value += list[i];
	}
	return value;
}

/***********************************************************************
* BRIEF: A function that can be called on exceptions                   *
* INFORMATION: If an exception happens its easier to find it in an     *
* infinite loop                                                        *
***********************************************************************/
void Error_Handler(void)
{
  while(1)
  {
  }
}

uint8_t reverse(uint8_t byte)
{
//	uint8_t ret = 0;
//	ret = (byte & 0x80) >> 7 | (byte & 0x01) << 7;
//	ret = (byte & 0x40) >> 6 | (byte & 0x02) << 6;
//	ret = (byte & 0x20) >> 5 | (byte & 0x04) << 5;
//	ret = (byte & 0x10) >> 4 | (byte & 0x08) << 4;

	byte = ((byte * 0x0802LU & 0x22110LU) | (byte * 0x8020LU & 0x88440LU)) * 0x10101LU >> 16;

	return byte;
}

int16_t constrain(int16_t value, int16_t min, int16_t max)
{
	if (value < min)
		return min;
	else if (value > max)
		return max;
	else
		return value;
}

/**************************************************************************
* BRIEF: Constrain float values within a defined limit                    *
* INFORMATION: Used in PID loop to limit values                           *
**************************************************************************/
float constrainf(float amt, int low, int high)
{
	if (amt < (float)low)
		return (float)low;
	else if (amt > (float)high)
		return (float)high;
	else
		return amt;
}
