/**************************************************************************
* NAME: 	   tasks.h													  *
*																	      *
* AUTHOR:  	   Jonas Holmberg											  *
*                                                            			  *
* PURPOSE:     Defining the the scheduler to be used in the system to     *
*              organize the runtime for the tasks in the system based on  *
*              priority.     											  *
* 		   																  *
* INFORMATION: Adds the initial task values for each task that can be in  *
* 			   the system, in the c file. In the h file functions that    *
* 			   when called will* perform the action of its corresponding  *
* 			   task.							  					      *
*                                                            			  *
* GLOBAL VARIABLES:                                                       *
* Variable        Type          Description                               *
* --------        ----          -----------                               *
***************************************************************************/

#include "Scheduler/tasks.h"
#include "Scheduler/scheduler.h"


/**************************************************************************
* Information - Initiate the values for all the tasks. This information
* needs to be changed when functionality and setting for the tasks needs to
* be changed. Each new task added to the system needs to be added here or
* it will not work.
*
* Note - desiredPeriod is given in micro seconds, writing for example
* (1000000 / 10) means 10 hz rate
***************************************************************************/
task_t SystemTasks[TASK_COUNT] =
	{

		[TASK_SYSTEM] =
		{
			.taskName = "SYSTEM",
			.taskFunction = systemTaskSystem,
			.desiredPeriod = GetUpdateRateHz(10),			//10 hz update rate (100 ms)
			.staticPriority = PRIORITY_HIGH,
		},

		[TASK_GYROPID] =
		{
			.taskName = "PID",
			.subTaskName = "GYRO",
			.taskFunction = systemTaskGyroPid,
			.desiredPeriod = GetUpdateRateHz(1000),			//1000 hz update rate (1 ms)
			.staticPriority = PRIORITY_REALTIME,
		},

		[TASK_ACCELEROMETER] =
		{
			.taskName = "ACCELEROMTER",
			.taskFunction = systemTaskAccelerometer,
			.desiredPeriod = GetUpdateRateHz(1000),			//1000 hz update rate (1 ms)
			.staticPriority = PRIORITY_MEDIUM,
		},

		[TASK_ATTITUDE] =
		{
			.taskName = "ATTITUDE",
			.taskFunction = systemTaskAttitude,
			.desiredPeriod = GetUpdateRateHz(100),			//100 hz update rate (10 ms)
			.staticPriority = PRIORITY_MEDIUM,
		},

		[TASK_RX] =
		{
			.taskName = "RX",
			.taskFunction = systemTaskRx,					//Event handler function, will check if a message is obtainable
			.checkEventTriggered = systemTaskRxCheck,
			.desiredPeriod = GetUpdateRateHz(50),			//Standard scheduling should not be used if event based, used as fail safe
			.staticPriority = PRIORITY_HIGH,
		},

		[TASK_RX_CLI] =
		{
			.taskName = "RXCLI",
			.taskFunction = systemTaskRxCli,					//Event handler function, will check if a message is obtainable
			.checkEventTriggered = systemTaskRxCliCheck,
			.desiredPeriod = GetUpdateRateHz(50),			//Standard scheduling should not be used if event based, used as fail safe
			.staticPriority = PRIORITY_HIGH,
		},

		[TASK_SERIAL] =
		{
			.taskName = "SERIAL",
			.taskFunction = systemTaskSerial,
			.desiredPeriod = GetUpdateRateHz(2),			//100 hz update rate (10 ms)
			.staticPriority = PRIORITY_LOW,
		},

		[TASK_BATTERY] =
		{
			.taskName = "BATTERY",
			.taskFunction = systemTaskBattery,
			.desiredPeriod = GetUpdateRateHz(50),			//50 hz update rate (20 ms)
			.staticPriority = PRIORITY_MEDIUM,
		},

		[TASK_ARDUINO] =
		{
		     .taskName = "ARDUINO",
		     .taskFunction = systemTaskArduino,
		 	 .desiredPeriod = GetUpdateRateHz(50),			//50 hz update rate (20 ms)
		 	 .staticPriority = PRIORITY_MEDIUM,
		 },

#ifdef BARO
		[TASK_BARO] =
		{
			.taskName = "BAROMETER",
			.taskFunction = systemTaskBaro,
			.desiredPeriod = GetUpdateRateHz(200),			//200 hz update rate (5 ms)
			.staticPriority = PRIORITY_LOW,
		},
#endif

#ifdef COMPASS
		[TASK_COMPASS] =
		{
			.taskName = "COMPASS",
			.taskFunction = systemTaskCompass,
			.desiredPeriod = GetUpdateRateHz(10),			//10 hz update rate (100 ms)
			.staticPriority = PRIORITY_LOW,
		},
#endif

#ifdef GPS
		[TASK_GPS] =
		{
			.taskName = "GPS",
			.taskFunction = systemTaskGps,
			.desiredPeriod = GetUpdateRateHz(10),			//10 hz update rate (100 ms)
			.staticPriority = PRIORITY_MEDIUM,
		},
#endif

#ifdef SONAR
		[TASK_SONAR] =
		{
			.taskName = "SONAR",
			.taskFunction = systemTaskSonar,
			.desiredPeriod = GetUpdateRateHz(20),			//20 hz update rate (50 ms)
			.staticPriority = PRIORITY_LOW,
		},
#endif

#if defined(BARO) || defined(SONAR)
		[TASK_ALTITUDE] =
		{
			.taskName = "ALTITUDE",
			.taskFunction = systemTaskAltitude,
			.desiredPeriod = GetUpdateRateHz(40),			//40 hz update rate (25 ms)
			.staticPriority = PRIORITY_LOW,
		},
#endif

#ifdef BEEPER
		[TASK_BEEPER] =
		{
			.taskName = "BEEPER",
			.taskFunction = systemTaskBeeper,
			.desiredPeriod = GetUpdateRateHz(100),			//100 hz update rate (10 ms)
			.staticPriority = PRIORITY_LOW,
		},
#endif

		/* ----------------------------------------------------------------------------------------------------------------- */
		/* DEBUG ONLY TASKS, NOT TO BE INCLUDED WHEN RUNNING THE REAL SYSTEM ----------------------------------------------- */
#ifdef USE_DEBUG_TASKS

		[TASK_DEBUG_1] =
				{
					.taskName = "DEBUG_1",
					.taskFunction = systemTaskDebug_1,
					.desiredPeriod = GetUpdateRateHz(100),
					.staticPriority = PRIORITY_REALTIME,
				},

		[TASK_DEBUG_2] =
				{
					.taskName = "DEBUG_2",
					.taskFunction = systemTaskDebug_2,
					.desiredPeriod = GetUpdateRateHz(40),
					.staticPriority = PRIORITY_MEDIUM,
				},

		[TASK_DEBUG_3] =
				{
					.taskName = "DEBUG_3",
					.taskFunction = systemTaskDebug_3,
					.desiredPeriod = GetUpdateRateHz(20),
					.staticPriority = PRIORITY_LOW,
				},
#endif

};
