/**************************************************************************
* NAME: filter.c                                                          *
* AUTHOR: Johan G�rtner                                                   *
*
* PURPOSE: This file contains filter functions                            *
* INFORMATION:                                                            *
* GLOBAL VARIABLES:                                                       *
* Variable        Type          Description                               *
* --------        ----          -----------                               *
*
* **************************************************************************/

#include "Flight/filter.h"

/**************************************************************************
* BRIEF: First order lowpass filter                                       *
* INFORMATION:                                                            *
**************************************************************************/
float pt1FilterApply4(pt1Filter_t *filter, float input, uint8_t f_cut, float dT)
{
	/*Pre calculate and store RC*/
	if (!filter->RC) {
		filter->RC = 1.0f / (2.0f * M_PIf * f_cut);
		filter->dT = dT;
		filter->state = 0;
	}

	/*Adds the new input to the old value of the low pass filter*/
	filter->state = filter->state + filter->dT / (filter->RC + filter->dT) * (input - filter->state);

	return filter->state;
}
