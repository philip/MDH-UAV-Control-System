 /**********************************************************************
 * NAME:        cli.c                                                  *
 * AUTHOR:      Jonas Holmberg                                         *
 * PURPOSE:     Provide the ability to change some values by use of    *
 * 				command line interface.                                *
 * INFORMATION: By some predefined commands values can be changed in   *
 * 				the system by writing in a serial communication        *
 * 				terminal that runs on usart. It also has the ability   *
 * 				to save the value changes to EEPROM and reset all	   *
 * 				values. The system can also be reseted and rebooted.   *
 *	                                                                   *
 * GLOBAL VARIABLES:                                                   *
 * Variable		Type		   Description                             *
 * --------		----		   -----------                             *
 * cliUsart		Usart_profile  The handler to the usart used by the cli*																   *
 **********************************************************************/

#include "config/cli.h"
#include "stdint.h"
#include "config/eeprom.h"
#include "stm32f4xx_revo.h"
#include <stdio.h>
#include <string.h>
#include "utilities.h"
#include "Scheduler/scheduler.h"
#include "drivers/motors.h"
#include "drivers/accel_gyro.h"
#include "Flight/pid.h"
#include "drivers/compass.h"

#define cliActivateCharacter	35
#define commandValueError		0xFFFFFFFFFFFFFFFF

#define minSimilarCharacters	2			//the minimum amount of characters needed to to a search on a value
#define maxSimilarSearchValues	18			//max amount of values that will be found when doing a search for similar strings based on the written chars
#define CLI_baudRate			115200		//The baudrate used for the CLI usart
#define msgArraySize 			3			//The number of words that a max command can contain (ex: set looptime 1000)

#define commandSize_1			1			//A command with a size of 1
#define commandSize_2			2			//A command with a size of 2
#define commandSize_3			3			//A command with a size of 3
#define commandSizeOffset		msgArraySize
#define commandSize(x)			1 << (x-1)
#define commandMask(x, y)		(commandSize(x) | (1 << (commandSizeOffset + y)))	//macro used to to mask a size of a command and the command ID
#define arrayPos_Action			0		//Command msg array position values: action
#define arrayPos_Command		1		//Command msg array position values: command
#define arrayPos_Value			2		//Command msg array position values: value



//the handler to the usart profile
usart_profile cliUsart;

bool cliIsRunning = false;			//Flag to tell if it is running
bool valueChanged = false;			//Flag that keeps track on if any value has changed and not been saved

//ToDo: currently not implemented, fix if needed. Could be used to force reboot if a certain value has changed
bool rebootSystemNeeded = false;	//Flag that signifies that a value was changed that require the system to reboot

//ToDo: used for testing REMOVE
uint8_t pid_pitch_pk = 0;

/* ID values for the ACTIONS that CLI can perform */
typedef enum
{
	ACTION_SET_VALUE = 0,			//sets a value
	ACTION_DUMP,					//Writes back all the current values set
	ACTION_SAVE,					//Saves the all the changes
	ACTION_UNDO,					//Undoes all the non made changes
	ACTION_HELP,					//Get information on how the cli works
	ACTION_GET_INFORMATION,			//Get information in a specific value
	ACTION_COMMANDS,				//Get information on all the specific commands
	ACTION_PROFILE,					//Set the active eeprom profile
	ACTION_EXIT,					//Exists the cli
	ACTION_REBOOT,					//Reboots the entire system
	ACTION_RESET,					//Resets the entire eeprom
	ACTION_STATS,					//gives statistics on the system
	ACTION_MOTORCALIBRATE,
	ACTION_GYROCALIBRATE,
	ACTION_ACCELEROMTETERCALIBRATE,
	ACTION_COMPASSCALIBRATE,
	ACTION_CALIBRATIONINFOACCEL,

	//The number of actions
	ACTION_COUNT,					//Count of the number of actions

	ACTION_NOACTION					//Not an legit action
}commandAction_t;

/* Sting values for the corresponding ACTION_IDS from "commandAction_t". Needs to be in the same order as the enum values */
const typeString commandAction_Strings[ACTION_COUNT] = {
	"set",
	"dump",
	"save",
	"undo",
	"help",
	"information",
	"commands",
	"profile",
	"exit",
	"reboot",
	"reset",
	"stats",
	"calibrate_motors",
	"calibrate_gyro",
	"calibrate_accelerometer",
	"calibrate_compass",
	"calibration_info_accelerometer"
};

/* String values descrbing information of a certain action */
const typeString commandActionInformation_Strings[ACTION_COUNT] = {
	"| set - Updates value for given variable.\n\r",
	"| dump - Gives the current information for all values.\n\r",
	"| save - Writes all the changes to the EEPROM.\n\r",
	"| undo - Undo all the unsaved changes by loading the stored values.\n\r",
	"| help - Gives information on how to use the CLI.\n\r",
	"| information - Provides information on a given value.\n\r",
	"| commands - Provides information on the available commands.\n\r",
	"| profile - Changes the active profile between the values (1-3).\n\r",
	"| exit - Exits the CLI mode.\n\r",
	"| reboot - Exit CLI and reboots the system.\n\r",
	"| reset - Restore all the values to its default values.\n\r",
	"| stats - Gives some current stats of the system and tasks.\n\r",
	"| calibrate_motors - Calibrates all motors.\n\r",
	"| calibrate_gyro - Calibrates the gyro.\n\r",
	"| calibrate_accelerometer - Calibrates the accelerometer.\n\r",
	"| calibrate_compass - Calibrates the compass.\n\r",
	"| calibration_info_accelerometer - Provides info on the accelerometer calibration.\n\r"
};

/* String array containing all the signature examples for each action */
const typeString commandActionSignature_Strings[ACTION_COUNT] = {
	" set example_value number",
	" dump",
	" save",
	" undo",
	" help",
	" information example_value",
	" commands",
	" profile number",
	" exit",
	" reboot",
	" reset",
	" stats",
	" calibrate_motors",
	" calibrate_gyro",
	" calibrate_accelerometer",
	" calibrate_compass",
	" calibration_info_accelerometer"
};

/* Size values for the values a command will require */
typedef enum
{
	VAL_UINT_8 = 1,
	VAL_INT_8,
	VAL_UINT_16,
	VAL_INT_16,
	VAL_UINT_32,
	VAL_INT_32,
	VAL_UINT_64,
	VAL_INT_64,
	VAL_BOOL
}valueTypes_t;

/* ID values for individual command, all commands must have a uniqe ID value */
typedef enum
{
	COMMAND_ID_ADC_SCALES_VCC,
	COMMAND_ID_ADC_SCALES_LEFT,
	COMMAND_ID_ADC_SCALES_RIGHT,
	COMMAND_ID_PID_ROLL_KP,

	/* calibrate motors command */
	COMMAND_ID_MOTORCALIBRATE,

	/* Period values for tasks */
	COMMAND_ID_PERIOD_SYSTEM ,
	COMMAND_ID_PERIOD_GYROPID,
	COMMAND_ID_PERIOD_ACCELERMETER,
	COMMAND_ID_PERIOD_ATTITUDE,
	COMMAND_ID_PERIOD_RX,
	COMMAND_ID_PERIOD_RX_CLI,
	COMMAND_ID_PERIOD_SERIAL,
	COMMAND_ID_PERIOD_BATTERY,
	COMMAND_ID_PERIOD_ARDUINO,
#ifdef BARO
	COMMAND_ID_PERIOD_BARO,
#endif
#ifdef COMPASS
	COMMAND_ID_PERIOD_COMPASS,
#endif
#ifdef GPS
	COMMAND_ID_PERIOD_GPS,
#endif
#ifdef SONAR
	COMMAND_ID_PERIOD_SONAR,
#endif
#if defined(BARO) || defined(SONAR)
	COMMAND_ID_PERIOD_ALTITUDE,
#endif
#ifdef BEEPER
	COMMAND_ID_PERIOD_BEEPER,
#endif


	/* Commands for chaning motormixer values */
	COMMAND_ID_MOTORMIX_MINTHROTTLE,
	COMMAND_ID_MOTORMIX_MAXTHROTTLE,
	COMMAND_ID_MOTORMIX_MINCOMMAND,
	COMMAND_ID_MOTORMIX_MAXCOMMAND,
	COMMAND_ID_MOTORMIX_MINCHECK,
	COMMAND_ID_MOTORMIX_PID_AT_MINTHROTTLE,
	COMMAND_ID_MOTORMIX_MOTORSTOP,
	COMMAND_ID_MOTORMIX_YAW_REVERSE_DIRECTION,


	/* Commands for changing flag toggles from RC */
	COMMAND_ID_FLAG_ARM_MINRANGE,
	COMMAND_ID_FLAG_ARM_MAXRANGE,
	COMMAND_ID_FLAG_ARM_CHANNEL,

	COMMAND_ID_FLAG_FLIGHTMODE_ACCELEROMETER_MINRANGE,
	COMMAND_ID_FLAG_FLIGHTMODE_ACCELEROMETER_MAXRANGE,
	COMMAND_ID_FLAG_FLIGHTMODE_ACCELEROMETER_CHANNEL,

	COMMAND_ID_FLAG_FLIGHTMODE_BAROMETER_MINRANGE,
	COMMAND_ID_FLAG_FLIGHTMODE_BAROMETER_MAXRANGE,
	COMMAND_ID_FLAG_FLIGHTMODE_BAROMETER_CHANNEL,

	COMMAND_ID_FLAG_FLIGHTMODE_COMPASS_MINRANGE,
	COMMAND_ID_FLAG_FLIGHTMODE_COMPASS_MAXRANGE,
	COMMAND_ID_FLAG_FLIGHTMODE_COMPASS_CHANNEL,

	COMMAND_ID_FLAG_FLIGHTMODE_GPS_MINRANGE,
	COMMAND_ID_FLAG_FLIGHTMODE_GPS_MAXRANGE,
	COMMAND_ID_FLAG_FLIGHTMODE_GPS_CHANNEL,

	COMMAND_ID_FLAG_MIXERFULLSCALE_MINRANGE,
	COMMAND_ID_FLAG_MIXERFULLSCALE_MAXRANGE,
	COMMAND_ID_FLAG_MIXERFULLSCALE_CHANNEL,

	COMMAND_ID_FLAG_MIXERLOWSCALE_MINRANGE,
	COMMAND_ID_FLAG_MIXERLOWSCALE_MAXRANGE,
	COMMAND_ID_FLAG_MIXERLOWSCALE_CHANNEL,

	COMMAND_ID_FLAG_FLIGHTMODE_3_MINRANGE,
	COMMAND_ID_FLAG_FLIGHTMODE_3_MAXRANGE,
	COMMAND_ID_FLAG_FLIGHTMODE_3_CHANNEL,


	/* Profile values */

	/* PID value */
	//GYRO
	COMMAND_ID_PID_GYRO_P_ROLL,
	COMMAND_ID_PID_GYRO_P_PITCH,
	COMMAND_ID_PID_GYRO_P_YAW,

	COMMAND_ID_PID_GYRO_I_ROLL,
	COMMAND_ID_PID_GYRO_I_PITCH,
	COMMAND_ID_PID_GYRO_I_YAW,

	COMMAND_ID_PID_GYRO_D_ROLL,
	COMMAND_ID_PID_GYRO_D_PITCH,
	COMMAND_ID_PID_GYRO_D_YAW,

	COMMAND_ID_PID_GYRO_DTERM_LPF,
	COMMAND_ID_PID_GYRO_PTERM_YAW_LPF,
	COMMAND_ID_PID_GYRO_YAW_P_LIMIT,
	COMMAND_ID_PID_GYRO_OUT_LIMIT,

	//ACCEL
	COMMAND_ID_PID_ACCEL_P_ROLL,
	COMMAND_ID_PID_ACCEL_P_PITCH,

	COMMAND_ID_PID_ACCEL_I_ROLL,
	COMMAND_ID_PID_ACCEL_I_PITCH,

	COMMAND_ID_PID_ACCEL_D_ROLL,
	COMMAND_ID_PID_ACCEL_D_PITCH,

	COMMAND_ID_PID_ACCEL_DTERM_LPF,
	COMMAND_ID_PID_ACCEL_PTERM_YAW_LPF,
	COMMAND_ID_PID_ACCEL_YAW_P_LIMIT,
	COMMAND_ID_PID_ACCEL_OUT_LIMIT,

	//Barometer
	COMMAND_ID_PID_BARO_P_HEIGHT,

	COMMAND_ID_PID_BARO_I_HEIGHT,

	COMMAND_ID_PID_BARO_D_HEIGHT,

	COMMAND_ID_PID_BARO_DTERM_LPF,
	COMMAND_ID_PID_BARO_PTERM_YAW_LPF,
	COMMAND_ID_PID_BARO_YAW_P_LIMIT,
	COMMAND_ID_PID_BARO_OUT_LIMIT,

	/* Enable the different pid loops */
	COMMAND_ID_PID_GYRO_ISENABLED,
	COMMAND_ID_PID_ACCEL_ISENABLED,
	COMMAND_ID_PID_BARO_ISENABLED,

	/* Counter for the amount of commands */
	COMMAND_ID_COUNT,

	/* ID number for a non existing commands */
	COMMAND_ID_NO_COMMAND
}command_Ids_t;

/* Struct used to contain min and max values */
typedef struct
{
	uint32_t min;
	uint32_t max;
}minMaxVal_t;

/* Struct containing all the data needed to create a CLI command */
typedef struct
{
	const char * name;				//The name of the command
	uint16_t commandId;				//The id of the command, must be uniqe
	uint16_t valueId;				//The id of the value to change, given from the enum in EEPROM.h:
	uint16_t valueIdLoc;			//If it is part of system values, profile values, etc...
	uint32_t addressValueOffset;	//The offset to the value to be set, useful if the value is inside a struct, if just a standard val offset is 0
	uint8_t valueType;				//The type the value will be given from the enum ValueTypes_t
	minMaxVal_t valueRange;			//The range the value must be inbetween, used as a failsafe check

}cliCommandConfig_t;


/* Array containing all the commands that exist in the CLI, every single comand is defined here */
const cliCommandConfig_t commandTable[COMMAND_ID_COUNT] = {

		[COMMAND_ID_ADC_SCALES_VCC] =
		{
			"adc_scales_vcc", 		COMMAND_ID_ADC_SCALES_VCC, EEPROM_ADC_SCALES, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_32, .valueRange = {0, 200}
		},
		[COMMAND_ID_ADC_SCALES_LEFT] =
		{
			"adc_scales_left", 		COMMAND_ID_ADC_SCALES_LEFT, EEPROM_ADC_SCALES, EEPROM_VALUE_TYPE_SYSTEM, 4, VAL_UINT_32, .valueRange = {0, 200}
		},
		[COMMAND_ID_ADC_SCALES_RIGHT] =
		{
			"adc_scales_right", 	COMMAND_ID_ADC_SCALES_RIGHT, EEPROM_ADC_SCALES, EEPROM_VALUE_TYPE_SYSTEM, 8, VAL_UINT_32, .valueRange = {0, 200}
		},

		/* Calibrate motors */
		[COMMAND_ID_MOTORCALIBRATE] =
		{
			"motor_calibration", 	COMMAND_ID_MOTORCALIBRATE, EEPROM_MOTORCALIBRATE, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_BOOL, .valueRange = {0, 1}
		},

		/* set period commands */
		[COMMAND_ID_PERIOD_SYSTEM] =
		{
			"task_period_system", 	COMMAND_ID_PERIOD_SYSTEM, EEPROM_PERIOD_SYSTEM, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_32, .valueRange = {0, 1000000000}
		},
		[COMMAND_ID_PERIOD_GYROPID] =
		{
			"task_period_gyropid", 	COMMAND_ID_PERIOD_GYROPID, EEPROM_PERIOD_GYROPID, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_32, .valueRange = {0, 1000000000}
		},
		[COMMAND_ID_PERIOD_ACCELERMETER] =
		{
			"task_period_accelerometer", 	COMMAND_ID_PERIOD_ACCELERMETER, EEPROM_PERIOD_ACCELEROMETER, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_32, .valueRange = {0, 1000000000}
		},
		[COMMAND_ID_PERIOD_ATTITUDE] =
		{
			"task_period_attitude", 	COMMAND_ID_PERIOD_ATTITUDE, EEPROM_PERIOD_ATTITUDE, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_32, .valueRange = {0, 1000000000}
		},
		[COMMAND_ID_PERIOD_RX] =
		{
			"task_period_rx", 	COMMAND_ID_PERIOD_RX, EEPROM_PERIOD_RX, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_32, .valueRange = {0, 1000000000}
		},
		[COMMAND_ID_PERIOD_RX_CLI] =
		{
			"task_period_rx_cli", 	COMMAND_ID_PERIOD_RX_CLI, EEPROM_PERIOD_RX_CLI, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_32, .valueRange = {0, 1000000000}
		},
		[COMMAND_ID_PERIOD_SERIAL] =
		{
			"task_period_serial", 	COMMAND_ID_PERIOD_SERIAL, EEPROM_PERIOD_SERIAL, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_32, .valueRange = {0, 1000000000}
		},
		[COMMAND_ID_PERIOD_BATTERY] =
		{
			"task_period_battery", 	COMMAND_ID_PERIOD_BATTERY, EEPROM_PERIOD_BATTERY, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_32, .valueRange = {0, 1000000000}
		},
		[COMMAND_ID_PERIOD_ARDUINO] =
		{
			 "task_period_arduino", 	COMMAND_ID_PERIOD_ARDUINO, EEPROM_PERIOD_ARDUINO, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_32, .valueRange = {0, 1000000000}
		},
#ifdef BARO
		[COMMAND_ID_PERIOD_BARO] =
		{
			"task_period_baro", 	COMMAND_ID_PERIOD_BARO, EEPROM_PERIOD_BARO, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_32, .valueRange = {0, 1000000000}
		},
#endif
#ifdef COMPASS
		[COMMAND_ID_PERIOD_COMPASS] =
		{
			"task_period_compass", 	COMMAND_ID_PERIOD_COMPASS, EEPROM_PERIOD_COMPASS, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_32, .valueRange = {0, 1000000000}
		},
#endif
#ifdef GPS
		[COMMAND_ID_PERIOD_GPS] =
		{
			"task_period_gps", 	COMMAND_ID_PERIOD_GPS, EEPROM_PERIOD_GPS, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_32, .valueRange = {0, 1000000000}
		},
#endif
#ifdef SONAR
		[COMMAND_ID_PERIOD_SONAR] =
		{
			"task_period_sonar", 	COMMAND_ID_PERIOD_SONAR, EEPROM_PERIOD_SONAR, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_32, .valueRange = {0, 1000000000}
		},
#endif
#if defined(BARO) || defined(SONAR)
		[COMMAND_ID_PERIOD_ALTITUDE] =
		{
			"task_period_altitude", 	COMMAND_ID_PERIOD_ALTITUDE, EEPROM_PERIOD_ALTITUDE, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_32, .valueRange = {0, 1000000000}
		},
#endif
#ifdef  BEEPER
		[COMMAND_ID_PERIOD_BEEPER] =
		{
			"task_period_beeper", 	COMMAND_ID_PERIOD_BEEPER, EEPROM_PERIOD_BEEPER, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_32, .valueRange = {0, 1000000000}
		},
#endif


		/* Commands for chaning motormixer values */
		[COMMAND_ID_MOTORMIX_MINTHROTTLE] =
		{
			"motormix_minthrottle", 	COMMAND_ID_MOTORMIX_MINTHROTTLE, EEPROM_MOTORMIX_CONFIG, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_16, .valueRange = {0, 2500}
		},
		[COMMAND_ID_MOTORMIX_MAXTHROTTLE] =
		{
			"motormix_maxthrottle", 	COMMAND_ID_MOTORMIX_MAXTHROTTLE, EEPROM_MOTORMIX_CONFIG, EEPROM_VALUE_TYPE_SYSTEM, 2, VAL_UINT_16, .valueRange = {0, 2500}
		},
		[COMMAND_ID_MOTORMIX_MINCOMMAND] =
		{
			"motormix_mincommand", 	COMMAND_ID_MOTORMIX_MINCOMMAND, EEPROM_MOTORMIX_CONFIG, EEPROM_VALUE_TYPE_SYSTEM, 4, VAL_UINT_16, .valueRange = {0, 2500}
		},
		[COMMAND_ID_MOTORMIX_MAXCOMMAND] =
		{
			"motormix_maxcommand", 	COMMAND_ID_MOTORMIX_MAXCOMMAND, EEPROM_MOTORMIX_CONFIG, EEPROM_VALUE_TYPE_SYSTEM, 6, VAL_UINT_16, .valueRange = {0, 2500}
		},
		[COMMAND_ID_MOTORMIX_MINCHECK] =
		{
			"motormix_mincheck", 	COMMAND_ID_MOTORMIX_MINCHECK, EEPROM_MOTORMIX_CONFIG, EEPROM_VALUE_TYPE_SYSTEM, 8, VAL_UINT_16, .valueRange = {0, 2500}
		},
		[COMMAND_ID_MOTORMIX_PID_AT_MINTHROTTLE] =
		{
			"motormix_pid_at_minthrottle", 	COMMAND_ID_MOTORMIX_PID_AT_MINTHROTTLE, EEPROM_MOTORMIX_CONFIG, EEPROM_VALUE_TYPE_SYSTEM, 12, VAL_BOOL, .valueRange = {0, 1}
		},
		[COMMAND_ID_MOTORMIX_MOTORSTOP] =
		{
			"motormix_motorstop", 	COMMAND_ID_MOTORMIX_MOTORSTOP, EEPROM_MOTORMIX_CONFIG, EEPROM_VALUE_TYPE_SYSTEM, 16, VAL_BOOL, .valueRange = {0, 1}
		},
		[COMMAND_ID_MOTORMIX_YAW_REVERSE_DIRECTION] =
		{
			"motormix_yaw_reverse_direction", 	COMMAND_ID_MOTORMIX_YAW_REVERSE_DIRECTION, EEPROM_MOTORMIX_CONFIG, EEPROM_VALUE_TYPE_SYSTEM, 20, VAL_BOOL, .valueRange = {0, 1}
		},


		/* Commands for changing flag toggles from RC */
		[COMMAND_ID_FLAG_ARM_MINRANGE] =
		{
			"flag_arm_minrange", 	COMMAND_ID_FLAG_ARM_MINRANGE, EEPROM_FLAG_ARM, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_16, .valueRange = {0, 2500}
		},
		[COMMAND_ID_FLAG_ARM_MAXRANGE] =
		{
			"flag_arm_maxrange", 	COMMAND_ID_FLAG_ARM_MAXRANGE, EEPROM_FLAG_ARM, EEPROM_VALUE_TYPE_SYSTEM, 2, VAL_UINT_16, .valueRange = {0, 2500}
		},
		[COMMAND_ID_FLAG_ARM_CHANNEL] =
		{
			"flag_arm_channel", 	COMMAND_ID_FLAG_ARM_CHANNEL, EEPROM_FLAG_ARM, EEPROM_VALUE_TYPE_SYSTEM, 4, VAL_UINT_8, .valueRange = {0, 18}
		},

		[COMMAND_ID_FLAG_FLIGHTMODE_ACCELEROMETER_MINRANGE] =
		{
			"flag_flightmode_accelerometer_minrange", 	COMMAND_ID_FLAG_FLIGHTMODE_ACCELEROMETER_MINRANGE, EEPROM_FLAG_FLIGHTMODE_ACCELEROMETER, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_16, .valueRange = {0, 2500}
		},
		[COMMAND_ID_FLAG_FLIGHTMODE_ACCELEROMETER_MAXRANGE] =
		{
			"flag_flightmode_accelerometer_maxrange", 	COMMAND_ID_FLAG_FLIGHTMODE_ACCELEROMETER_MAXRANGE, EEPROM_FLAG_FLIGHTMODE_ACCELEROMETER, EEPROM_VALUE_TYPE_SYSTEM, 2, VAL_UINT_16, .valueRange = {0, 2500}
		},
		[COMMAND_ID_FLAG_FLIGHTMODE_ACCELEROMETER_CHANNEL] =
		{
			"flag_flightmode_accelerometer_channel", 	COMMAND_ID_FLAG_FLIGHTMODE_ACCELEROMETER_CHANNEL, EEPROM_FLAG_FLIGHTMODE_ACCELEROMETER, EEPROM_VALUE_TYPE_SYSTEM, 4, VAL_UINT_8, .valueRange = {0, 18}
		},

		[COMMAND_ID_FLAG_FLIGHTMODE_BAROMETER_MINRANGE] =
		{
			"flag_flightmode_barometer_minrange", 	COMMAND_ID_FLAG_FLIGHTMODE_BAROMETER_MINRANGE, EEPROM_FLAG_FLIGHTMODE_BAROMETER, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_16, .valueRange = {0, 2500}
		},
		[COMMAND_ID_FLAG_FLIGHTMODE_BAROMETER_MAXRANGE] =
		{
			"flag_flightmode_barometer_maxrange", 	COMMAND_ID_FLAG_FLIGHTMODE_BAROMETER_MAXRANGE, EEPROM_FLAG_FLIGHTMODE_BAROMETER, EEPROM_VALUE_TYPE_SYSTEM, 2, VAL_UINT_16, .valueRange = {0, 2500}
		},
		[COMMAND_ID_FLAG_FLIGHTMODE_BAROMETER_CHANNEL] =
		{
			"flag_flightmode_barometer_channel", 	COMMAND_ID_FLAG_FLIGHTMODE_BAROMETER_CHANNEL, EEPROM_FLAG_FLIGHTMODE_BAROMETER, EEPROM_VALUE_TYPE_SYSTEM, 4, VAL_UINT_8, .valueRange = {0, 18}
		},

		[COMMAND_ID_FLAG_FLIGHTMODE_COMPASS_MINRANGE] =
		{
			"flag_flightmode_compass_minrange", 	COMMAND_ID_FLAG_FLIGHTMODE_COMPASS_MINRANGE, EEPROM_FLAG_FLIGHTMODE_COMPASS, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_16, .valueRange = {0, 2500}
		},
		[COMMAND_ID_FLAG_FLIGHTMODE_COMPASS_MAXRANGE] =
		{
			"flag_flightmode_compass_maxrange", 	COMMAND_ID_FLAG_FLIGHTMODE_COMPASS_MAXRANGE, EEPROM_FLAG_FLIGHTMODE_COMPASS, EEPROM_VALUE_TYPE_SYSTEM, 2, VAL_UINT_16, .valueRange = {0, 2500}
		},
		[COMMAND_ID_FLAG_FLIGHTMODE_COMPASS_CHANNEL] =
		{
			"flag_flightmode_compass_channel", 	COMMAND_ID_FLAG_FLIGHTMODE_COMPASS_CHANNEL, EEPROM_FLAG_FLIGHTMODE_COMPASS, EEPROM_VALUE_TYPE_SYSTEM, 4, VAL_UINT_8, .valueRange = {0, 18}
		},

		[COMMAND_ID_FLAG_FLIGHTMODE_GPS_MINRANGE] =
		{
			"flag_flightmode_gps_minrange", 	COMMAND_ID_FLAG_FLIGHTMODE_GPS_MINRANGE, EEPROM_FLAG_FLIGHTMODE_GPS, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_16, .valueRange = {0, 2500}
		},
		[COMMAND_ID_FLAG_FLIGHTMODE_GPS_MAXRANGE] =
		{
			"flag_flightmode_gps_maxrange", 	COMMAND_ID_FLAG_FLIGHTMODE_GPS_MAXRANGE, EEPROM_FLAG_FLIGHTMODE_GPS, EEPROM_VALUE_TYPE_SYSTEM, 2, VAL_UINT_16, .valueRange = {0, 2500}
		},
		[COMMAND_ID_FLAG_FLIGHTMODE_GPS_CHANNEL] =
		{
			"flag_flightmode_gps_channel", 	COMMAND_ID_FLAG_FLIGHTMODE_GPS_CHANNEL, EEPROM_FLAG_FLIGHTMODE_GPS, EEPROM_VALUE_TYPE_SYSTEM, 4, VAL_UINT_8, .valueRange = {0, 18}
		},

		[COMMAND_ID_FLAG_MIXERFULLSCALE_MINRANGE] =
		{
			"flag_mixerfullscale_minrange", 	COMMAND_ID_FLAG_MIXERFULLSCALE_MINRANGE, EEPROM_FLAG_MIXERFULLSCALE, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_16, .valueRange = {0, 2500}
		},
		[COMMAND_ID_FLAG_MIXERFULLSCALE_MAXRANGE] =
		{
			"flag_mixerfullscale_maxrange", 	COMMAND_ID_FLAG_MIXERFULLSCALE_MAXRANGE, EEPROM_FLAG_MIXERFULLSCALE, EEPROM_VALUE_TYPE_SYSTEM, 2, VAL_UINT_16, .valueRange = {0, 2500}
		},
		[COMMAND_ID_FLAG_MIXERFULLSCALE_CHANNEL] =
		{
			"flag_mixerfullscale_channel", 	COMMAND_ID_FLAG_MIXERFULLSCALE_CHANNEL, EEPROM_FLAG_MIXERFULLSCALE, EEPROM_VALUE_TYPE_SYSTEM, 4, VAL_UINT_8, .valueRange = {0, 18}
		},

		[COMMAND_ID_FLAG_MIXERLOWSCALE_MINRANGE] =
		{
			"flag_mixerlowscale_minrange", 	COMMAND_ID_FLAG_MIXERLOWSCALE_MINRANGE, EEPROM_FLAG_MIXERLOWSCALE, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_16, .valueRange = {0, 2500}
		},
		[COMMAND_ID_FLAG_MIXERLOWSCALE_MAXRANGE] =
		{
			"flag_mixerlowscale_maxrange", 	COMMAND_ID_FLAG_MIXERLOWSCALE_MAXRANGE, EEPROM_FLAG_MIXERLOWSCALE, EEPROM_VALUE_TYPE_SYSTEM, 2, VAL_UINT_16, .valueRange = {0, 2500}
		},
		[COMMAND_ID_FLAG_MIXERLOWSCALE_CHANNEL] =
		{
			"flag_mixerlowscale_channel", 	COMMAND_ID_FLAG_MIXERLOWSCALE_CHANNEL, EEPROM_FLAG_MIXERLOWSCALE, EEPROM_VALUE_TYPE_SYSTEM, 4, VAL_UINT_8, .valueRange = {0, 18}
		},

		[COMMAND_ID_FLAG_FLIGHTMODE_3_MINRANGE] =
		{
			"flag_flightmode_3_minrange", 	COMMAND_ID_FLAG_FLIGHTMODE_3_MINRANGE, EEPROM_FLAG_FLIGHTMODE_3, EEPROM_VALUE_TYPE_SYSTEM, 0, VAL_UINT_16, .valueRange = {0, 2500}
		},
		[COMMAND_ID_FLAG_FLIGHTMODE_3_MAXRANGE] =
		{
			"flag_flightmode_3_maxrange", 	COMMAND_ID_FLAG_FLIGHTMODE_3_MAXRANGE, EEPROM_FLAG_FLIGHTMODE_3, EEPROM_VALUE_TYPE_SYSTEM, 2, VAL_UINT_16, .valueRange = {0, 2500}
		},
		[COMMAND_ID_FLAG_FLIGHTMODE_3_CHANNEL] =
		{
			"flag_flightmode_3_channel", 	COMMAND_ID_FLAG_FLIGHTMODE_3_CHANNEL, EEPROM_FLAG_FLIGHTMODE_3, EEPROM_VALUE_TYPE_SYSTEM, 4, VAL_UINT_8, .valueRange = {0, 18}
		},

		//PROFILE VALUES
		//PID VALUES
		//GYRO P
		[COMMAND_ID_PID_GYRO_P_ROLL] =
		{
				"pid_gyro_p_roll", 	COMMAND_ID_PID_GYRO_P_ROLL, EEPROM_PID_GYRO, EEPROM_VALUE_TYPE_PROFILE, 5, VAL_UINT_8, .valueRange = {0, 255}
		},
		[COMMAND_ID_PID_GYRO_P_PITCH] =
		{
				"pid_gyro_p_pitch", 	COMMAND_ID_PID_GYRO_P_PITCH, EEPROM_PID_GYRO, EEPROM_VALUE_TYPE_PROFILE, 6, VAL_UINT_8, .valueRange = {0, 255}
		},
		[COMMAND_ID_PID_GYRO_P_YAW] =
		{
				"pid_gyro_p_yaw", 	COMMAND_ID_PID_GYRO_P_YAW, EEPROM_PID_GYRO, EEPROM_VALUE_TYPE_PROFILE, 7, VAL_UINT_8, .valueRange = {0, 255}
		},

		//GYRO I
		[COMMAND_ID_PID_GYRO_I_ROLL] =
		{
				"pid_gyro_i_roll", 	COMMAND_ID_PID_GYRO_I_ROLL, EEPROM_PID_GYRO, EEPROM_VALUE_TYPE_PROFILE, 8, VAL_UINT_8, .valueRange = {0, 255}
		},
		[COMMAND_ID_PID_GYRO_I_PITCH] =
		{
				"pid_gyro_i_pitch", 	COMMAND_ID_PID_GYRO_I_PITCH, EEPROM_PID_GYRO, EEPROM_VALUE_TYPE_PROFILE, 9, VAL_UINT_8, .valueRange = {0, 255}
		},
		[COMMAND_ID_PID_GYRO_I_YAW] =
		{
				"pid_gyro_i_yaw", 	COMMAND_ID_PID_GYRO_I_YAW, EEPROM_PID_GYRO, EEPROM_VALUE_TYPE_PROFILE, 10, VAL_UINT_8, .valueRange = {0, 255}
		},

		//GYRO D
		[COMMAND_ID_PID_GYRO_D_ROLL] =
		{
				"pid_gyro_d_roll", 	COMMAND_ID_PID_GYRO_D_ROLL, EEPROM_PID_GYRO, EEPROM_VALUE_TYPE_PROFILE, 11, VAL_UINT_8, .valueRange = {0, 255}
		},
		[COMMAND_ID_PID_GYRO_D_PITCH] =
		{
				"pid_gyro_d_pitch", 	COMMAND_ID_PID_GYRO_D_PITCH, EEPROM_PID_GYRO, EEPROM_VALUE_TYPE_PROFILE, 12, VAL_UINT_8, .valueRange = {0, 255}
		},
		[COMMAND_ID_PID_GYRO_D_YAW] =
		{
				"pid_gyro_d_yaw", 	COMMAND_ID_PID_GYRO_D_YAW, EEPROM_PID_GYRO, EEPROM_VALUE_TYPE_PROFILE, 13, VAL_UINT_8, .valueRange = {0, 255}
		},

		//Gyro filter and limits
		[COMMAND_ID_PID_GYRO_DTERM_LPF] =
		{
				"pid_gyro_dterm_lpf", 	COMMAND_ID_PID_GYRO_DTERM_LPF, EEPROM_PID_GYRO, EEPROM_VALUE_TYPE_PROFILE, 14, VAL_UINT_16, .valueRange = {0, 255}
		},
		[COMMAND_ID_PID_GYRO_PTERM_YAW_LPF] =
		{
				"pid_gyro_pterm_yaw_lpf", 	COMMAND_ID_PID_GYRO_PTERM_YAW_LPF, EEPROM_PID_GYRO, EEPROM_VALUE_TYPE_PROFILE, 16, VAL_UINT_16, .valueRange = {0, 255}
		},
		[COMMAND_ID_PID_GYRO_YAW_P_LIMIT] =
		{
				"pid_gyro_yaw_lpf", 	COMMAND_ID_PID_GYRO_YAW_P_LIMIT, EEPROM_PID_GYRO, EEPROM_VALUE_TYPE_PROFILE, 18, VAL_UINT_16, .valueRange = {0, 255}
		},
		[COMMAND_ID_PID_GYRO_OUT_LIMIT] =
		{
				"pid_gyro_out_limit", 	COMMAND_ID_PID_GYRO_OUT_LIMIT, EEPROM_PID_GYRO, EEPROM_VALUE_TYPE_PROFILE, 20, VAL_UINT_16, .valueRange = {0, 255}
		},

		//ACCEL P
		[COMMAND_ID_PID_ACCEL_P_ROLL] =
		{
				"pid_accel_p_roll", 	COMMAND_ID_PID_ACCEL_P_ROLL, EEPROM_PID_ACCELEROMETER, EEPROM_VALUE_TYPE_PROFILE, 5, VAL_UINT_8, .valueRange = {0, 255}
		},
		[COMMAND_ID_PID_ACCEL_P_PITCH] =
		{
				"pid_accel_p_pitch", 	COMMAND_ID_PID_ACCEL_P_PITCH, EEPROM_PID_ACCELEROMETER, EEPROM_VALUE_TYPE_PROFILE, 6, VAL_UINT_8, .valueRange = {0, 255}
		},

		//ACCEL I
		[COMMAND_ID_PID_ACCEL_I_ROLL] =
		{
				"pid_accel_i_roll", 	COMMAND_ID_PID_ACCEL_I_ROLL, EEPROM_PID_ACCELEROMETER, EEPROM_VALUE_TYPE_PROFILE, 8, VAL_UINT_8, .valueRange = {0, 255}
		},
		[COMMAND_ID_PID_ACCEL_I_PITCH] =
		{
				"pid_accel_i_pitch", 	COMMAND_ID_PID_ACCEL_I_PITCH, EEPROM_PID_ACCELEROMETER, EEPROM_VALUE_TYPE_PROFILE, 9, VAL_UINT_8, .valueRange = {0, 255}
		},

		//ACCEL D
		[COMMAND_ID_PID_ACCEL_D_ROLL] =
		{
				"pid_accel_d_roll", 	COMMAND_ID_PID_ACCEL_D_ROLL, EEPROM_PID_ACCELEROMETER, EEPROM_VALUE_TYPE_PROFILE, 11, VAL_UINT_8, .valueRange = {0, 255}
		},
		[COMMAND_ID_PID_ACCEL_D_PITCH] =
		{
				"pid_accel_d_pitch", 	COMMAND_ID_PID_ACCEL_D_PITCH, EEPROM_PID_ACCELEROMETER, EEPROM_VALUE_TYPE_PROFILE, 12, VAL_UINT_8, .valueRange = {0, 255}
		},

		//ACCEL Filters and limits
		[COMMAND_ID_PID_ACCEL_DTERM_LPF] =
		{
				"pid_accel_dterm_lpf", 	COMMAND_ID_PID_ACCEL_DTERM_LPF, EEPROM_PID_ACCELEROMETER, EEPROM_VALUE_TYPE_PROFILE, 14, VAL_UINT_16, .valueRange = {0, 65000}
		},
		[COMMAND_ID_PID_ACCEL_PTERM_YAW_LPF] =
		{
				"pid_accel_pterm_yaw_lpf", 	COMMAND_ID_PID_ACCEL_PTERM_YAW_LPF, EEPROM_PID_ACCELEROMETER, EEPROM_VALUE_TYPE_PROFILE, 16, VAL_UINT_16, .valueRange = {0, 65000}
		},
		[COMMAND_ID_PID_ACCEL_YAW_P_LIMIT] =
		{
				"pid_accel_yaw_p_limit", 	COMMAND_ID_PID_ACCEL_YAW_P_LIMIT, EEPROM_PID_ACCELEROMETER, EEPROM_VALUE_TYPE_PROFILE, 18, VAL_UINT_16, .valueRange = {0, 65000}
		},
		[COMMAND_ID_PID_ACCEL_OUT_LIMIT] =
		{
				"pid_accel_out_limit", 	COMMAND_ID_PID_ACCEL_OUT_LIMIT, EEPROM_PID_ACCELEROMETER, EEPROM_VALUE_TYPE_PROFILE, 20, VAL_UINT_16, .valueRange = {0, 65000}
		},

		//BARO P
		[COMMAND_ID_PID_BARO_P_HEIGHT] =
		{
				"pid_baro_p_height", 	COMMAND_ID_PID_BARO_P_HEIGHT, EEPROM_PID_BAROMETER, EEPROM_VALUE_TYPE_PROFILE, 5, VAL_UINT_8, .valueRange = {0, 255}
		},

		//BARO I
		[COMMAND_ID_PID_BARO_I_HEIGHT] =
		{
				"pid_baro_i_height", 	COMMAND_ID_PID_BARO_I_HEIGHT, EEPROM_PID_BAROMETER, EEPROM_VALUE_TYPE_PROFILE, 8, VAL_UINT_8, .valueRange = {0, 255}
		},
		//BARO D
		[COMMAND_ID_PID_BARO_D_HEIGHT] =
		{
				"pid_baro_d_height", 	COMMAND_ID_PID_BARO_D_HEIGHT, EEPROM_PID_BAROMETER, EEPROM_VALUE_TYPE_PROFILE, 11, VAL_UINT_8, .valueRange = {0, 255}
		},

		//BARO Filters and limits
		[COMMAND_ID_PID_BARO_DTERM_LPF] =
		{
				"pid_baro_dterm_lpf", 	COMMAND_ID_PID_BARO_DTERM_LPF, EEPROM_PID_BAROMETER, EEPROM_VALUE_TYPE_PROFILE, 14, VAL_UINT_16, .valueRange = {0, 65000}
		},
		[COMMAND_ID_PID_BARO_PTERM_YAW_LPF] =
		{
				"pid_baro_pterm_yaw_lpf", 	COMMAND_ID_PID_BARO_PTERM_YAW_LPF, EEPROM_PID_BAROMETER, EEPROM_VALUE_TYPE_PROFILE, 16, VAL_UINT_16, .valueRange = {0, 65000}
		},
		[COMMAND_ID_PID_BARO_YAW_P_LIMIT] =
		{
				"pid_baro_yaw_p_limit", 	COMMAND_ID_PID_BARO_YAW_P_LIMIT, EEPROM_PID_BAROMETER, EEPROM_VALUE_TYPE_PROFILE, 18, VAL_UINT_16, .valueRange = {0, 65000}
		},
		[COMMAND_ID_PID_BARO_OUT_LIMIT] =
		{
				"pid_baro_out_limit", 	COMMAND_ID_PID_BARO_OUT_LIMIT, EEPROM_PID_BAROMETER, EEPROM_VALUE_TYPE_PROFILE, 20, VAL_UINT_16, .valueRange = {0, 65000}
		},

		/* Enable pid loops */
		[COMMAND_ID_PID_GYRO_ISENABLED] =
		{
				"pid_gyro_isenabled", 	COMMAND_ID_PID_GYRO_ISENABLED, EEPROM_PID_GYRO, EEPROM_VALUE_TYPE_PROFILE, 0, VAL_BOOL, .valueRange = {0, 1}
		},
		[COMMAND_ID_PID_ACCEL_ISENABLED] =
		{
				"pid_accel_isenabled", 	COMMAND_ID_PID_ACCEL_ISENABLED, EEPROM_PID_ACCELEROMETER, EEPROM_VALUE_TYPE_PROFILE, 0, VAL_BOOL, .valueRange = {0, 1}
		},
		[COMMAND_ID_PID_BARO_ISENABLED] =
		{
				"pid_baro_isenabled", 	COMMAND_ID_PID_BARO_ISENABLED, EEPROM_PID_BAROMETER, EEPROM_VALUE_TYPE_PROFILE, 0, VAL_BOOL, .valueRange = {0, 1}
		},


};

/***********************************************************************
* BRIEF: 	   Initiates the CLI                                  	   *
* INFORMATION: The function initiates the CLI. To do this it will need *
* 			   a usart that it should receive its commands from.	   *
***********************************************************************/
void cliInit(USART_TypeDef* usart)
{
	usart_init(usart, &cliUsart, CLI_baudRate, STOP_BITS_1, PARITY_NONE);
}

/***********************************************************************
* BRIEF: 	   Function that checks if the CLI asigned usart has a new *
* 			   message that can be read. 						   	   *
* INFORMATION: If there is a new message in the designated usart the   *
* 		   	   function will return true, otherwise false.			   *
***********************************************************************/
bool cliHasMessage()
{
	return usart_poll_data_ready(&cliUsart);
}

/***********************************************************************
* BRIEF:	   Reads a cahracter from the usart and checks if it       *
* 		       is the start character for the CLI.					   *
* INFORMATION: Will read a character from the usart and compare if it  *
*              is the character that needs to be read to start the CLI *
***********************************************************************/
bool cliShouldRun()
{
	uint8_t buffer[1] = {0};
	usart_poll(&cliUsart, buffer, 1, 10);

	//since the message to start cli is "#" we should only read two bytes: (#) and (\r)
	if (buffer[0] == cliActivateCharacter)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/***********************************************************************
* BRIEF:	   Transmits a message on the usart.					   *
* INFORMATION: Given a string it will try to send it over usart.       *
***********************************************************************/
void TransmitBack(char msg[maxStringSize_CLI])
{
	//Transmits one character at a time over the usart
	for (int i = 0; i < maxStringSize_CLI && msg[i] != 0; i++)
	{
		uint8_t toSend = msg[i];
		usart_transmit(&cliUsart, &toSend, 1, 100);
	}
}

/***********************************************************************
* BRIEF:	   Given a string searches for all ACTIONS that start with *
*              the same string.										   *
* INFORMATION: Given a string it will search through all ACTIONS in    *
* 			   to see if any of them match the given message. It will  *
* 			   search the from the start of each string to check with  *
* 			   the length of the input message.						   *
***********************************************************************/
void findSimilarActions(char msg[maxStringSize_CLI], typeString  similarCommands[maxSimilarSearchValues])
{
	uint16_t similarCommandsCount = 0;	//Number of similar commands found
	uint16_t msgCharLength = calculateStringLength(msg, maxStringSize_CLI);		//Length of the message to search on

	//Loop through the commandTable or until similar commands is full
	for (int i = 0; i < ACTION_COUNT && similarCommandsCount <= maxSimilarSearchValues; i++)
	{
		//If the length of the search value is greater than the one we want to compare it cant be that one
		if(msgCharLength > calculateStringLength(commandAction_Strings[i], maxStringSize_CLI))
			continue;

		//Check if the message has any action that is similar to the message
		char tempString[maxStringSize_CLI] = {0};
		strncpy (tempString, commandAction_Strings[i], msgCharLength);
		if (strcmp(msg, tempString) == 0)
		{
			//completely reset the current string in the similarComands array
			memset(&similarCommands[i][0], 0, maxStringSize_CLI);

			//Copy the string over to the return array
			int tempSize = calculateStringLength(commandAction_Strings[i], maxStringSize_CLI);
			strncpy(similarCommands[similarCommandsCount], commandAction_Strings[i], tempSize);
			similarCommandsCount++;
		}
	}

}

/***********************************************************************
* BRIEF:	   Given a string searches for all COMMANDS that start with*
*              the same string.										   *
* INFORMATION: Given a string it will search through all COMMANDS in   *
* 			   to see if any of them match the given message. It will  *
* 			   search the from the start of each string to check with  *
* 			   the length of the input message.						   *
***********************************************************************/
void findSimilarCommands(const char msg[maxStringSize_CLI], typeString  similarCommands[maxSimilarSearchValues])
{
	uint16_t similarCommandsCount = 0;	//Number of similar commands found
	uint16_t msgCharLength = calculateStringLength(msg, maxStringSize_CLI);		//Length of the message to search on

	if (msgCharLength < minSimilarCharacters)
		return;

	//Loop through the commandTable or until similar commands is full
	for (int i = 0; i < COMMAND_ID_COUNT && similarCommandsCount < maxSimilarSearchValues; i++)
	{
		//If the length of the search value is greater than the one we want to compare it cant be that one
		if(msgCharLength > calculateStringLength(commandTable[i].name, maxStringSize_CLI))
			continue;

		//Check if the message has any action that is similar to the message
		char tempString[maxStringSize_CLI] = {0};
		strncpy (tempString, commandTable[i].name, msgCharLength);
		if (strcmp(msg, tempString) == 0)
		{
			//completely reset the current string in the similarComands array
			memset(&similarCommands[similarCommandsCount][0], 0, maxStringSize_CLI);

			//Copy the string over to the return array
			int tempSize = calculateStringLength(commandTable[i].name, maxStringSize_CLI);
			strncpy(similarCommands[similarCommandsCount], commandTable[i].name, tempSize);
			similarCommandsCount++;
		}
	}

}

/***********************************************************************
* BRIEF:       Finds possible commands given a message and transmits   *
* 			   them over usart.  				 					   *
* INFORMATION: Given a certain message similar commands will be found. *
* 		       If any are found they will be transmitted over the usart*
***********************************************************************/
void TransmitPossibleSimilarCommands(char * msg, int minCharAmount)
{
	char msgToCheck[maxStringSize_CLI];
	typeString similarCommands[maxSimilarSearchValues];
	int msgLength = calculateStringLength(msg, maxStringSize_CLI);	//Calculate the length of the message

	if (msgLength < minCharAmount)	//Check if the given message is longer than the amount of characters we want ot compare with
		return;

	strncpy (msgToCheck, msg, minCharAmount);	//Copy the characters we want to compare with
	findSimilarCommands(msgToCheck, similarCommands);	//get all the similar commands based on the text we send in
	int similarCount = calculateTypeStringLength(similarCommands, maxSimilarSearchValues);	//Get the count of the amount of similar values

	TransmitBack("- Possible commands: \n\r");
	for (int i = 0; i < similarCount; i++)	//loop through all the values
	{
		char buffer[maxStringSize_CLI];
		sprintf(buffer,"-- %s \n\r", similarCommands[i]);
		TransmitBack(buffer);
	}
	TransmitBack("---------------------------------\n\n\r");
}

/***********************************************************************
* BRIEF:       Tries to get the ID of the action.
* INFORMATION: Given a string of an action it tries to match it to an
* 			   action ID.
***********************************************************************/
uint8_t getCommandAction(char msg[maxStringSize_CLI])
{
	for (int i = 0; i < ACTION_COUNT; i++)
	{
		if (strcmp(msg, commandAction_Strings[i]) == 0)
		{
			return i;
		}
	}
	return ACTION_NOACTION;
}

/***********************************************************************
* BRIEF:	   Tries to get the id of a command      				   *
* INFORMATION: Given a string of an command it tries to match it to a  *
* 			   command id											   *
***********************************************************************/
uint16_t getCommandId(char * msg)
{
	for (int i = 0; i < COMMAND_ID_COUNT; i++)
	{
		if (strcmp(msg, commandTable[i].name) == 0)
		{
			return commandTable[i].commandId;
		}
	}
	return COMMAND_ID_NO_COMMAND;
}

/***********************************************************************
* BRIEF:       Parses a string into a value.
* INFORMATION: Parses a string into a value and returns it
***********************************************************************/
uint64_t getCommanValue(char * msg)
{
	//Check if the msg are only valid numbers (0-9). If they are not retun max value of 64 bit int that will serve as an error
	if (!isStringNumbers(msg))
	{
		return commandValueError;
	}
	return parseToInt64(msg);
}

/***********************************************************************
* BRIEF:       Tries to process a command to set a value
* INFORMATION: Tries to set the process a set command. Given and
* 			   action and command ID and a numerical value. It will
* 			   check what value the action wants to change checks if it
* 			   is within the given range and if it is it will change it
***********************************************************************/
bool processCommand(uint8_t action, uint16_t id, uint64_t value)
{
	//check if value is within the allowed area, if not return
	if ( !(value >= commandTable[id].valueRange.min && value <= commandTable[id].valueRange.max) )
	{
		char buffer[maxStringSize_CLI];
		sprintf(buffer,"Value not within ranges: %d - %d", (int)commandTable[id].valueRange.min, (int)commandTable[id].valueRange.max);
		TransmitBack(buffer);
		return false;
	}

	//Get the correct pointer to the data
	void * valuePointer = getDataAddresFromID(commandTable[id].valueId, commandTable[id].valueIdLoc);

	//adjust to the correct addres with the offset
	valuePointer += commandTable[id].addressValueOffset;

	//switch on the value that this command should give
	switch(commandTable[id].valueType)
	{
		case VAL_UINT_8:
			*((uint8_t *)valuePointer) = value;
			break;
		case VAL_INT_8:
			*((int8_t *)valuePointer) = value;
			break;
		case VAL_UINT_16:
			*((uint16_t *)valuePointer) = value;
			break;
		case VAL_INT_16:
			*((int16_t *)valuePointer) = value;
			break;
		case VAL_UINT_32:
			*((uint32_t *)valuePointer) = value;
			break;
		case VAL_INT_32:
			*((int32_t *)valuePointer) = value;
			break;
		case VAL_UINT_64:
			*((uint64_t *)valuePointer) = value;
			break;
		case VAL_INT_64:
			*((int64_t *)valuePointer) = value;
			break;
		case VAL_BOOL:
			*((bool *)valuePointer) = value;
			break;
	}

	return true;
}

void valuePointerToString(char * dst, uint16_t id, void * address)
{
	switch(commandTable[id].valueType)
	{
		case VAL_UINT_8:
			sprintf(dst,"%d", *((uint8_t *)address));
			break;
		case VAL_INT_8:
			sprintf(dst,"%d", *((int8_t *)address));
			break;
		case VAL_UINT_16:
			sprintf(dst,"%d", *((uint16_t *)address));
			break;
		case VAL_INT_16:
			sprintf(dst,"%d", *((int16_t *)address));
			break;
		case VAL_UINT_32:
			sprintf(dst,"%d", *((uint32_t *)address));
			break;
		case VAL_INT_32:
			sprintf(dst,"%d", *((int32_t *)address));
			break;
		case VAL_UINT_64:
			sprintf(dst,"%d", *((uint64_t *)address));
			break;
		case VAL_INT_64:
			sprintf(dst,"%d", *((int64_t *)address));
			break;
		case VAL_BOOL:
			sprintf(dst,"%d", *((bool *)address));
			break;
	}
}

/***********************************************************************
* BRIEF:	   Will loop and read input from serial.
* INFORMATION: Will read input for the serial in a loop and will add
*              everything read into a string and also display it in
*              terminal by transmitting what is read back. The loop
*              will break when a enter has been read, this equals the
*              end of a command. This string will be saved in msg and
*              can be used afterwards to identify if a valid command
*              has been received.
***********************************************************************/
void ReceiveCommand(char msg[maxStringSize_CLI], int msgSize)
{
	//loop until enter or end of msgsize
	uint8_t c = 0;
	uint8_t c_blank = 32;
	char esteticString[3] = "#: ";
	uint8_t c_backspace= 8;
	uint32_t readBytes = 0;
	typeString suggestionsArr[maxSimilarSearchValues]  = {0};	//Maximum of 10 suggestions can be stored
	int suggestionAmount = 0;
	int suggestedCounter = 0;
	int lastSpaceIndex = 0;
	bool suggestNew = true;
	int counter = 0;

	//The last char in the final message must always be and \r
	msg[msgSize-1] = '\r';

	//add estetic part to the write
	TransmitBack(esteticString);

	//Loop untill we are done or our max message have been received
	while(counter < msgSize-1)
	{
		//Will loop and try to read something until it has read something
		while(readBytes = usart_poll(&cliUsart, &c, 1, 10000), readBytes <= 0);

		//if an enter(\r) has been read the message is considered complete
		if (c == '\r')
		{
			msg[counter] = c;		//Add the read value to the out value
			TransmitBack("\r>>>");	//Show in terminal that was written was a previous command and it is marked by this transmit
			break;
		}
		if (c == '\b')	//backspace
		{
			if (counter <= 0)
				continue;

			//Decrease the counter since we remove
			counter --;

			//If we use suggest new it needs to check again next time
			suggestNew = true;

			//Check if a blank space is being erased and the find the last one
			if (msg[counter] == ' ')
			{
				int tempVal =  FindPreviousWhiteSpaceIndex(msg,counter-1);
				lastSpaceIndex = (tempVal == 0) ? 0 : tempVal+1;
			}

			//set the value for this counter to null since it should be nothing, meaning we erase it
			msg[counter] = 0;

			//special transmit back that removes one displayed character so it looks correct
			usart_transmit(&cliUsart, &c, 1, 1000000000);
			usart_transmit(&cliUsart, &c_blank, 1, 100000000);
			usart_transmit(&cliUsart, &c, 1, 1000000000);
		}
		else if (c == 9)		//Tabb was pressed, should try and find a value for the imputed text
		{
			//This part works, next fucks it up
			if (suggestNew == true)
			{
				//Reset suggestion array with 0 at the start
				for (int i = 0; i < maxSimilarSearchValues; i++)
					suggestionsArr[i][0] = 0;

				//if we are at the start and have not written anything switch on the possible actions
				if (lastSpaceIndex == 0)
				{
					//find similar actions
					findSimilarActions(msg, suggestionsArr);
				}
				else
				{
					//find similar commands
					findSimilarCommands(msg+lastSpaceIndex, suggestionsArr);
				}
				suggestionAmount = calculateTypeStringLength(suggestionsArr,maxSimilarSearchValues);
				suggestNew = false;
				suggestedCounter = 0;
			}
			if (suggestNew == false && suggestionAmount > 0)
			{
				//Get the length of the current message shown in the terminal
				int tempMsgSize = calculateStringLength(msg+lastSpaceIndex, maxStringSize_CLI);
				int totMsgSize = calculateStringLength(msg, maxStringSize_CLI);
				int i_stop = totMsgSize - tempMsgSize;

				//Erase what is shown in the terminal right now
				for (int i = totMsgSize; i > 0; i--)
				{
					usart_transmit(&cliUsart, &c_backspace, 1, 1000000000);
					usart_transmit(&cliUsart, &c_blank, 1, 100000000);
					usart_transmit(&cliUsart, &c_backspace, 1, 1000000000);

					if (i > i_stop)
						msg[i-1] = 0;
				}

				//Get the length of the suggested string
				int suggestionStringLength = calculateStringLength(suggestionsArr[suggestedCounter], maxStringSize_CLI);

				//Add the suggested string to the end of the read message, also increase the counter
				strncpy (msg+lastSpaceIndex, suggestionsArr[suggestedCounter], suggestionStringLength);
				counter = calculateStringLength(msg, maxStringSize_CLI);

				//Show the updated message
				TransmitBack(msg);
				suggestedCounter = (suggestedCounter+1 < suggestionAmount) ? suggestedCounter + 1 : 0;

			}
		}
		else	//standard, just some key was sent
		{
			//check if it is a apace then set last space counter
			if (c == ' ')
				lastSpaceIndex = counter+1;

			//If we use suggest new it needs to check again next time
			suggestNew = true;

			msg[counter] = c;	//Add the read value to the out value
			usart_transmit(&cliUsart, &c, 1, 100);	//Send the read message back to display in the terminal
			counter ++;			//Increment counter
		}

		HAL_Delay(1);
	}

	//add a new line every time
	TransmitBack("\n\n\r");
}

/***********************************************************************
* BRIEF:       Get a binary decision from serial
* INFORMATION: Will perform a binary decision based on the on and off
* 			   input parameters. If on is read true will be returned if
* 			   off is read false will be returned. This values are
* 			   numbers in the ascii table. The function will read from
* 			   serial until one of the values for on or off has been
* 			   read.
***********************************************************************/
bool ReceiveBinaryDecision(uint8_t on, uint8_t off)
{
	uint8_t c;
	uint32_t readBytes = 0;

	while(true)
	{
		//Read until either on or off have been read from the serial
		while(readBytes = usart_poll(&cliUsart, &c, 1, 10000), readBytes <= 0);

		if (c == on)
			return true;
		else if (c == off)
			return false;
	}
}

/***********************************************************************
* BRIEF:	   Transmits the signature of a command
* INFORMATION: Transmits over usart the signature how to write a
* 		       specific command id.
***********************************************************************/
void TransmitCommandInstruction(uint8_t id)
{
	TransmitBack("- The signature for ");
	TransmitBack(commandAction_Strings[id]);
	TransmitBack(" is: '");
	TransmitBack(commandActionSignature_Strings[id]);
	TransmitBack("'...\n\r");
}

/***********************************************************************
* BRIEF:	   Writes the value of a that a command is associated with.
* INFORMATION: Given a command id it will display information about
* 			   the values that the command manipulates.
***********************************************************************/
void writeTaskInformation(uint16_t id)
{
	//buffer
	char buffer[maxStringSize_CLI];
	char valBuffer[16];
	//Get the correct pointer to the data
	void * valuePointer = getDataAddresFromID(commandTable[id].valueId, commandTable[id].valueIdLoc);

	//adjust to the correct addres with the offset
	valuePointer += commandTable[id].addressValueOffset;
	valuePointerToString(valBuffer, id, valuePointer);
	//Formats the string so that it will be printed nicely
	sprintf(buffer,"- %-40s  Value: %-20s allowed range: %d - %d", commandTable[id].name, valBuffer, (int)commandTable[id].valueRange.min, (int)commandTable[id].valueRange.max);
	TransmitBack(buffer);
	TransmitBack("\n\r");
}

/***********************************************************************
* BRIEF:       Will transmit some system stats over the usart
* INFORMATION: Will transmit some system stats, including scheduler
* 			   data, individial task data potentially more.
***********************************************************************/
void TransmitSystemStats()
{
	char * buffer[maxStringSize_CLI*2];
	float taskExecEfficiency = ((float)systemSchedulerStatistics.totalTaskRuntime/(float)systemSchedulerStatistics.totalSystemRuntime)*100.f;
	float taskschedulingUptime = ((float)systemSchedulerStatistics.totalTasksScheduled/(float)systemSchedulerStatistics.totalSchedulerCalls)*100.f;
	float averageOverHead = ((float)systemSchedulerStatistics.totalOverHead/(float)systemSchedulerStatistics.totalSchedulerCalls)*100.f;
	//initial start message                                 "
	TransmitBack("- System statistics\n\r-------------------\n\n\r- Scheduler stats: \n\r");
	TransmitBack("------------------------------------------------------\n\r");
	sprintf(buffer,"- %-35s%-16d ms\n\r", "System runtime:", systemSchedulerStatistics.totalSystemRuntime/1000);
	TransmitBack(buffer);
	sprintf(buffer,"- %-35s%-16d ms\n\r", "Tasks runtime:", systemSchedulerStatistics.totalTaskRuntime/1000);
	TransmitBack(buffer);
	sprintf(buffer,"- %-35s%-16d ms\n\r", "Overhead:", systemSchedulerStatistics.totalOverHead/1000);
	TransmitBack(buffer);
	sprintf(buffer,"- %-35s%-16d micro\n\r", "Average Overhead:", ((int)taskschedulingUptime));
	TransmitBack(buffer);
	sprintf(buffer,"- %-35s%-16d percent\n\r", "Task exec by runtime:", ((int)taskExecEfficiency));
	TransmitBack(buffer);
	sprintf(buffer,"- %-35s%-16d times\n\r", "Scheduler calls:", systemSchedulerStatistics.totalSchedulerCalls);
	TransmitBack(buffer);
	sprintf(buffer,"- %-35s%-16d times\n\r", "Task scheduled:", systemSchedulerStatistics.totalTasksScheduled);
	TransmitBack(buffer);
	sprintf(buffer,"- %-35s%-16d percent\n\r", "Task schedule load:", ((int)taskschedulingUptime));
	TransmitBack(buffer);
	sprintf(buffer,"- %-35s%-16d times\n\r", "Missed periods:", systemSchedulerStatistics.totalMissedPeriods);
	TransmitBack(buffer);


	TransmitBack("\n\n\r- Individual task stats: \n\r");
	TransmitBack("------------------------------------------------------\n\r");
	for (int i = 0; i < TASK_COUNT; i++)
	{
		sprintf(buffer,"- %-35s%s\n\r", "Task name: ", SystemTasks[i].taskName);
		TransmitBack(buffer);
		sprintf(buffer,"- %-35s%-16d ms\n\r", "Total execution time: ", SystemTasks[i].totalExecutionTime/1000);
		TransmitBack(buffer);
		sprintf(buffer,"- %-35s%-16d micro\n\r", "Max execution time: ", SystemTasks[i].maxExecutionTime);
		TransmitBack(buffer);
		sprintf(buffer,"- %-35s%-16d micro\n\r", "Average execution time: ", SystemTasks[i].averageExecutionTime);
		TransmitBack(buffer);
		sprintf(buffer,"- %-35s%-16d micro\n\r", "Average time between exec: ", SystemTasks[i].taskAverageDeltaTime);
		TransmitBack(buffer);

		TransmitBack("\n\r");
	}


}

/***********************************************************************
* BRIEF:       Will print informations of all the the commands
* INFORMATION: Will loop through all the commands in the system and
* 			   display all their information by calling
* 			   writeTaskInformation.
***********************************************************************/
void dumpTaskInformation()
{
	char buffer[maxStringSize_CLI];
	sprintf(buffer, "Active profile: %d\n\n\r", getActiveProfile());
	TransmitBack(buffer);
	for (int i = 0; i < COMMAND_ID_COUNT; i++)
	{
		writeTaskInformation(commandTable[i].commandId);
	}
	//usart_send("----------------------\n\r");
}


/***********************************************************************
* BRIEF:       The main loop of the cli
* INFORMATION: The main loop of the cli that will read a message and
*              thereafter handle it accordingly.
***********************************************************************/
void cliRun()
{
	//set running flag to true, when it should stop it should be set to false
	cliIsRunning = true;

	//start message to send that tells we have started CLI
	TransmitBack("\n\r- Entered CLI MODE...\n\n\r");

	//Will run until it is decided to stop
	while(cliIsRunning)
	{
		/* Poll reading of a message until we receive and enter, or message size overflow */
		char msg[maxStringSize_CLI] = {0};			//msg to be read from the usart
		ReceiveCommand(msg, maxStringSize_CLI);

		//split string, first item should be and action, if second item should be id, last if used should be value
		typeString msgArr[msgArraySize] = {0};
		if (!splitStringWhitespace(msg, msgArr, maxStringSize_CLI, msgArraySize))
		{
			TransmitBack("- Wrong command format... \n\n\r");

			//Message was invalid do not try to process it
			continue;
		}

		//Get the amount of words in the command
		uint32_t msgArrLength = calculateTypeStringLength(msgArr, msgArraySize);
		if(msgArrLength <= 0)
		{
			TransmitBack("- No command sent... \n\n\r");
			continue;
		}


		uint8_t actionId = getCommandAction(msgArr[arrayPos_Action]);	//First check what type of action the command is, gives an id value for the action
		uint16_t commandId;												//Id for the command
		uint64_t commandValue;											//Value of a command
		bool processSuccessfull;										//Bool for if the command was processed successfully

		switch (commandMask(msgArrLength, actionId))
		{
			case commandMask(commandSize_3, ACTION_SET_VALUE):
				//Obtain the command id if it is exsits, otherwise produce an error
				if ((commandId = getCommandId(msgArr[arrayPos_Command])) == COMMAND_ID_NO_COMMAND )
				{
					TransmitBack("- ERROR: No valid variable given...\n\n\r");

					//If the command is close to any other command those will be written back over usart
					TransmitPossibleSimilarCommands(msgArr[arrayPos_Command], 3);
					break;
				}

				//Get the value of the command
				if ((commandValue = getCommanValue(msgArr[arrayPos_Value])) == commandValueError)
				{
					TransmitBack("- ERROR: The value provided are not numerical...\n\n\r");
					break;
				}

				//Try to process a given message
				processSuccessfull = processCommand(actionId, commandId, commandValue);
				if (processSuccessfull)
				{
					//Updated value correctly
					TransmitBack("- Updated value correctly...\n\r");
					writeTaskInformation(commandId);
					TransmitBack("\n\r");
					valueChanged = true;
				}
				else
				{
					//could not update value
					TransmitBack("- Value not updated...\n\n\r");
				}
				break;
			case commandMask(commandSize_1, ACTION_DUMP):
				//dump informaton for all values that can be accessed in the commandTable
				TransmitBack("Value of variables: \n\n\r");
				dumpTaskInformation();
				TransmitBack("\n-------------------------------------------------- \n\r");
				break;
			case commandMask(commandSize_1, ACTION_SAVE):
				//Write all the values that have been changed to the eprom
				valueChanged = false;
				saveEEPROM();
				TransmitBack("- Values successfully saved to EEPROM... \n\n\r");
				break;
			case commandMask(commandSize_1, ACTION_UNDO):
				//Undp all things that have not been saved by reading the current saved values in the eeprom
				valueChanged = false;
				readEEPROM();
				TransmitBack("- All unsaved values discarded... \n\n\r");
				break;
			case commandMask(commandSize_1, ACTION_HELP):
				TransmitBack("- Updates the values for variables in the system. To get a list of the commands write: commands.\n\n\r");
				break;
			case commandMask(commandSize_2, ACTION_GET_INFORMATION):
				if ((commandId = getCommandId(msgArr[arrayPos_Command])) == COMMAND_ID_NO_COMMAND )
					{
						//ToDo: add so that suggestions of what could be written instead is sent back
						TransmitBack("- ERROR: No valid variable given...\n\n\r");
						break;
					}
				writeTaskInformation(commandId);
				break;
			case commandMask(commandSize_1, ACTION_COMMANDS):
				//Gives information on all the tasks
				TransmitBack("- All the commands that can be used: \n\r");
				for(int i=0; i < ACTION_COUNT; i++)
				{
					TransmitBack(commandActionInformation_Strings[i]);
				}
				TransmitBack("------------------------------------ \n\n\r");
				break;
			case commandMask(commandSize_2, ACTION_PROFILE):
				//Get the value of the command
				if ((commandValue = getCommanValue(msgArr[arrayPos_Value-1])) == commandValueError)
				{
					TransmitBack("- ERROR: The value provided are not numerical...\n\n\r");
					break;
				}

				//check if the value is within the ranges for possible profiles
				if (!(commandValue >= 1 && commandValue <=3))
				{
					TransmitBack("- ERROR: A profile value must be between 1-3...\n\n\r");
				}

				valueChanged = false;
				setActiveProfile(commandValue);
				TransmitBack("- Changed profile... \n\n\r");

				break;
			case commandMask(commandSize_1, ACTION_EXIT):

				if (valueChanged == true)
				{
					TransmitBack("- Changes not saved, Want to save changes? (y/n)... \n\n\r");

					//read until a y or n is found
					if (ReceiveBinaryDecision(121, 110))
					{
						saveEEPROM();
						TransmitBack("- Values successfully saved to EEPROM... \n\n\r");
					}
				}
				TransmitBack("- Exiting CLI... \n\n\r");
				cliIsRunning = false;
				valueChanged = false;
				break;
			case commandMask(commandSize_1, ACTION_REBOOT):
				if (valueChanged == true)
				{
					TransmitBack("- Changes not saved, Want to save changes? (y/n)... \n\n\r");

					//read until a y or n is found
					if (ReceiveBinaryDecision(121, 110))
					{
						saveEEPROM();
						TransmitBack("- Values successfully saved to EEPROM... \n\n\r");
					}
				}

				TransmitBack("- Exiting CLI and Rebooting system...\n\n\r");
				cliIsRunning = false;
				HAL_NVIC_SystemReset();
				break;
			case commandMask(commandSize_1, ACTION_RESET):
				//resets all the values in the eeprom to the default values
				TransmitBack("- All values will be deleted and system rebooted. Continue? (y/n)... \n\n\r");

				//read until a y or n is found
				if (ReceiveBinaryDecision(121, 110))
				{
					defaultEEPROM();
				}
				else
				{
					TransmitBack("- Values unchanged...\n\n\r");
				}
				break;
			case commandMask(commandSize_1, ACTION_STATS):
					TransmitSystemStats();
				break;
			case commandMask(commandSize_1, ACTION_MOTORCALIBRATE):
					TransmitBack("- Make sure PROPELLERS are REMOVED and be prepared that the aircraft my start the engines\n\n\r!");
					TransmitBack("- REMOVE HANDS FROM THE AIRCRAFT\n\n\r");

					TransmitBack("Do you really want to calibrate? (y/n)\n\n\r");
					if (ReceiveBinaryDecision(121,110))
					{
						TransmitBack("Starting calibration, BE CAREFUL!!! \n\n\r");
						calibrateMotors();
						TransmitBack("Wait until the beeping have ceased... \n\n\r");
					}
					else
					{
						TransmitBack("Exiting the calibration, BE CAREFUL!!! \n\n\r");
					}

					break;
			case commandMask(commandSize_1, ACTION_GYROCALIBRATE):
					TransmitBack("Do you really want to calibrate the gyro? (y/n)\n\n\r");
					if (ReceiveBinaryDecision(121,110))
					{
						TransmitBack("Starting calibration! \n\n\r");
						TransmitBack("NOT IMPLEMENTED! \n\n\r");
						TransmitBack("Calibration complete! \n\n\r");
					}
					else
					{
						TransmitBack("Exiting the calibration! \n\n\r");
					}

					break;
			case commandMask(commandSize_1, ACTION_ACCELEROMTETERCALIBRATE):
					TransmitBack("Do you really want to calibrate the accelerometer? (y/n)\n\n\r");
					if (ReceiveBinaryDecision(121,110))
					{
						TransmitBack("Starting calibration! \n\n\r");
						mpu6000_read_acc_offset(&accelProfile);
						TransmitBack("Calibration complete! \n\n\r");
					}
					else
					{
						TransmitBack("Exiting the calibration! \n\n\r");
					}

					break;
			case commandMask(commandSize_1, ACTION_COMPASSCALIBRATE):
					TransmitBack("Do you really want to calibrate the compass? (y/n)\n\n\r");
					if (ReceiveBinaryDecision(121,110))
					{
						TransmitBack("Starting calibration! \n\n\r");
						calibrate_compass();
						TransmitBack("Calibration complete! \n\n\r");
					}
					else
					{
						TransmitBack("Exiting the calibration! \n\n\r");
					}

					break;
			case commandMask(commandSize_1, ACTION_CALIBRATIONINFOACCEL):
					{
					char tempBuffer[100];
					TransmitBack("- Accelerometer calibration information: \n\n\r");
					sprintf(tempBuffer, "- Finetune values: \n\r- Pitch: %0.2f \n\r- Roll: %0.2f \n\n\r", accPitchFineTune, accRollFineTune);
					TransmitBack(tempBuffer);
					sprintf(tempBuffer, "- BaseTune: \n\r-OffsetX: %d \n\r-OffsetY: %d \n\r-OffsetZ: %d \n\n\r", accelProfile.offsetX, accelProfile.offsetY, accelProfile.offsetZ);
					TransmitBack(tempBuffer);

					break;
					}
			default:
				if (actionId != ACTION_NOACTION)
					TransmitCommandInstruction(actionId);
				TransmitBack("- For valid commands type: 'commands'\n\n\r");
				break;
		}
	}
}

