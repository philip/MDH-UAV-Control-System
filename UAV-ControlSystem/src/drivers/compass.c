#include "drivers/compass.h"

#include "drivers/arduino_com.h"


#define sq(x) ((x)*(x))
#define map(x, in_min, in_max, out_min, out_max) (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min


float MagnetFilteredOld[3];
float alphaMagnet = 0.4;
int MagnetMax[3];
int MagnetMin[3];
float MagnetMap[3];
float Yaw;
float YawU;


bool initialize_compass()
{

}

void calibrate_compass()
{

}

void calculate_heading()
{

//	readAcc();
//		float xMagnetFiltered = 0;
//		float yMagnetFiltered = 0;
//		float zMagnetFiltered = 0;
//		xMagnetFiltered = MagnetFilteredOld[0] + alphaMagnet * (compass_data.x - MagnetFilteredOld[0]);
//		yMagnetFiltered = MagnetFilteredOld[1] + alphaMagnet * (compass_data.y - MagnetFilteredOld[1]);
//		zMagnetFiltered = MagnetFilteredOld[2] + alphaMagnet * (compass_data.z - MagnetFilteredOld[2]);
//
//		MagnetFilteredOld[0] = xMagnetFiltered;
//		MagnetFilteredOld[1] = yMagnetFiltered;
//		MagnetFilteredOld[2] = zMagnetFiltered;
//
//
//		//this part is required to normalize the magnetic vector
//		if (xMagnetFiltered>MagnetMax[0]) { MagnetMax[0] = xMagnetFiltered; }
//		if (yMagnetFiltered>MagnetMax[1]) { MagnetMax[1] = yMagnetFiltered; }
//		if (zMagnetFiltered>MagnetMax[2]) { MagnetMax[2] = zMagnetFiltered; }
//
//		if (xMagnetFiltered<MagnetMin[0]) { MagnetMin[0] = xMagnetFiltered; }
//		if (yMagnetFiltered<MagnetMin[1]) { MagnetMin[1] = yMagnetFiltered; }
//		if (zMagnetFiltered<MagnetMin[2]) { MagnetMin[2] = zMagnetFiltered; }
//
//		float norm;
//
//		MagnetMap[0] = (float)(map(xMagnetFiltered, MagnetMin[0], MagnetMax[0], -10000, 10000)) / 10000.0;
//		MagnetMap[1] = (float)(map(yMagnetFiltered, MagnetMin[1], MagnetMax[1], -10000, 10000)) / 10000.0;
//		MagnetMap[2] = (float)(map(zMagnetFiltered, MagnetMin[2], MagnetMax[2], -10000, 10000)) / 10000.0;
//
//		//normalize the magnetic vector
//		norm = sqrt(sq(MagnetMap[0]) + sq(MagnetMap[1]) + sq(MagnetMap[2]));
//		MagnetMap[0] /= norm;
//		MagnetMap[1] /= norm;
//		MagnetMap[2] /= norm;
//
////		compare Applications of Magnetic Sensors for Low Cost Compass Systems by Michael J. Caruso
////		for the compensated Yaw equations...
////		http://www.ssec.honeywell.com/magnetic/datasheets/lowcost.pdf
//		Yaw = atan2(-(MagnetMap[1] * cos(accelProfile.rollAngle) +
//					  MagnetMap[2] * sin(accelProfile.rollAngle)),
//					  MagnetMap[0] * cos(accelProfile.pitchAngle) +
//					  MagnetMap[1] * sin(accelProfile.pitchAngle) * sin(accelProfile.rollAngle) +
//					  MagnetMap[2] * sin(accelProfile.rollAngle) * cos(accelProfile.pitchAngle));
//		YawU = atan2(MagnetMap[1], MagnetMap[0]);
//
//	float XH;
//	XH = (compass_data.y * cos(accelProfile.pitchAngle * (3.141592 / 180))) +
//		 (compass_data.x * sin(accelProfile.pitchAngle * (3.141592 / 180)) * sin(accelProfile.rollAngle * (3.141592 / 180))) +
//		 (compass_data.z * cos(accelProfile.pitchAngle * (3.141592 / 180)) * sin(accelProfile.rollAngle * (3.141592 / 180)));
//	float YH;
//	YH = (compass_data.x * cos(accelProfile.rollAngle * (3.141592 / 180))) +
//		 (compass_data.z * sin(accelProfile.rollAngle * (3.141592 / 180)));
//
//	Yaw = atan2f(-YH, XH);
//	YawU = atan2f(-compass_data.x, compass_data.y);
}
