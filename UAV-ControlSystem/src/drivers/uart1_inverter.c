/*
 * uart1_inverter.c
 *
 *  Created on: 21 sep. 2016
 *      Author: Philip
 */
 /**********************************************************************
 * NAME: uart1_inverter.h                                              *
 * AUTHOR: Philip Johansson                                            *
 * PURPOSE: Flip USART inversion on USART0                             *
 * INFORMATION:                                                        *
 * This functionality exists only on USART0.                           *
 * Initialize with the desired value                                   *
 *	                                                                   *
 * GLOBAL VARIABLES:                                                   *
 * Variable		Type		Description                                *
 * --------		----		-----------                                *
 * 																	   *
 **********************************************************************/
#include "stm32f4xx.h"

void uart1_rx_inverter_set(bool on);

/***********************************************************************
* BRIEF: Init Inverter                                                 *
* INFORMATION: Must be ran before inversion can be activated.          *
* The "on" arg sets if the inverter should be initiated on or off      *
***********************************************************************/
void uart1_rx_inverter_init(bool on)
{
	GPIO_InitTypeDef gpinit;
	gpinit.Pin = GPIO_PIN_0;
	gpinit.Mode = GPIO_MODE_OUTPUT_PP;
	gpinit.Pull = GPIO_NOPULL;// OBS different from other IOs as they have PULLUP
	gpinit.Speed = GPIO_SPEED_HIGH;
	HAL_GPIO_Init(GPIOC, &gpinit);
	uart1_rx_inverter_set(on);
}

/***********************************************************************
* BRIEF: Set Inverter                                                  *
* INFORMATION: Does not need bo be ran unless the value is changed.    *
***********************************************************************/
void uart1_rx_inverter_set(bool on)
{
	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_0,on ? GPIO_PIN_SET : GPIO_PIN_RESET);
}
