/**************************************************************************
* NAME: 	   led.h                                                      *
* 	                                                                      *
* AUTHOR: 	   Jonas Holmberg 											  *
*																	      *
* PURPOSE: 	   Contains some led functionality.							  *
* 										                                  *
* INFORMATION: Contains functions to configure pins to LEDs. It also      *
* 			   contains some led error codes that can be used in the 	  *
* 			   system, although at the time hardcoded to work only with   *
* 			   the revo on board leds.   								  *
* 			                                                              *
* GLOBAL VARIABLES:                                                       *
* Variable        Type          Description                               *
* --------        ----          -----------                               *
***************************************************************************/

#include "stm32f4xx.h"
#include "stm32f4xx_revo.h"
#include "drivers/system_clock.h"
#include "drivers/led.h"

static int32_t ledTimer = 0;							//Timer that keeps track of when it is ok to refresh the led warnings
static uint16_t currentLedWarning = LEDWARNING_OFF;		//The current led warning that is activated in the system


/**************************************************************************
* BRIEF:	   Toggles a led
*
* INFORMATION: Given a pin and port, it attempts to toggle a led that
* 			   should be linked with the given combination.
**************************************************************************/
void ledToggle(uint16_t led_pin, GPIO_TypeDef* led_port)
{
	HAL_GPIO_TogglePin(led_port, led_pin);
}


/**************************************************************************
* BRIEF:	   Turns on a led.
*
* INFORMATION: Given a pin and port, the function tries to turn on a led.
**************************************************************************/
void ledOn(uint16_t led_pin, GPIO_TypeDef* led_port)
{
	HAL_GPIO_WritePin(led_port, led_pin, GPIO_PIN_SET);
}


/**************************************************************************
* BRIEF: 	   Turns off a led.
*
* INFORMATION: Given a pin and port, the function tries to turn off a led.
**************************************************************************/
void ledOff(uint16_t led_pin, GPIO_TypeDef* led_port)
{
	HAL_GPIO_WritePin(led_port, led_pin, GPIO_PIN_RESET);
}


/**************************************************************************
* BRIEF: 	   Turns on a led that is inverted
*
* INFORMATION: Given a pin and port, the function tries to turn on a led.
**************************************************************************/
void ledOnInverted(uint16_t led_pin, GPIO_TypeDef* led_port)
{
	HAL_GPIO_WritePin(led_port, led_pin, GPIO_PIN_RESET);
}


/**************************************************************************
* BRIEF:	   Turns off a led that is inverted
*
* INFORMATION: Given a pin and port, the function tries to turn off a led.
**************************************************************************/
void ledOffInverted(uint16_t led_pin, GPIO_TypeDef* led_port)
{
	HAL_GPIO_WritePin(led_port, led_pin, GPIO_PIN_SET);
}


/**************************************************************************
* BRIEF:	   Enables pins so that they can be use for a Led
*
* INFORMATION: Given a pin and port enables that configuration to use led
**************************************************************************/
void ledEnable(uint16_t led_pin, GPIO_TypeDef* led_port)
{
	GPIO_InitTypeDef gpinit;
	gpinit.Pin = led_pin;
	gpinit.Mode = GPIO_MODE_OUTPUT_PP;
	gpinit.Pull = GPIO_PULLUP;
	gpinit.Speed = GPIO_SPEED_HIGH;
	HAL_GPIO_Init(led_port, &gpinit);
}


/**************************************************************************
* BRIEF: 	   Enables the two leds on board leds on the revolution board
*
* INFORMATION:
**************************************************************************/
void ledReavoEnable(void)
{
	ledEnable(Led0_PIN, GPIOB);
	ledEnable(Led1, GPIOB);
}


/**************************************************************************
* BRIEF: 	   Change the warning type of a led
*
* INFORMATION: Change the warning type of led given a warningId that is
*              obtained from ledWarnings_t.
**************************************************************************/
void ledSetWarningType(uint16_t warningId)
{
	currentLedWarning = warningId;
}


/**************************************************************************
* BRIEF: 	   Refresh the led warnings
*
* INFORMATION: Given the current led warning perform associated action.
**************************************************************************/
void ledIndicatorsRefresh()
{
	/* Use the two leds on the revo board for warnings, revo on board leds are inverted (off = on, vice versa) */
	switch(currentLedWarning)
	{
		case LEDWARNING_LED0_ON:
			ledOnInverted(Led0_PIN, Led0_GPIO_PORT);
			break;
		case LEDWARNING_LED1_ON:
			ledOnInverted(Led1, Led1_GPIO_PORT);
			break;
		case LEDWARNING_LED0_BLINK:
			ledToggle(Led0_PIN, Led0_GPIO_PORT);
			break;
		case LEDWARNING_LED1_BLINK:
			ledToggle(Led1, Led1_GPIO_PORT);
			break;
		case LEDWARNING_LED0_BLINK_ONCE:
			ledOnInverted(Led0_PIN, Led0_GPIO_PORT);
			ledSetWarningType(LEDWARNING_OFF);
			break;
		case LEDWARNING_LED1_BLINK_ONCE:
			ledOnInverted(Led1, Led1_GPIO_PORT);
			ledSetWarningType(LEDWARNING_OFF);
			break;
		case LEDWARNING_OFF:
		default:
			ledOffInverted(Led1, Led1_GPIO_PORT);
			ledOffInverted(Led0_PIN, Led0_GPIO_PORT);
			break;
	}

	//Update the timer so that the led will not update unnecessarily to often
	int32_t timeNow = clock_get_us();
	ledTimer = timeNow +LED_UPDATE_TIMER;
}


/**************************************************************************
* BRIEF:	   Updates the warning leds in the system
*
* INFORMATION: Checks if any led warning should be active and if its the
* 			   case perform some led activities on a specific led
**************************************************************************/
void ledIndicatorsUpdate(void)
{
	//get the current time
	int32_t timeNow = clock_get_us();

	//Can only refresh the leds if a certain time has passed
	if (timeNow - ledTimer < 0)
		return;

	//Refresh the led warnings
	ledIndicatorsRefresh();
}
