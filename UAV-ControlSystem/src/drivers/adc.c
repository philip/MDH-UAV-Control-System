/*
 * ADC.c
 /**********************************************************************
 * NAME: adc.c                                                         *
 * AUTHOR: Philip Johansson                                            *
 * PURPOSE: Set up and read from ADC                                   *
 * INFORMATION:                                                        *
 *	How to use this driver is explained in page 107 of HAL driver      *
 *	Enable the ADC clock                                               *
 *	Enable the GPIO clock for the pin wanted                           *
 *	Configure the GPIO pin as analog input                             *
 *	Configure the ADC speed (prescaler/sampling time)                  *
 *	Enable continuous measurement mode                                 *
 *	                                                                   *
 *	Read more at: www.visualgdb.com/tutorials/arm/stm32/adc/           *
 *	                                                                   *
 * GLOBAL VARIABLES:                                                   *
 * Variable		Type		Description                                *
 * --------		----		-----------                                *
 * 																	   *
 **********************************************************************/
#include "drivers/adc.h"
#include "system_variables.h"
#include "utilities.h"
#include <stdlib.h>

/* EEPROM value - Initialized here if EEPROM corrupted or reset */
bool uart1_rx_inverter = 0;

/* A buffer for the ADC to write with DMA */
enum{ ADC_BUFFER_LENGTH = 10 }; // TODO: Make this define instead of enum
uint32_t g_ADCBuffer[160] = {0}; // We allocate more than needed. Gives 10 readings on 16ch

/* Handler for the ADC */
ADC_HandleTypeDef g_AdcHandle;

/* DMA handler */
DMA_HandleTypeDef g_DmaHandle;

/* Stores a list (in order) of the channels added
 * OBS: Index/size defined by channel_counter */
uint32_t channels_added[16];
/* Increments when new ADC input is added */
int channel_counter = 0;

/* Stores the rank of every channel */
uint32_t channel_ranks[16] = {0};



void adc_pin_conf(uint32_t adc_channel, int adc_rank);


/***********************************************************************
* BRIEF: Read ADC channel                                              *
* INFORMATION: Returns a mean value from "ADB_BUFFER_LENGTH" samples.  *
***********************************************************************/
uint32_t adc_read(uint32_t adc_channel)
{
	int rank = channel_ranks[adc_channel];
	int sum = 0;
	for (int i = rank - 1; i < ADC_BUFFER_LENGTH * channel_counter; i += channel_counter )
		sum += g_ADCBuffer[i];

	return (sum/ADC_BUFFER_LENGTH);
}


/***********************************************************************
* BRIEF: DMA configuration                                             *
* INFORMATION: Is triggered automatically, enables clocks              *
***********************************************************************/
void HAL_ADC_MspInit(ADC_HandleTypeDef * hadc)
{

	   //static DMA_HandleTypeDef g_DmaHandle;

	   // Enable DMA, ADC and related GPIO ports clock
	   __DMA2_CLK_ENABLE();
	   __ADC1_CLK_ENABLE();
	   __GPIOA_CLK_ENABLE();
	   __GPIOC_CLK_ENABLE();

	   // DMA2 Stream4 channel1 (physically mapped to ADC1) configuration
	   g_DmaHandle.Instance = DMA2_Stream4;
	   g_DmaHandle.Init.Channel = DMA_CHANNEL_0;
	   g_DmaHandle.Init.Direction = DMA_PERIPH_TO_MEMORY;
	   g_DmaHandle.Init.PeriphInc = DMA_PINC_DISABLE;
	   g_DmaHandle.Init.MemInc = DMA_MINC_ENABLE;
	   g_DmaHandle.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
	   g_DmaHandle.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
	   g_DmaHandle.Init.Mode = DMA_CIRCULAR;
	   g_DmaHandle.Init.Priority = DMA_PRIORITY_HIGH;
	   g_DmaHandle.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
	   g_DmaHandle.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_HALFFULL;
	   g_DmaHandle.Init.MemBurst = DMA_MBURST_SINGLE;
	   g_DmaHandle.Init.PeriphBurst = DMA_PBURST_SINGLE;
	   HAL_DMA_Init(&g_DmaHandle);

	   // Associate the initialized DMA handle to the the ADC handle
	   //__HAL_LINKDMA(adc_handler, DMA_Handle, g_DmaHandle);
	   __HAL_LINKDMA(&g_AdcHandle, DMA_Handle, g_DmaHandle);

	   /* These are for interrupt which we are not using for ADC/DMA */
	   //NVIC configuration for DMA transfer complete interrupt
	   //HAL_NVIC_SetPriority(DMA2_Stream4_IRQn, 15, 15);
	   //HAL_NVIC_EnableIRQ(DMA2_Stream4_IRQn);
}


/***********************************************************************
* BRIEF: Configuration of ADC                                          *
* INFORMATION: Also initializes                                        *
***********************************************************************/
void adc_configure()
{

	/*  Not using the IRQs but here is how they can be declared */
	//	__ADC1_CLK_ENABLE();
	//
	//	HAL_NVIC_SetPriority(ADC_IRQn, 0,0);
	//	HAL_NVIC_EnableIRQ(ADC_IRQn);

	g_AdcHandle.Instance = ADC1;
	g_AdcHandle.Init.ClockPrescaler = ADC_CLOCKPRESCALER_PCLK_DIV8;
	g_AdcHandle.Init.Resolution = ADC_RESOLUTION12b;
	g_AdcHandle.Init.ScanConvMode = ENABLE; //DISABLE;
	g_AdcHandle.Init.ContinuousConvMode = ENABLE;
	g_AdcHandle.Init.DiscontinuousConvMode = DISABLE;
	g_AdcHandle.Init.NbrOfDiscConversion = 0;
	g_AdcHandle.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE; //ADC_EXTERNALTRIGCONV_T1_CC1;
	g_AdcHandle.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONVEDGE_NONE;
	g_AdcHandle.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	g_AdcHandle.Init.NbrOfConversion = channel_counter; //adc_rank;
	g_AdcHandle.Init.DMAContinuousRequests = ENABLE;//DISABLE;
	g_AdcHandle.Init.EOCSelection = EOC_SINGLE_CONV;


	if (HAL_ADC_Init(&g_AdcHandle) != HAL_OK)
		Error_Handler();
	for (int i = 0; i < channel_counter; i++)
		adc_pin_conf(channels_added[i], i + 1);
}

/***********************************************************************
* BRIEF: Add the wanted channels to a list                             *
* INFORMATION: Not initialized here but later                          *
***********************************************************************/
void adc_pin_add(uint32_t adc_channel)
{
	channels_added[channel_counter] = adc_channel;
	channel_counter++;
}

/***********************************************************************
* BRIEF: Configures the ADC channel and ads it to the handler          *
* INFORMATION: Each added channel gets an incrementing rank            *
***********************************************************************/
void adc_pin_conf(uint32_t adc_channel, int adc_rank)
{

	/* Variable used to initialize the GPIO */
	GPIO_InitTypeDef gpioInit;
	/* Variable to assign the IO to its Port ex PA, PB etc */
	GPIO_TypeDef * gpio_port;

	/* Configuration dependent on channel - TODO complete this switch */
	switch (adc_channel)
	{
		case ADC_CHANNEL_0:
			gpio_port = GPIOA;
			gpioInit.Pin = GPIO_PIN_0;
			break;
		case ADC_CHANNEL_1:
			gpio_port = GPIOA;
			gpioInit.Pin = GPIO_PIN_1;
			break;
		case ADC_CHANNEL_2:
			gpio_port = GPIOA;
			gpioInit.Pin = GPIO_PIN_2;
			break;
		case ADC_CHANNEL_3:
		case ADC_CHANNEL_4:
		case ADC_CHANNEL_5:
		case ADC_CHANNEL_6:
		case ADC_CHANNEL_7:
		case ADC_CHANNEL_8:
		case ADC_CHANNEL_9:
		case ADC_CHANNEL_10:
		case ADC_CHANNEL_11:
			gpio_port = GPIOC;
			gpioInit.Pin = GPIO_PIN_1;
			break;
		case ADC_CHANNEL_12:
			gpio_port = GPIOC;
			gpioInit.Pin = GPIO_PIN_2;
			break;
		case ADC_CHANNEL_13:
		case ADC_CHANNEL_14:
		case ADC_CHANNEL_15:
			break;
	}

	gpioInit.Mode = GPIO_MODE_ANALOG;
	gpioInit.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(gpio_port,&gpioInit);

	ADC_ChannelConfTypeDef adc_channel_conf;

	adc_channel_conf.Channel = adc_channel; // ex ADC_CHANNEL_12
	adc_channel_conf.Rank = (uint32_t)adc_rank;
	adc_channel_conf.SamplingTime = ADC_SAMPLETIME_480CYCLES;
	adc_channel_conf.Offset = 0;

	channel_ranks[adc_channel] = (uint32_t)adc_rank;

	if (HAL_ADC_ConfigChannel(&g_AdcHandle, &adc_channel_conf) != HAL_OK)
		Error_Handler();
}


/***********************************************************************
* BRIEF: When ADC is configured this function starts the DMA sampling  *
* INFORMATION: Third arg is size. When its full it starts over.        *
***********************************************************************/
void adc_start()
{
	if (HAL_ADC_Start_DMA(&g_AdcHandle, (uint32_t*)&g_ADCBuffer, channel_counter * ADC_BUFFER_LENGTH) != HAL_OK)
		Error_Handler();
}




