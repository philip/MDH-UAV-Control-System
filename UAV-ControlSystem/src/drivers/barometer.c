/**************************************************************************
* NAME: 	   barometer.c												  *
*																		  *
* AUTHOR: 	   Jonas Holmberg											  *
*                                                        				  *
* PURPOSE: 	   Used to provide an estimated altitude, in regards to the   *
*              lift of height that would represent zero meters in height. *
*																		  *
* INFORMATION: Using I2C to communicate with the barometer a pressure and *
*			   temperature value can be obtained. These values can then be*
*			   used to estimate an altitude. Note that this is not an     *
*			   altitude value relative to the ground underneath it. It is *
*			   relative to the position where the system was started from.*
*			   The start position of the system will indicate the zero    *
*			   height. It is that position and only that one which will   *
*			   be the compared height.									  *
* 			                                                              *
* GLOBAL VARIABLES:                                                       *
* Variable        Type          Description                               *
* --------        ----          -----------                               *
***************************************************************************/

#include "drivers/barometer.h"
#include "drivers/I2C.h"
#include "stm32f4xx_revo.h"
#include "drivers/system_clock.h"
#include "math.h"
#include "drivers/i2c_soft.h"
#include "drivers/failsafe_toggles.h"

#define Device_address_1		0x56 //Address of our device, not really important in this case

#define ADDR_WRITE 				0xEE // Module address write mode
#define ADDR_READ 				0xEF // Module address read mode
#define ADDRESS_BARO			0x77 //0x77

#define CMD_RESET 				0x1E // ADC reset command
#define CMD_ADC_READ 			0x00 // ADC read command
#define CMD_ADC_CONV 			0x40 // ADC conversion command
#define CMD_ADC_D1 				0x00 // ADC D1 conversion
#define CMD_ADC_D2 				0x10 // ADC D2 conversion
#define CMD_ADC_256 			0x00 // ADC OSR=256
#define CMD_ADC_512 			0x02 // ADC OSR=512
#define CMD_ADC_1024 			0x04 // ADC OSR=1024
#define CMD_ADC_2048 			0x06 // ADC OSR=2048
#define CMD_ADC_4096 			0x08 // ADC OSR=4096
#define CMD_PROM_RD 			0xA0 // Prom read command

#define SEA_PRESS   			1013.25 //default sea level pressure level in mb
#define FTMETERS    			0.3048  //convert feet to meters

#define CALIBRATION_VAL_AMOUNT	40
#define NUMB_AVERAGE_VALS		10

I2C_HandleTypeDef baroI2C_handle;				 //Handle for the HW i2c (NOT USED ATM)
DMA_HandleTypeDef baroI2C_Rx_DMA_handle;		 //Dma handle receive (NOT USED ATM)
DMA_HandleTypeDef baroI2C_Tx_DMA_handle;		 //Dma handle for transmit (NOT USED ATM)
I2C_SOFT_handle_t baroI2C_soft_handle;			 //Handle for the SW i2c

uint8_t sampleAmount;							 //The amount of samples to be used when by the barometer to calculate the needed variables
double baro_Preassure;                      	 // compensated pressure value (mB)
double baro_Temperature;                    	 // compensated temperature value (degC)
double baro_Altitude;                       	 // altitude
double baro_S;                       		     // sea level barometer (mB)

/* Calibration variables */
float altitudeCalibrationValue = 0;				 //Value used as calibration value
float calibrationSamples[CALIBRATION_VAL_AMOUNT];//array of stored values to be used for calibration, only samples calibration values when machine is not armed
int calibrationSamplesCount = 0;				 //Counter for the amount of calibration samples
int calibrationSamplesIterator = 0;				 //Iterator for when going through all the samples

/* average altitude variables */
float averageAltitude[NUMB_AVERAGE_VALS];
uint8_t averageAltitudeIterator = 0;
uint8_t averageAltitudeCount = 0;


/* address: 0 = factory data and the setup
 * address: 1-6 = calibration coefficients
 * address: 7 = serial code and CRC */
uint32_t coefficients_arr[8];                    //coefficient storage
uint8_t cobuf[3] = {0};							 //Array used when writing and reading data over the I2C

/***********************************************************************
 * BRIEF:	    Adds a new altitude value to the average buffer vals   *
 * INFORMATION: Will add the last calculated altitude value to the     *
 * 				buffer used to provide a average calc of altitude      *
 ***********************************************************************/
void barometer_addAverageAltitudeSample()
{
	//fisrt check if the amount of samples is greater than the array
	if (!(averageAltitudeCount >= NUMB_AVERAGE_VALS))
		averageAltitudeCount++;	//if not increase the counter

	//Check if the iterator should restart from the beginning because of overflow
	if (averageAltitudeIterator >= NUMB_AVERAGE_VALS)
		averageAltitudeIterator = 0;	//if it is set it to zero

	//Add the lates calculated altitude value to the samples
	averageAltitude[averageAltitudeIterator] = baro_Altitude;

	//increase the iterator
	averageAltitudeIterator ++;
}

/***********************************************************************
 * BRIEF:	    Adds a new altitude value to the calibration samples.  *
 * INFORMATION: Will add the last calculated altitude value to the     *
 * 				buffer used to provide a calibration value.            *
 ***********************************************************************/
void barometer_addCalibrationSample()
{
	//fisrt check if the amount of samples is greater than the array
	if (!(calibrationSamplesCount >= CALIBRATION_VAL_AMOUNT))
		calibrationSamplesCount++;	//if not increase the counter

	//Check if the iterator should restart from the beginning because of overflow
	if (calibrationSamplesIterator >= CALIBRATION_VAL_AMOUNT)
		calibrationSamplesIterator = 0;	//if it is set it to zero

	//Add the lates calculated altitude value to the samples
	calibrationSamples[calibrationSamplesIterator] = baro_Altitude;

	//increase the iterator
	calibrationSamplesIterator ++;
}

/***********************************************************************
 * BRIEF:       Calibrates the barometers start position.			   *
 * INFORMATION: An array of values are sampled as long as the system   *
 * 				is not armed. Upon arming the system the values in	   *
 * 				the buffer will be averaged and this will give the	   *
 * 				calibration value. In other words it will give the 	   *
 * 				height that represents zero. This value will be        *
 * 				subtracted from every altitude calculation that is	   *
 * 				performed.											   *
 ***********************************************************************/
bool barometer_Calibrate()
{
	//Check if any calibration values exist
	if (calibrationSamplesCount <= 0)
		return false;

	float sampled = 0;

	//loop through all the calibration samples
	for (int i = 0; i < calibrationSamplesCount; i++)
	{
		sampled += calibrationSamples[i];
	}

	//calculate the new calibration value based on the average of all the samples
	altitudeCalibrationValue = sampled / calibrationSamplesCount;

	//rest all the values associated with the calibration samples
	calibrationSamplesCount = 0;
	calibrationSamplesIterator = 0;

	//Set the calibration flag to true
	flags_Set_ID(systemFlags_barometerIsCalibrated_id);

	//Calibration went ok
	return true;
}

/***********************************************************************
 * BRIEF:       Initializes the barometer.							   *
 * INFORMATION: Initializes the barometer and it needs to be called	   *
 *              before anything else when using the barometer.         *
 ***********************************************************************/
bool barometer_init()
{
	//Set the sample rate of the data that will be calculated on the barometer peripheral
	sampleAmount = CMD_ADC_2048;

	//Initialize pins for SCL and SDA
#ifdef BARO_USE_I2C_SOFT
	i2c_soft_Init(I2C1, &baroI2C_soft_handle);
#endif
#ifdef BARO_USE_I2C_HARD
	//Todo: needs implementation
#endif

	return true;
}

/***********************************************************************
 * BRIEF:       Resets the barometer.								   *
 * INFORMATION: Resets the barometer needs to be called after the init.*
 * 				It will send a reset message over the I2C to the       *
 * 				barometer telling it that is should perform a reset.   *
 * 				This needs to be done or it wont be possible to read   *
 * 				data from the barometer.  							   *
 ***********************************************************************/
bool barometer_reset()
{
	/* Send a reset command to the baromter
	 * Afterwards read in all the coefficient values that are stored on the PROM of the barometer */

#ifdef BARO_USE_I2C_SOFT
	i2c_soft_Write(&baroI2C_soft_handle, ADDRESS_BARO, CMD_RESET, 1);
	HAL_Delay(2800);

	for (int i = 0; i < 8; i++)
	{
		uint8_t rxbuf[2] = { 0, 0 };
		i2c_soft_Read(&baroI2C_soft_handle, ADDRESS_BARO, CMD_PROM_RD + i * 2, 2, rxbuf); // send PROM READ command
		coefficients_arr[i] = rxbuf[0] << 8 | rxbuf[1];
	}
#endif

#ifdef BARO_USE_I2C_HARD
	//Todo: needs implementation

	uint8_t cobuf2[3] = {0};
		/* Change to hardware polling mode */
	cobuf2[0] = CMD_ADC_CONV + (CMD_ADC_D2 + sampleAmount);
	HAL_GPIO_DeInit(I2C1_PORT, I2C1_SCL_PIN);
	HAL_GPIO_DeInit(I2C1_PORT, I2C1_SDA_PIN);
	i2c_configure(I2C1, &baroI2C_handle, 0x0);
	bool isSent = i2c_send(&baroI2C_handle, ADDRESS_BARO, cobuf2, 1);
	HAL_Delay(20);

	cobuf2[0] = CMD_ADC_READ;
	i2c_send(&baroI2C_handle, ADDRESS_BARO, cobuf2, 1);
	i2c_receive(&baroI2C_handle, ADDRESS_BARO, cobuf2, 3);



		/* Hardware DMA test */
	//	cobuf2[0] = CMD_ADC_CONV + (CMD_ADC_D2 + sampleAmount);
	//	HAL_GPIO_DeInit(I2C1_PORT, I2C1_SCL_PIN);
	//	HAL_GPIO_DeInit(I2C1_PORT, I2C1_SDA_PIN);
	//	i2c_configure_DMA(I2C1, &baroI2C_handle, &baroI2C_Rx_DMA_handle, &baroI2C_Tx_DMA_handle, 0x0);
	//	bool isSent2 = HAL_I2C_Master_Transmit_DMA(&baroI2C_handle, ADDRESS_BARO, cobuf2, 1);
	//	//bool isSent2 = i2c_send(&baroI2C_handle, ADDRESS_BARO, cobuf2, 1);
	//	HAL_Delay(20);
	//
	//
	//	cobuf2[0] = CMD_ADC_READ;
	//	isSent2 = HAL_I2C_Master_Transmit_DMA(&baroI2C_handle, ADDRESS_BARO, cobuf2, 1);
	//	//isSent2 = i2c_send(&baroI2C_handle, ADDRESS_BARO, cobuf2, 1);
	//
	//	HAL_Delay(20);
	//	HAL_I2C_Master_Receive_DMA(&baroI2C_handle, ADDRESS_BARO, cobuf2, 3);
	//	//i2c_receive(&baroI2C_handle, ADDRESS_BARO, cobuf2, 3);
	//	while(HAL_DMA_GetState(&baroI2C_handle) != HAL_DMA_STATE_READY )
	//	{
	//
	//	}
	//	HAL_Delay(20);

#endif


	/* Produce an initial calibration value */
	/* Run loop 5 times since there are 5 state, also need delay to ensure values will be read */
	for (int i = 0; i < 5; i ++)
	{
		barometer_CaclulateValues();
		HAL_Delay(10);
	}

	/* Set the inital calibration value */
	barometer_Calibrate();

	//force bakc the iscalibrated status to false
	flags_Clear_ID(systemFlags_barometerIsCalibrated_id);

	return true;
}

/***********************************************************************
 * BRIEF:       Calculates the values of temp, pres, and altitude.     *
 * INFORMATION: It takes in two values D1 and D2 which are the values  *
 * 				that have been read from the barometer. This values are*
 * 				then used to perform the calculations together with    *
 * 				the coefficients that have been read in the reset      *
 * 				function.											   *
 ***********************************************************************/
void barometer_CalculatePTA(uint32_t D1, uint32_t D2)
{
	/* calculate dT, difference between actual and reference temp: (D2 - C5 * 2^8) */
	int64_t dT = D2 - ((uint64_t)coefficients_arr[5] << 8);

	/* Calculate OFF, offset at actual temp: (C2 * 2^16 + (C4 * dT)/2^7) */
	int64_t OFF = (((uint32_t)coefficients_arr[2]) << 16) + ((coefficients_arr[4] * dT) >> 7);

	/* Calculate SENS, sensitivity at actual temperature: (C1 * 2^15 + (C3 * dT)/2^8) */
	int64_t SENS = ((uint32_t)coefficients_arr[1] << 15) + ((coefficients_arr[3] * dT) >> 8 );

	/* Calculate TEMP: Actual temperature -40 to 85 C: (2000 + dT * C6/2^23) */
	int32_t TEMP = 2000 + (int64_t)dT * (int64_t)coefficients_arr[6] / (int64_t)(1 << 23);
	baro_Temperature = (double)TEMP / 100.0;	//Assign the calculated temp to the holding variable

	/* Improvements for different temperatures */
	if (TEMP < 2000) //if temp is lower than 20 Celsius
	{
		int64_t T1 = ((int64_t)TEMP - 2000) * ((int64_t)TEMP - 2000);
		int64_t OFF1  = (5 * T1) >> 1;
		int64_t SENS1 = (5 * T1) >> 2;
		if(TEMP < -1500) //if temp is lower than -15
		{
			T1 = ((int64_t)TEMP + 1500) * ((int64_t)TEMP + 1500);
			OFF1  +=  7 * T1;
			SENS1 += 11 * T1 >> 1;
		}
		OFF -= OFF1;
		SENS -= SENS1;
	}
	/* Calculate pressure, temperature compensated pressure 10..1200mbar: ( (D1 * SENS/2^21 - OFF)2^15 ) */
	baro_Preassure = (double)(((((int64_t)D1 * SENS ) >> 21) - OFF) / (double) (1 << 15)) / 100.0;

	/* Calculate the altitude */
	float feet = ((float)1 - (pow(((float)baro_Preassure / (float)SEA_PRESS), (float)0.190284))) * (float)145366.45;
	baro_Altitude = (flags_IsSet_ID(systemFlags_barometerIsCalibrated_id)) ? (feet * FTMETERS) - altitudeCalibrationValue : (feet * FTMETERS);


	/* Add altitude values to altitude buffer containing the last few readings */
	barometer_addAverageAltitudeSample();
}

/***********************************************************************
 * BRIEF:	    Calculates the values of the preassure, temperature and*
 * 			    altitude.											   *
 * INFORMATION: This function needs to be called five times for the    *
 * 				data to be updated. This is because of some limitations*
 * 				and to ensure the schedulability of the system it needs*
 * 				to be divided. Firstly there is an inherit delay inside*
 * 				the barometer sensor. To get data from the barometer a *
 * 				message needs to be sent that tells the barometer to   *
 * 				prepare the data. This takes, depending on the amount  *
 * 				of sampling that is done up to 10 ms for the highest   *
 * 				amount of sampling. This also needs to be done two     *
 * 				times before that data can be calculated. Also since   *
 * 				the implementation uses a software I2C at the moment   *
 * 				because of some problems with the DMA implementation   *
 * 				the speed is not very high. Therefore sending several  *
 * 				messages and reading at the same time may take to long *
 * 				time and could cause the system to be unschedulable.   *
 * 				Because of this the function is divided into different *
 * 				cases:												   *
 * 					1: Prepare data.								   *
 * 					2: Read data.									   *
 * 					3: Prepare data.								   *
 * 					4: Read data.									   *
 * 					5: Calculate temperature, pressure and altitude.   *
 ***********************************************************************/
void barometer_CaclulateValues()
{
	/*the calculation is in need of different states. This is because the
	 * a wait time is needed when talking to the sensor. Because we cant
	 * use a delay wait we need to do parts of the calculation every time
	 * the function is called. The "delay" is handled by the period of
	 * the task that handles the calculation. It cant have a period faster
	 * that 10 ms or 5ms or 2.5 ms and so on, depending on the CMD_ADC_ assigned
	 *  to the variable "sampleAmount"  The wait will not be enough in some cases according
	 * to the datasheet of the sensor http://www.amsys.info/sheets/amsys.en.ms5611_01ba03.pdf*/
	static uint8_t currentCalculationState = CALCSTATE_D2_CALCULATION;
	static uint32_t D1 = 0;
	static uint32_t D2 = 0;
	uint8_t cobuf[3] = {0};
	uint32_t startTime;
	uint32_t endTime;

	//If the machine is armed and not calibrated we perform a calibraton
	if (!flags_IsSet_ID(systemFlags_barometerIsCalibrated_id))
	{
		if (flags_IsSet_ID(systemFlags_armed_id))
		{
			barometer_Calibrate();
		}
	}

	switch (currentCalculationState)
	{
		case CALCSTATE_D2_CALCULATION:
			//Set the message to be sent to the barometer
			cobuf[0] = CMD_ADC_CONV + (CMD_ADC_D2 + sampleAmount);

			//Send the message to the barometer
			#ifdef BARO_USE_I2C_SOFT
			i2c_soft_Write(&baroI2C_soft_handle, ADDRESS_BARO, CMD_ADC_CONV + (CMD_ADC_D2 + sampleAmount),1); // send conversion command
			#endif
			#ifdef BARO_USE_I2C_HARD
			i2c_send(&baroI2C_handle, ADDRESS_BARO, cobuf, 1);
			#endif

			//change the state so we will go to D2 read next time function is called
			currentCalculationState = CALCSTATE_D2_READ;
			break;
		case CALCSTATE_D2_READ:
			//Set the message to be sent to the barometer
			cobuf[0] = CMD_ADC_READ;

			//Read the message from the barometer
			#ifdef BARO_USE_I2C_SOFT
			i2c_soft_Read(&baroI2C_soft_handle, ADDRESS_BARO, CMD_ADC_READ, 3, cobuf); // send PROM READ command
			#endif
			#ifdef BARO_USE_I2C_HARD
			i2c_send(&baroI2C_handle, ADDRESS_BARO, cobuf, 1);
			i2c_receive(&baroI2C_handle, ADDRESS_BARO, cobuf, 3);
			#endif

			//Shift the values to the correct position for the 24 bit D2 value
			D2 = (cobuf[0] << 16) + (cobuf[1] << 8) + cobuf[2];

			//change the state so we will go to D2 read next time function is called
			currentCalculationState = CALCSTATE_D1_CALCULATION;
			break;
		case CALCSTATE_D1_CALCULATION:
			//Set the message to be sent to the barometer
			cobuf[0] = CMD_ADC_CONV + (CMD_ADC_D1 + sampleAmount);

			//Send the message to the barometer
			#ifdef BARO_USE_I2C_SOFT
			i2c_soft_Write(&baroI2C_soft_handle, ADDRESS_BARO, CMD_ADC_CONV + (CMD_ADC_D1 + sampleAmount),1); // send conversion command
			#endif
			#ifdef BARO_USE_I2C_HARD
			i2c_send(&baroI2C_handle, ADDRESS_BARO, cobuf, 1);
			#endif

			//change the state so we will go to D1 read next time function is called
			currentCalculationState = CALCSTATE_D1_READ;
			break;
		case CALCSTATE_D1_READ:
			//Set the message to be sent to the barometer
			cobuf[0] = CMD_ADC_READ;

			//Read the message from the baromter
			#ifdef BARO_USE_I2C_SOFT
			i2c_soft_Read(&baroI2C_soft_handle, ADDRESS_BARO, CMD_ADC_READ, 3, cobuf); // send PROM READ command
			#endif
			#ifdef BARO_USE_I2C_HARD
			i2c_send(&baroI2C_handle, ADDRESS_BARO, cobuf, 1);
			i2c_receive(&baroI2C_handle, ADDRESS_BARO, cobuf, 3);
			#endif

			//Shift the values to the correct position for the 24 bit D2 value
			D1 = (cobuf[0] << 16) + (cobuf[1] << 8) + cobuf[2];

			//Change the state for the next time the function is called
			currentCalculationState = CALCSTATE_CALCULATE_PTA;
			break;
		case CALCSTATE_CALCULATE_PTA:
			startTime = clock_get_us();
			//Calculate the Pressure, temperature and altitude
			barometer_CalculatePTA(D1, D2);

			//only calculate new calibration values if we are not armed
			if (!flags_IsSet_ID(systemFlags_armed_id))
			{
				barometer_addCalibrationSample();	//add new calibration value
				flags_Clear_ID(systemFlags_barometerIsCalibrated_id);	//Clear the flag for barometer calibration
			}

			endTime = clock_get_us() - startTime;
			//Change the state
			currentCalculationState = CALCSTATE_D2_CALCULATION;
			break;
	}

}

/***********************************************************************
 * BRIEF:	    Retrieves the previously calculated pressure.		   *
 * INFORMATION: Returns the last calculated pressure value. No         *
 *              calculation is performed here so calling this will give*
 *              the same value until a new calculation has been        *
 *              performed.											   *
 ***********************************************************************/
double barometer_GetCurrentPressure()
{
	return baro_Preassure;
}

/***********************************************************************
 * BRIEF:	    Retrieves the previously calculated temperature.	   *
 * INFORMATION: Returns the last calculated temperature value. No      *
 *              calculation is performed here so calling this will give*
 *              the same value until a new calculation has been		   *
 *              performed.											   *
 ***********************************************************************/
double barometer_GetCurrentTemperature()
{
	return baro_Temperature;
}

/***********************************************************************
 * BRIEF:	    Retrieves the previously calculated altitude.		   *
 * INFORMATION: Returns the last calculated altitude value. No		   *
 *              calculation is performed here so calling this will give*
 *              the same value until a new calculation has been		   *
 *              performed.										       *
 ***********************************************************************/
float barometer_GetCurrentAltitude()
{
	return baro_Altitude;
}

/***********************************************************************
 * BRIEF:	    Gets the altitude based on the last number of values.   *
 * INFORMATION: Averages the value on the last few reading to get a more*
 * 			    accurate reading.									    *
 ***********************************************************************/
float barometer_GetCurrentAveragedtAltitude()
{
//	float toReturn = 0;
//	/* Go through all the values in the buffer */
//	for (int i = 0; i < averageAltitudeCount; i++)
//	{
//		toReturn += averageAltitude[i];
//	}
//
//	/* Return the average of the stored values */
//	toReturn = toReturn/averageAltitudeCount;
//	return toReturn;
//

	static float lpf_Acc = 0;
	static float smooth = 0;
	float toReturn = 0;


	/* Filter part, go thorugh each axis */

	//Calculate a new smooth value based on a factor of the LPF value
	smooth = lpf_Acc / 16;

	//Calculate the new LPF value based on the raw sensor data - the smoothing value
	lpf_Acc += baro_Altitude - smooth;

	//recalculate the accelerometer data based on the smooth value, since we had to take the square root off the original value we square it to get in the original size
	// toReturn = smooth * smooth;

	return smooth;




//	static float prevVal = 0;
//	float toRet = (prevVal*6 + baro_Altitude) / 8;
//	prevVal = baro_Altitude;
//	return toRet;


}


