/***************************************************************************
* NAME: I2C.c                                                              *
* AUTHOR: Lennart Eriksson                                                 *
* PURPOSE: Enabole the I2C Communication channel on the Revo board         *
* INFORMATION:                                                             *
* This file initilizes the I2C communication that can be used by barometer *
* communication etc.                                                       *
*                                                                          *
* GLOBAL VARIABLES:                                                        *
* Variable        Type          Description                                *
* --------        ----          -----------                                *
***************************************************************************/

#include "drivers/I2C.h"
#include "stm32f4xx_revo.h"

/******************************************************************************
* BRIEF: Configure the I2C bus to be used                                     *
* INFORMATION:                                                                *
******************************************************************************/
bool i2c_configure(I2C_TypeDef *i2c,
				   I2C_HandleTypeDef *out_profile,
				   uint32_t my_address)
{
	uint8_t i2c_af;
	uint16_t sda_pin, scl_pin;
	GPIO_TypeDef *i2c_port;

	// get the correct alternate function and pins for the selected I2C
	// Only I2C1 and I2C2 is available on the REVO board
	if(i2c == I2C1)
	{
		i2c_af = GPIO_AF4_I2C1;
		i2c_port = I2C1_PORT;
		sda_pin = I2C1_SDA_PIN;
		scl_pin = I2C1_SCL_PIN;
//		if(__HAL_RCC_I2C1_IS_CLK_DISABLED())
//			__HAL_RCC_I2C1_CLK_ENABLE();
	}
	else if(i2c == I2C2)
	{
		i2c_af = GPIO_AF4_I2C2;
		i2c_port = I2C2_PORT;
		sda_pin = I2C2_SDA_PIN;
		scl_pin = I2C2_SCL_PIN;
		if(__HAL_RCC_I2C2_IS_CLK_DISABLED())
			__HAL_RCC_I2C2_CLK_ENABLE();
	}
	else
		return false; // The provided I2C is not correct

	if(__HAL_RCC_GPIOB_IS_CLK_DISABLED())
		__HAL_RCC_GPIOB_CLK_ENABLE();

	//Initialize pins for SCL and SDA
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = sda_pin | scl_pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStruct.Alternate = i2c_af;
	HAL_GPIO_Init(i2c_port, &GPIO_InitStruct);


	HAL_Delay(10);
	if(__HAL_RCC_I2C1_IS_CLK_DISABLED())
				__HAL_RCC_I2C1_CLK_ENABLE();
	//Initialize I2C communication
	out_profile->Instance = i2c;
	out_profile->Init.ClockSpeed = 400000;
	out_profile->Init.DutyCycle = I2C_DUTYCYCLE_2/*I2C_DUTYCYCLE_2*/;
	out_profile->Init.OwnAddress1 = my_address;
	out_profile->Init.OwnAddress2 = 0;
	out_profile->Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	out_profile->Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	out_profile->Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	out_profile->Init.NoStretchMode = I2C_NOSTRETCH_DISABLED;
	if(HAL_I2C_Init(out_profile) != HAL_OK)
		return false;

	return true;
}


/******************************************************************************
* BRIEF: Get data over the I2C bus                                            *
* INFORMATION:                                                                *
* Since this system uses a 7 bit addressing mode we send the slave address    *
* in the first bytes 7 MSbs and then the LSb is set to 1 indicating that      *
* it is a receive command, after that the slave responds with a 1 bit ack and *
* the data is sent one byte at a time with an acknowledge in between          *
* every byte, as per the standard. Returns true if successful                 *
******************************************************************************/
bool i2c_receive(I2C_HandleTypeDef* profile,
				 uint8_t slave_address,
				 uint8_t* buffer,
				 uint32_t length)
{
	uint8_t i = 0;
	while(HAL_I2C_Master_Receive(profile, (slave_address << 1) | 1, buffer, length, 1000)!= HAL_OK && i++ < 10)
	{}
	while (HAL_I2C_GetState(profile) != HAL_I2C_STATE_READY)
	{}

	return (i < 10);
}

/***************************************************************************
* BRIEF: Send data over the I2C bus                                        *
* INFORMATION:                                                             *
* Since this system uses a 7 bit addressing mode we send the slave address *
* in the first bytes 7 MSbs and then the LSb is set to 0 indicating that   *
* it is a send command, after that the slave responds with a 1 bit ack and *
* the data is sent one byte at a time with an acknowledge in between       *
* every byte, as per the standard. Returns true if successful              *
***************************************************************************/
bool i2c_send(I2C_HandleTypeDef* profile,
			  uint8_t slave_address,
			  uint8_t* data,
			  uint32_t length)
{
	//TODO: Fix this function
	uint8_t i = 0;
	// try to send the data 10 times and if no acknowledge is received during these 10 messages we stop trying so that
	// the system don't gets stuck forever because a slave is unreachable
	while(HAL_I2C_Master_Transmit(profile,(slave_address << 1), (uint8_t*)data, length, 1000) != HAL_OK && i++ < 10)
	{I2C1->CR1 |= (1 << 9);}
//	while(HAL_I2C_Master_Transmit(profile, slave_address, (uint8_t*)data, length, 5000) != HAL_OK && i++ < 10)
//		{}

	//Wait til the I2C bus is done with all sending
    while (HAL_I2C_GetState(profile) != HAL_I2C_STATE_READY){}

	return (i < 10);
}

uint8_t dma_baro_rx_buffer[3];
uint8_t dma_baro_tx_buffer[1];

bool i2c_configure_DMA(I2C_TypeDef *i2c,
				   I2C_HandleTypeDef *out_profile,
				   DMA_HandleTypeDef *out_rxDMA_profile,
				   DMA_HandleTypeDef *out_txDMA_profile,
				   uint32_t my_address)
{
	uint8_t i2c_af;
	uint16_t sda_pin, scl_pin;
	GPIO_TypeDef *i2c_port;
	DMA_Stream_TypeDef *dma_rx_instance, *dma_tx_instance;
	uint32_t channel;

	// get the correct alternate function and pins for the selected I2C
	// Only I2C1 and I2C2 is available on the REVO board
	if(i2c == I2C1)
	{
		i2c_af = GPIO_AF4_I2C1;
		i2c_port = I2C1_PORT;
		sda_pin = I2C1_SDA_PIN;
		scl_pin = I2C1_SCL_PIN;
		dma_rx_instance = DMA1_Stream5;
		dma_tx_instance = DMA1_Stream6;
		channel = DMA_CHANNEL_1;
//		if(__HAL_RCC_I2C1_IS_CLK_DISABLED())
//			__HAL_RCC_I2C1_CLK_ENABLE();
	}
	else if(i2c == I2C2)
	{
		i2c_af = GPIO_AF4_I2C2;
		i2c_port = I2C2_PORT;
		sda_pin = I2C2_SDA_PIN;
		scl_pin = I2C2_SCL_PIN;
		if(__HAL_RCC_I2C2_IS_CLK_DISABLED())
			__HAL_RCC_I2C2_CLK_ENABLE();
	}
	else
		return false; // The provided I2C is not correct

	if(__HAL_RCC_GPIOB_IS_CLK_DISABLED())
		__HAL_RCC_GPIOB_CLK_ENABLE();


	//Initialize pins for SCL and SDA
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = sda_pin | scl_pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStruct.Alternate = i2c_af;
	HAL_GPIO_Init(i2c_port, &GPIO_InitStruct);

	HAL_Delay(10);
	if(__HAL_RCC_I2C1_IS_CLK_DISABLED())
				__HAL_RCC_I2C1_CLK_ENABLE();
	if(__HAL_RCC_DMA1_IS_CLK_DISABLED())
				__HAL_RCC_DMA1_CLK_ENABLE();
	//Initialize I2C communication
	out_profile->Instance = i2c;
	out_profile->Init.ClockSpeed = 400000;
	out_profile->Init.DutyCycle = I2C_DUTYCYCLE_2/*I2C_DUTYCYCLE_2*/;
	out_profile->Init.OwnAddress1 = my_address;
	out_profile->Init.OwnAddress2 = 0;
	out_profile->Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	out_profile->Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	out_profile->Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	out_profile->Init.NoStretchMode = I2C_NOSTRETCH_DISABLED;


	if(HAL_I2C_Init(out_profile) != HAL_OK)
		return false;

	// Enable the DMA on the i2c register level
	out_profile->Instance->CR2 |= (1 << 11);

	/* Create the DMAs */
	DMA_HandleTypeDef out_rxDMA_profile2;
	out_rxDMA_profile2.Instance = dma_rx_instance;
	out_rxDMA_profile2.Init.Channel = channel;
	out_rxDMA_profile2.Init.Direction = DMA_PERIPH_TO_MEMORY;
	out_rxDMA_profile2.Init.PeriphInc = DMA_PINC_DISABLE;
	out_rxDMA_profile2.Init.MemInc = DMA_MINC_ENABLE;
	out_rxDMA_profile2.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	out_rxDMA_profile2.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
	out_rxDMA_profile2.Init.Mode = DMA_CIRCULAR;
	out_rxDMA_profile2.Init.Priority = DMA_PRIORITY_VERY_HIGH;
	if(HAL_DMA_Init(&out_rxDMA_profile2) != HAL_OK)
		return false;

	__HAL_LINKDMA(out_profile ,hdmarx, out_rxDMA_profile2);

	DMA_HandleTypeDef out_txDMA_profile2;
	out_txDMA_profile2.Instance = dma_tx_instance;
	out_txDMA_profile2.Init.Channel = channel;
	out_txDMA_profile2.Init.Direction = DMA_MEMORY_TO_PERIPH;
	out_txDMA_profile2.Init.PeriphInc = DMA_PINC_DISABLE;
	out_txDMA_profile2.Init.MemInc = DMA_MINC_ENABLE;
	out_txDMA_profile2.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	out_txDMA_profile2.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
	out_txDMA_profile2.Init.Mode = DMA_CIRCULAR;
	out_txDMA_profile2.Init.Priority = DMA_PRIORITY_VERY_HIGH;
	if(HAL_DMA_Init(&out_txDMA_profile2) != HAL_OK)
			return false;

	__HAL_LINKDMA(out_profile ,hdmatx, out_txDMA_profile2);


	//Setup DMA buffers

	// Disable the DMA, must be done before writing to the addresses below
	dma_rx_instance->CR &= ~(DMA_SxCR_EN);

	dma_rx_instance->NDTR = 3;                                    // Set the buffer size
	dma_rx_instance->PAR =  (uint32_t)&i2c->DR; // Set the address to the USART data register
	dma_rx_instance->M0AR = (uint32_t)dma_baro_rx_buffer;                // Set the address to the first DMA buffer
	dma_rx_instance->CR &=  ~(0xF << 11);                                         // Set the data size to 8 bit values

	//Enable the DMA again to start receiving data from the USART
	dma_rx_instance->CR |= DMA_SxCR_EN;


	dma_tx_instance->CR &= ~(DMA_SxCR_EN);

	dma_tx_instance->NDTR = 1;
	dma_tx_instance->PAR =  (uint32_t)&i2c->DR;
	dma_tx_instance->M0AR = (uint32_t)dma_baro_tx_buffer;
	dma_tx_instance->CR &=  ~(0xF << 11);


	dma_tx_instance->CR |= DMA_SxCR_EN;


	return true;
}

