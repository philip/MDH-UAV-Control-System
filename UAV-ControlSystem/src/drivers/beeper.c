/*
 * beeper.c
 *
 *  Created on: 14 nov. 2016
 *      Author: holmis
 */

#include "drivers/beeper.h"


uint16_t beeperPin;
GPIO_TypeDef* beeperPort;

void initBeeper(uint16_t beeper_pin, GPIO_TypeDef* beeper_port)
{
	beeperPin = beeper_pin;
	beeperPort = beeper_port;

	GPIO_InitTypeDef gpinit;
	gpinit.Pin = beeper_pin;
	gpinit.Mode = GPIO_MODE_OUTPUT_PP;
	gpinit.Pull = GPIO_PULLUP;
	gpinit.Speed = GPIO_SPEED_HIGH;
	HAL_GPIO_Init(beeper_port, &gpinit);

}

void busyWaitBeep(uint16_t beepTimeMs)
{
	/* If you use this in the scheduled part of the code, you might face a problem with a little bit of a crash ok? */
	HAL_GPIO_WritePin(beeperPort, beeperPin, SET);
	HAL_Delay(beepTimeMs);
	HAL_GPIO_WritePin(beeperPort, beeperPin, RESET);
}
