/*
 * i2c_soft.c
 *
 *  Created on: 27 okt. 2016
 *      Author: holmis
 */

#include "drivers/i2c_soft.h"
#include "stm32f4xx_revo.h"

#define WRITE_INDICATOR					0
#define READ_INDICATOR					1

/***********************************************************************
 * BRIEF:       set given pin to high
 * INFORMATION:
 ***********************************************************************/
static void IOHi(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
	HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_SET);
}

/***********************************************************************
 * BRIEF:       Set given pin to low
 * INFORMATION:
 ***********************************************************************/
static void IOLo(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
	HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_RESET);
}

/***********************************************************************
 * BRIEF:	    Read given ii pin
 * INFORMATION:
 ***********************************************************************/
static bool IORead(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
	return !! (GPIOx->IDR & GPIO_Pin);
}

/***********************************************************************
 * BRIEF:       Delay for a few micros
 * INFORMATION:
 ***********************************************************************/
static void i2c_soft_delay(void)
{
    volatile int i = 1;
    while (i) {
        i--;
    }
}

/***********************************************************************
 * BRIEF:       Initializes the SW I2C.
 * INFORMATION: Initializes the SW I2C, needs to be done before any
 * 			    thing else.
 ***********************************************************************/
void i2c_soft_Init(I2C_TypeDef *i2c, I2C_SOFT_handle_t *out_profile)
{
	uint16_t sda_pin, scl_pin;
	GPIO_TypeDef *i2c_port;

	//Check what i2c should be used
	if(i2c == I2C1)
	{
		i2c_port = I2C1_PORT;
		sda_pin = I2C1_SDA_PIN;
		scl_pin = I2C1_SCL_PIN;
	}
	else if(i2c == I2C2)
	{
		i2c_port = I2C2_PORT;
		sda_pin = I2C2_SDA_PIN;
		scl_pin = I2C2_SCL_PIN;
	}

	//Init the GPIO pins
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = scl_pin | sda_pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
	HAL_GPIO_Init(i2c_port, &GPIO_InitStruct);

	//Assign the values to the out struct
	out_profile->i2c_Port = i2c_port;
	out_profile->i2c_scl_pin = scl_pin;
	out_profile->i2c_sda_pin = sda_pin;
}

/***********************************************************************
 * BRIEF:       Starts the i2c
 * INFORMATION:
 ***********************************************************************/
static bool i2c_soft_Start(I2C_SOFT_handle_t *handle)
{
	IOHi(handle->i2c_Port, handle->i2c_sda_pin);
	IOHi(handle->i2c_Port, handle->i2c_scl_pin);
    asm ("nop"); // i2c_soft_delay();
    if (!IORead(handle->i2c_Port, handle->i2c_sda_pin)) {
        return false;
    }
    IOLo(handle->i2c_Port, handle->i2c_sda_pin);
    asm ("nop"); // i2c_soft_delay();
    if (IORead(handle->i2c_Port, handle->i2c_sda_pin)) {
        return false;
    }
    IOLo(handle->i2c_Port, handle->i2c_sda_pin);
    asm ("nop"); // i2c_soft_delay();
    return true;
}

/***********************************************************************
 * BRIEF:      Stops the i2c
 * INFORMATION:
 ***********************************************************************/
static void i2c_soft_Stop(I2C_SOFT_handle_t *handle)
{
	IOLo(handle->i2c_Port, handle->i2c_scl_pin);
	asm ("nop"); // i2c_soft_delay();
    IOLo(handle->i2c_Port, handle->i2c_sda_pin);
    asm ("nop"); // i2c_soft_delay();
    IOHi(handle->i2c_Port, handle->i2c_scl_pin);
    asm ("nop"); // i2c_soft_delay();
    IOHi(handle->i2c_Port, handle->i2c_sda_pin);
    asm ("nop"); // i2c_soft_delay();
}

/***********************************************************************
 * BRIEF:       Sends ack
 * INFORMATION:
 ***********************************************************************/
static void i2c_soft_Ack(I2C_SOFT_handle_t *handle)
{
	IOLo(handle->i2c_Port, handle->i2c_scl_pin);
	asm ("nop"); // i2c_soft_delay();
    IOLo(handle->i2c_Port, handle->i2c_sda_pin);
    asm ("nop"); // i2c_soft_delay();
    IOHi(handle->i2c_Port, handle->i2c_scl_pin);
    asm ("nop"); // i2c_soft_delay();
    IOLo(handle->i2c_Port, handle->i2c_scl_pin);
    asm ("nop"); // i2c_soft_delay();
}

/***********************************************************************
 * BRIEF:      Sends no ack
 * INFORMATION:
 ***********************************************************************/
static void i2c_soft_NoAck(I2C_SOFT_handle_t *handle)
{
	IOLo(handle->i2c_Port, handle->i2c_scl_pin);
    asm ("nop"); // i2c_soft_delay();
    IOHi(handle->i2c_Port, handle->i2c_sda_pin);
    asm ("nop"); // i2c_soft_delay();
    IOHi(handle->i2c_Port, handle->i2c_scl_pin);
    asm ("nop"); // i2c_soft_delay();
    IOLo(handle->i2c_Port, handle->i2c_scl_pin);
    asm ("nop"); // i2c_soft_delay();
}

/***********************************************************************
 * BRIEF:       Wait for an acknowledge.
 * INFORMATION: Waits for an acknowledge when a message has been sent.
 ***********************************************************************/
static bool i2c_soft_WaitAck(I2C_SOFT_handle_t *handle)
{
	IOLo(handle->i2c_Port, handle->i2c_scl_pin);
	asm ("nop"); // i2c_soft_delay();
    IOHi(handle->i2c_Port, handle->i2c_sda_pin);
    asm ("nop"); // i2c_soft_delay();
    IOHi(handle->i2c_Port, handle->i2c_scl_pin);
    asm ("nop"); // i2c_soft_delay();
    if (IORead(handle->i2c_Port, handle->i2c_sda_pin)) {
    	IOLo(handle->i2c_Port, handle->i2c_scl_pin);
        return false;
    }
    IOLo(handle->i2c_Port, handle->i2c_scl_pin);
    return true;
}

/***********************************************************************
 * BRIEF:       Sends a byte.
 * INFORMATION: Sends the value byte over the i2c.
 ***********************************************************************/
static void i2c_soft_SendByte(I2C_SOFT_handle_t *handle, uint8_t byte)
{
    uint8_t i = 8;
    while (i--) {
    	IOLo(handle->i2c_Port, handle->i2c_scl_pin);
    	asm ("nop"); // i2c_soft_delay();
        if (byte & 0x80) {
        	IOHi(handle->i2c_Port, handle->i2c_sda_pin);
        }
        else {
        	IOLo(handle->i2c_Port, handle->i2c_sda_pin);
        }
        byte <<= 1;
        asm ("nop"); // i2c_soft_delay();
        IOHi(handle->i2c_Port, handle->i2c_scl_pin);
        asm ("nop"); // i2c_soft_delay();
    }
    IOLo(handle->i2c_Port, handle->i2c_scl_pin);
}

/***********************************************************************
 * BRIEF:       Receives a byte.
 * INFORMATION: Receives a byte and stores is in the byte value.
 ***********************************************************************/
static uint8_t i2c_soft_ReceiveByte(I2C_SOFT_handle_t *handle)
{
    uint8_t i = 8;
    uint8_t byte = 0;

    IOHi(handle->i2c_Port, handle->i2c_sda_pin);
    while (i--) {
        byte <<= 1;
        IOLo(handle->i2c_Port, handle->i2c_scl_pin);
        asm ("nop"); // i2c_soft_delay();
        IOHi(handle->i2c_Port, handle->i2c_scl_pin);
        asm ("nop"); // i2c_soft_delay();
        if (IORead(handle->i2c_Port, handle->i2c_sda_pin)) {
            byte |= 0x01;
        }
    }
    IOLo(handle->i2c_Port, handle->i2c_scl_pin);
    return byte;
}

/***********************************************************************
 * BRIEF:       Reads a message.
 * INFORMATION: Tries to read a message from addr. reg is the message
 * 				that says a read is desired. len is the length of the
 * 				message that should be read and buf is the buffer that
 * 				will store the read data.
 ***********************************************************************/
bool i2c_soft_Read(I2C_SOFT_handle_t *handle, uint8_t addr, uint8_t reg, uint8_t len, uint8_t *buf)
{
	//just send the addres 0x77
		//write = 0, read = 1

    if (!i2c_soft_Start(handle)) {
        return false;
    }

    i2c_soft_SendByte(handle, addr << 1 | 0);
    if (!i2c_soft_WaitAck(handle)) {
        i2c_soft_Stop(handle);
        //i2cErrorCount++;
        return false;
    }
    i2c_soft_SendByte(handle, reg);
    i2c_soft_WaitAck(handle);
    i2c_soft_Start(handle);
    i2c_soft_SendByte(handle, addr << 1 | 1);
    i2c_soft_WaitAck(handle);
    while (len) {
        *buf = i2c_soft_ReceiveByte(handle);
        if (len == 1) {
        	i2c_soft_NoAck(handle);
        }
        else {
        	i2c_soft_Ack(handle);
        }
        buf++;
        len--;
    }
    i2c_soft_Stop(handle);
    return true;
}

/***********************************************************************
 * BRIEF:       Writes a message.
 * INFORMATION: Tries to write to an address. reg is the message that is
 *   			written to the addr. data is the size of the data that
 *   			is written.
 ***********************************************************************/
bool i2c_soft_Write(I2C_SOFT_handle_t *handle, uint8_t addr, uint8_t reg, uint8_t data)
{
	//just send the addres 0x77
	//write = 0, read = 1

	//Start the i2c, if it cant return
	if (!i2c_soft_Start(handle)) {
	        return false;
	    }

	//Send the address
	i2c_soft_SendByte(handle, addr << 1 | WRITE_INDICATOR);
    if (!i2c_soft_WaitAck(handle)) {
        i2c_soft_Stop(handle);
       // i2cErrorCount++;
        return false;
    }

    //send the data
    i2c_soft_SendByte(handle, reg);
    i2c_soft_WaitAck(handle);
    i2c_soft_SendByte(handle, data);
    i2c_soft_WaitAck(handle);
    i2c_soft_Stop(handle);
    return true;
}

