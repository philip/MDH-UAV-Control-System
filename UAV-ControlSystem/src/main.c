
/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  * Awesome ADC fix start project here to be good awesome!!
  ******************************************************************************
*/


#include "drivers/adc.h"
#include "drivers/system_clock.h"
#include "drivers/led.h"
#include "Scheduler/scheduler.h"
#include "stm32f4xx.h"
#include "stm32f4xx_revo.h"
#include "system_variables.h"
#include "utilities.h"
#include <string.h>
#include "drivers/uart1_inverter.h"
#include "config/cli.h"
#include "config/eeprom.h"
#include "drivers/failsafe_toggles.h"
#include "drivers/sbus.h"
#include "drivers/motormix.h"
#include "drivers/motors.h"
#include "Flight/pid.h"
#include "drivers/barometer.h"
#include "drivers/arduino_com.h"
#include "drivers/beeper.h"

/**************************************************************************
* BRIEF: Should contain all the initializations of the system, needs to
* run before the scheduler.
*
* INFORMATION: Everything that will run somewhere in the system at some
* possible point and needs to be initialized to run properly, have to do it
* inside this function.
**************************************************************************/
void init_system()
{
	//Init the Hardware abstraction layer (HAL)
	HAL_Init();

	//Configure the clock
	system_clock_config();

	//init motors to run with oneshot 125, small delay
	HAL_Delay(1000);
	pwmEnableAllMotors(Oneshot125);

	//Initializes all the pids that are used in the system. This part will also init the gyro and accelerometer.
	pidInit();

	/* read saved variables from eeprom, in most cases eeprom should be read after a lot of the initializes  */
	readEEPROM();

	pidEproom();

	//initialize the CLI NOTE: Cant use the same usart as anything else or there will be some big trouble
	cliInit(USART3);

	//init sbus, using USART1
	sbus_init();

	arduinoCom_init(USART6);
//	uart1_rx_inverter_init(false);


#ifdef USE_LEDS
	//Initialize the on board leds
	ledReavoEnable();
	ledOffInverted(Led0_PIN, Led0_GPIO_PORT);
	ledOffInverted(Led1, Led1_GPIO_PORT);
	//ledEnable(GPIO_PIN_0, GPIOA);
#endif

#ifdef BARO
	barometer_init();
	barometer_reset();
#endif

#ifdef COMPASS

#endif

#ifdef GPS

#endif

#ifdef SONAR

#endif

#if defined(BARO) || defined(SONAR)

#endif

#ifdef BEEPER
	initBeeper(BEEPER_PIN, BEEPER_PORT);
#endif



}

/**************************************************************************
* BRIEF: Main function of the system, every thing originates from this
* point.
*
* INFORMATION: The function is responsible for calling the Initialize
* function that will make the system ready to run. It is also responsible
* for starting and running the scheduler in a loop that never stops.
**************************************************************************/
int main(void)
{
	//Init the system
	init_system();

	//Light the yellow led
	ledOnInverted(Led1, Led1_GPIO_PORT);

	//beep that it has been initialized
	busyWaitBeep(1000);

	//Initialize the scheduler, add all the tasks that should run to the ready queue of the scheduler
	initScheduler();


	while (1)
	{
		//Run the scheduler, responsible for distributing all the work of the running system
		scheduler();

#ifdef USE_LED_WARNINGS
		ledIndicatorsUpdate();
#endif
	}

	for(;;);
}

